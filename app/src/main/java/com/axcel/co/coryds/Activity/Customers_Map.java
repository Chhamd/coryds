package com.axcel.co.coryds.Activity;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.axcel.co.coryds.Location.FetchURL;
import com.axcel.co.coryds.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Customers_Map extends AppCompatActivity  implements OnMapReadyCallback{
    private GoogleMap mMap;
    LocationManager locationManager;

    ArrayList<String> name,long_,lat_,pic;
String my_plat,my_plong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers__map);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        name=new ArrayList<>();
        long_=new ArrayList<>();
        lat_=new ArrayList<>();
        pic=new ArrayList<>();

        name= getIntent().getStringArrayListExtra("names");
        long_= getIntent().getStringArrayListExtra("long");
        lat_= getIntent().getStringArrayListExtra("lat");
        pic= getIntent().getStringArrayListExtra("pic");
my_plat=getIntent().getStringExtra("my_plat");
        my_plong=getIntent().getStringExtra("my_plong");






    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(final Marker arg0)
            {
                View vMapInfo = Customers_Map.this.getLayoutInflater().inflate(R.layout.map_info, null);
                final TextView text=(vMapInfo).findViewById(R.id.title);
                TextView distance=(vMapInfo).findViewById(R.id.distance);

                Location loc1 = new Location("");
                loc1.setLatitude(arg0.getPosition().latitude);
                loc1.setLongitude(arg0.getPosition().longitude);
                Location loc2 = new Location("");
                loc2.setLatitude(Double.parseDouble(my_plat));
                loc2.setLongitude(Double.parseDouble(my_plong));
                distance.setText(CalculationByDistance(new LatLng(loc1.getLatitude(),loc1.getLongitude()),new LatLng(loc2.getLatitude(),loc2.getLongitude()))+"");
                text.setText(arg0.getTitle().toUpperCase()+" is ");






                return vMapInfo;
            }

            @Override
            public View getInfoContents(Marker arg0)
            {
                //View v = getLayoutInflater().inflate(R.layout.map_info_layout, null);
                return null;

            }
        });
    showMarker(lat_,long_,pic,name);

    }
    public  void showMarker(ArrayList<String> lat,ArrayList<String> long_,ArrayList<String> dp,ArrayList<String> name){

        for (int i=0 ; i<lat.size(); i++)
        {
            LatLng pick = new LatLng(Double.parseDouble(lat.get(i)), Double.parseDouble(long_.get(i)));
            mMap.addMarker(new MarkerOptions().position(pick)
                    .title(" "+name.get(i))

                    // .icon(BitmapDescriptorFactory.fromBitmap(  createCustomMarker(Customers_Map.this,R.drawable.mail,"Neha","https://coryds.com/uploads/profile/source/"+dp.get(i))))
                    .icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromURL("https://coryds.com/uploads/profile/source/"+dp.get(i))))


            );

        }


        LatLng pick = new LatLng(Double.parseDouble(lat.get(0)), Double.parseDouble(long_.get(0)));

        float zoomLevel = 17.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pick, zoomLevel));
     //   Log.e("hamdullahDistance",""+distance(pick,drop));
//        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
//
//            options.add(pick);
//        options.add(drop);
//
//
//        Polyline line = mMap.addPolyline(options);
      //  MarkerOptions place1 = new MarkerOptions().position(pick).title("Location 1");
     //   MarkerOptions place2 = new MarkerOptions().position(drop).title("Location 2");

    //    new FetchURL(RideDetail.this).execute(getUrl(place1.getPosition(), place2.getPosition(), "driving"), "driving");

    }

    public  Bitmap createCustomMarker(Context context, @DrawableRes int resource, String _name,String url) {
        Bitmap myBitmap;
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_circle, null);

        CircleImageView markerImage = (CircleImageView) marker.findViewById(R.id.user_dp);
        markerImage.setImageResource(resource);
        TextView txt_name = (TextView)marker.findViewById(R.id.name);
        txt_name.setText(_name);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        try {
            URL url1 = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
             myBitmap = BitmapFactory.decodeStream(input);
            Canvas canvas = new Canvas(myBitmap);
            marker.draw(canvas);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }



        return myBitmap;
    }
    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return getCroppedBitmap(myBitmap);
//            return myBitmap;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public Bitmap getCroppedBitmap(Bitmap bitmap) {

        Bitmap _bmp = Bitmap.createScaledBitmap(bitmap, 130, 130, true);

        return getRoundedCroppedBitmap(_bmp);
//        return output;
    }
    private Bitmap getRoundedCroppedBitmap(Bitmap bitmap) {
        int widthLight = bitmap.getWidth();
        int heightLight = bitmap.getHeight();

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        Paint paintColor = new Paint();
        paintColor.setFlags(Paint.ANTI_ALIAS_FLAG);

        RectF rectF = new RectF(new Rect(0, 0, widthLight, heightLight));

        canvas.drawRoundRect(rectF, widthLight / 2 ,heightLight / 2,paintColor);

        Paint paintImage = new Paint();
        paintImage.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(bitmap, 0, 0, paintImage);

        return output;
    }
    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);


        return Radius * c;
    }
}
