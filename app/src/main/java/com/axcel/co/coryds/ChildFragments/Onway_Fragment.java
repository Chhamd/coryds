package com.axcel.co.coryds.ChildFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;

import android.widget.DatePicker;
import android.widget.EditText;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;


import com.axcel.co.coryds.Activity.AddNewCar;
import com.axcel.co.coryds.Activity.Edit_a_Ride;
import com.axcel.co.coryds.Activity.My_Cars;
import com.axcel.co.coryds.Activity.Post_a_Ride;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Location.PlaceAutoSuggestAdapter;
import com.axcel.co.coryds.Location.SingleShotLocationProvider;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.content.Context.LOCATION_SERVICE;


public class Onway_Fragment extends Fragment {
    CheckBox is_onetime, is_usual;
    EditText departure_date, dp_time;
    RelativeLayout days;
    boolean isonetime;
    TextView IS_One, IS_USUAL;
    String current_loc;
    EditText price, seats;
    ArrayAdapter<CharSequence> langAdapter;
    Spinner spinner;
    LocationManager locationManager;
    AutoCompleteTextView Pick_edt, Drop_edt;
    DatePickerDialog.OnDateSetListener date1;
    private ProgressDialog dialog;
    ;
    Button btn_upinfo;
    String source, destination, car, finaltimepick, seats_, pricefinal, mydate_d;
    String pickupltlng, droplatlng;
    String isontime;
    public String[] mycars;
    public String[] mycarsid;
    int year, month, day;
    DatePickerDialog startDatePicker;
    boolean is_enable;
    String Phone, Comments, Route, Route_latlong;
    String serverresponse;
    CheckBox monday, tuesday, wednesday, thursday, friday, satday, sunday;



    String userCountry, userAddress;
    Location gps_loc, network_loc, final_loc;
    ImageView curentloc;
    double latitude, longitude;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.oneway, container, false);
        dialog = new ProgressDialog(getActivity());
        is_onetime = v.findViewById(R.id.is_onetime);
        is_usual = v.findViewById(R.id.is_usual);
        IS_One = v.findViewById(R.id.t1);
        IS_USUAL = v.findViewById(R.id.t2);
        departure_date = v.findViewById(R.id.dpdate);
        days = v.findViewById(R.id.usual1);
        spinner = v.findViewById(R.id.spinner2);
        price = v.findViewById(R.id.price);
        seats = v.findViewById(R.id.seats);
        dp_time = v.findViewById(R.id.dp_time);
        btn_upinfo = v.findViewById(R.id.btn_upinfo);
        curentloc = v.findViewById(R.id.location);

        monday = v.findViewById(R.id.check1);
        tuesday = v.findViewById(R.id.check2);
        wednesday = v.findViewById(R.id.check0);
        thursday = v.findViewById(R.id.check3);
        friday = v.findViewById(R.id.check4);
        satday = v.findViewById(R.id.check9);
        sunday = v.findViewById(R.id.check5);


        initiate();
        is_onetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isonetime) {
                    isonetime = false;
                    switchtousual();
                } else {
                    switchtoonetime();
                    isonetime = true;
                }
            }
        });
        is_usual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isonetime) {
                    isonetime = false;
                    switchtousual();

                } else {

                    switchtoonetime();
                    isonetime = true;
                }
            }
        });
        IS_One.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isonetime) {
                    isonetime = false;
                    switchtousual();
                } else {
                    switchtoonetime();
                    isonetime = true;
                }
            }
        });
        IS_USUAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isonetime) {
                    isonetime = false;
                    switchtousual();

                } else {

                    switchtoonetime();
                    isonetime = true;
                }
            }
        });

        ImageButton p_seats = v.findViewById(R.id.p_seats);
        ImageButton m_seats = v.findViewById(R.id.m_seats);
        p_seats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = Integer.parseInt(seats.getText().toString());

                val = val + 1;
                seats.setText(val + "");
            }
        });
        m_seats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = Integer.parseInt(seats.getText().toString());
                if (val >= 0) {
                    val = val - 1;
                    seats.setText(val + "");
                }

            }
        });

        ImageButton p_price = v.findViewById(R.id.p_price);
        ImageButton m_price = v.findViewById(R.id.m_price);
        p_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = Integer.parseInt(price.getText().toString());

                val = val + 100;
                price.setText(val + "");
            }
        });
        m_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = Integer.parseInt(price.getText().toString());
                if (val >= 100) {
                    val = val - 100;
                    price.setText(val + "");
                }

            }
        });
//
//        new MyCars().execute();
//String mycars[]={"car1","car2","car3","car4"};
//        langAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_text,mycars );
//        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
//        spinner.setAdapter(langAdapter);
        SingleShotLocationProvider.requestSingleUpdate(getActivity(),
                new SingleShotLocationProvider.LocationCallback() {
                    @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        try {

                            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1);
                            if (addresses != null && addresses.size() > 0) {
                                current_loc=addresses.get(0).getAddressLine(0);
                                Log.e("checkmylocationfinal",addresses.get(0).getAddressLine(0));

                            } else {
                                current_loc=null;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("Location", "my location is " + location.toString());
                    }
                });

        Pick_edt = v.findViewById(R.id.pick);
        Pick_edt.setAdapter(new PlaceAutoSuggestAdapter(getActivity(), android.R.layout.simple_list_item_1));

        curentloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_loc!=null)
                {

                    Pick_edt.setText(current_loc);

                }
                else
                {
                    Toast.makeText(getActivity(), "Waiting For Your location", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //
        Drop_edt = v.findViewById(R.id.drop);

        Drop_edt.setAdapter(new PlaceAutoSuggestAdapter(getActivity(), android.R.layout.simple_list_item_1));

        final Calendar myCalendar = Calendar.getInstance();
         startDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
                departure_date.setText(sdf.format(myCalendar.getTime()));

            }
        }, year, month, day);
        //Disable date before today


//        date1 = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                //   String myFormat = "yyyy/mm/dd"; //In which you need put here
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
//
//                departure_date.setText(sdf.format(myCalendar.getTime()));
//            }
//
//        };
        departure_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                startDatePicker.show();



            }
        });
        dp_time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        dp_time.setText( hour + ":" + selectedMinute+ " " + AM_PM );
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        btn_upinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                source=Pick_edt.getText().toString();
                destination=Drop_edt.getText().toString();
                car= mycarsid[spinner.getSelectedItemPosition()];
                finaltimepick=dp_time.getText().toString();

                seats_=seats.getText().toString();
                pricefinal=price.getText().toString();
                mydate_d=departure_date.getText().toString();
//                if(mydate.compareTo(getdate())>0 && checktimings(finaltimepick,finaldoptime) && source.length()>4 && destination.length()>4&& seats.length()==1)
//                Toast.makeText(getActivity(), com_Date(getdate2(),finaltimepick)+"", Toast.LENGTH_LONG).show();
                if(source.length()>4 && destination.length()>4&finaltimepick.length()>0)
                {
                            if(isonetime)
                            {
                                if(mydate_d.length()>0)
                                {
                                    MyDialogue();
                                }
                                else
                                {
                                    Toast.makeText(getActivity(), "Check your date", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                            {
                                MyDialogue();
                            }




                }
                else
                {
//                    if(Integer.parseInt(Constatns.reviews)<5)
//                    {
//                        Toast.makeText(getActivity(), "You Cannot Post Ride Before 24HRS", Toast.LENGTH_SHORT).show();
//                        departure_date.setError("You Cannot Post Ride Before 24HRS");
//                    }
//                    else
//                    {
                        Toast.makeText(getActivity(), "Please Enter Valid Credentials", Toast.LENGTH_SHORT).show();

//                    }


                }
            }
        });
        return v;
    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Log.e("location1",addresses.toString());
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getAddressLine(0));

            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }
    public  DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        // the callback received when the user "sets" the Date in the
        // DatePickerDialog
        public void onDateSet(DatePicker view, int yearSelected,
                              int monthOfYear, int dayOfMonth) {
            year = yearSelected;
            month = monthOfYear + 1;
            day = dayOfMonth;
            // Set the Selected Date in Select date Button
            departure_date.setText(year + "-" + month + "-" + day);
        }
    };
    public void initiate()
    {
        isonetime=true;
        isontime="1";
        is_onetime.setChecked(true);

        is_usual.setChecked(false);
        departure_date.setVisibility(View.VISIBLE);
        days.setVisibility(View.GONE);

    }
    public void switchtousual()
    {
        isontime="0";
        is_onetime.setChecked(false);
        is_usual.setChecked(true);
        departure_date.setVisibility(View.GONE);
        days.setVisibility(View.VISIBLE);
    }
    public void switchtoonetime()
    {  isontime="1";
        is_onetime.setChecked(true);
        is_usual.setChecked(false);
        departure_date.setVisibility(View.VISIBLE);
        days.setVisibility(View.GONE);
    }







    class MyCars extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            is_enable=true;

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);

            if(mycars.length==0)
            {
                btn_upinfo.setVisibility(View.GONE);
                No_Car_Dialogue();
//                Toast.makeText(getActivity(), "There Is No Car, Please Add Car In Cars Section", Toast.LENGTH_SHORT).show();
                   // to prevent irritating accidental logouts


            }
            else
            {
                btn_upinfo.setVisibility(View.VISIBLE);
                langAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_text, mycars );
                langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
                spinner.setAdapter(langAdapter);
            }




        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id",Constatns.user_id);


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_CARS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    mycarsid=new String[categories.length()+1];
                    mycars=new String[categories.length()+1];
                    mycars[0]=("Select a car");
                    mycarsid[0]=("99");
                    for (int i = 0,j=1; i < categories.length(); i++,j++) {
                        JSONObject c = categories.getJSONObject(i);
                        mycars[j]=(c.getString("name")+"-"+c.getString("vehicle_number"));
                        mycarsid[j]=(c.getString("vehicle_id"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {

                mycarsid=new String[0];
                mycars=new String[0];
            }

            return FinalData;
        }
    }
    private LatLng getLatLngFromAddress(String address){

        Geocoder geocoder=new Geocoder(getActivity());
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocationName(address, 1);
            if(addressList!=null){
                Address singleaddress=addressList.get(0);
                LatLng latLng=new LatLng(singleaddress.getLatitude(),singleaddress.getLongitude());
                return latLng;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private Address getAddressFromLatLng(LatLng latLng){
        Geocoder geocoder=new Geocoder(getActivity());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 5);
            if(addresses!=null){
                Address address=addresses.get(0);
                return address;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public String getdate2()
    {
        String currentDate = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public String getdate3()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd hh:mm aa", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    private boolean checktimings(String time, String endtime) {

        String pattern = "hh:mm aa";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }
    class POST_A_RIDE extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.is_refresh=false;
            Constatns.isposted=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(Constatns.isposted=true)
            {
                Pick_edt.setText("");
                Drop_edt.setText("");

                dp_time.setText("");
//                droptime.setText("");
                seats.setText("");
                Toast.makeText(getActivity(), serverresponse, Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
                Constatns.is_refresh=true;
            }
            else
            {
                Toast.makeText(getActivity(), serverresponse, Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();
            String s_lat,s_long,d_lat,d_long;
            HashMap<String, String> HashMapParams = new HashMap<String, String>();
            LatLng latLng=getLatLngFromAddress(source);
            pickupltlng=latLng.latitude+","+latLng.longitude;
            s_lat=latLng.latitude+"";
            s_long=latLng.longitude+"";

            LatLng latLng2=getLatLngFromAddress(destination);
            droplatlng=latLng2.latitude+","+latLng2.longitude;
            d_lat=latLng2.latitude+"";
            d_long=latLng2.longitude+"";
            HashMapParams.put("current_time", getdate3());
            HashMapParams.put("trip_time", mydate_d+finaltimepick);

            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("source_lt","~"+pickupltlng+"~");
            HashMapParams.put("destination_lt", "~"+droplatlng+"~");
            HashMapParams.put("source",source);
            HashMapParams.put("destination", destination);
            HashMapParams.put("pick_time",timeConversion(finaltimepick));
//            HashMapParams.put("drop_time", finaldoptime);
            HashMapParams.put("seats",seats_);
            HashMapParams.put("date", mydate_d);
            HashMapParams.put("vehicle_id",car);
            HashMapParams.put("status", "1");
            HashMapParams.put("price", pricefinal);
            HashMapParams.put("is_onetime", isontime);
            HashMapParams.put("s_lat", s_lat);
            HashMapParams.put("s_long", s_long);
            HashMapParams.put("d_lat", d_lat);
            HashMapParams.put("d_long", d_long);
            HashMapParams.put("phone", Phone);
            HashMapParams.put("coments", Comments);
            HashMapParams.put("route", Route);
            HashMapParams.put("route_latlong", "~"+Route_latlong+"~");
            HashMapParams.put("trip_routes",source +"~"+Route+"~"+destination);
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_POST_RIDE, HashMapParams);

            Log.e("responsemehamd",FinalData);
            serverresponse=FinalData;
            if(FinalData.contains("posted")) {
                try {
                    Constatns.isposted=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);



                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }

    public void MyDialogue()
    {
        final View Text_b = View.inflate(getActivity(), R.layout.post_ride_comments, null);
        final EditText phone = Text_b.findViewById(R.id.phone);
        final EditText comments = Text_b.findViewById(R.id.cmnts);
        final AutoCompleteTextView route = Text_b.findViewById(R.id.route);
        route.setAdapter(new PlaceAutoSuggestAdapter(getActivity(), android.R.layout.simple_list_item_1));
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                .setView(Text_b)
                .setCancelable(false)
                .setPositiveButton("POST", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(phone.getText().toString().length()==11&&comments.getText().toString().length()>0&&route.getText().length()>0)
                        {
                            Phone=phone.getText().toString();
                            Comments=comments.getText().toString();
                            Route=route.getText().toString();
                            LatLng new1=getLatLngFromAddress(route.getText().toString());
                            Route_latlong=new1.latitude+","+new1.longitude;
                            Log.e("checkroute",Route_latlong);
//
                            if(Internet.isInternetAvailable(getActivity())) {
                                new POST_A_RIDE().execute();
                            }
                            else
                            {
                                Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Check Your Phone Number", Toast.LENGTH_SHORT).show();

                        }


                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();



    }
    public static String timeConversion(String Time) {
        DateFormat f1 = new SimpleDateFormat("hh:mm a"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("HH:mm");
        String x = f2.format(d); // "23:00"

        return x;
    }
    public static String timeConversiontorw(String Time) {
        DateFormat f1 = new SimpleDateFormat("HH:mm"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("hh:mm a");
        String x = f2.format(d); // "23:00"

        return x;
    }
    public String gettime()
    {
        String currentDate = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new Date());
        return currentDate;
    }

    public int  com_Date(String day1,String day2)
    {    Date Start = null;
        Date End = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        try {
            Start = simpleDateFormat.parse(day1);
            End = simpleDateFormat.parse(day2);}
        catch(ParseException e){
            //Some thing if its not working
        }

        long difference = End.getTime() - Start.getTime();
        int days = (int) (difference / (1000*60*60*24));
        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
        if(hours < 0){
            hours+=24;
        }if(min < 0){
        float  newone = (float)min/60 ;
        min +=60;
        hours =(int) (hours +newone);}
         int c = hours;
        return c;
    }

    @Override
    public void onResume() {

        new MyCars().execute();

        super.onResume();
    }
    public void No_Car_Dialogue()
    {
        final View Text_b = View.inflate(getActivity(), R.layout.no_car_dialogue, null);
        final Button yes = Text_b.findViewById(R.id.yes);
        final Button cancel = Text_b.findViewById(R.id.cancel);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        Intent i=new Intent(getActivity(), AddNewCar.class);
                        startActivity(i);
                getActivity().onBackPressed();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });


//
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                .setView(Text_b)
                .setCancelable(false)
                .show();






    }
}
