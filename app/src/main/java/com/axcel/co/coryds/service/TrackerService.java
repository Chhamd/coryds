package com.axcel.co.coryds.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;

import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.Detail_Model;
import com.axcel.co.coryds.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class TrackerService extends Service {
    DatabaseReference trackingdb;
    private static final String TAG = TrackerService.class.getSimpleName();
    double lat = 0.0 ,lng = 0.0;
    float[] distance ={0,0,0};
    FusedLocationProviderClient client;
    LocationCallback locationCallback;
    public static final int NOTIFICATION_ID = 555;
    private final String PRIMARY_CHANNEL = "PRIMARY_CHANNEL_ID";
    private final String PRIMARY_CHANNEL_NAME = "PRIMARY";
    @Override
    public IBinder onBind(Intent intent) {return null;}

    @Override
    public void onCreate() {
        super.onCreate();
        trackingdb= FirebaseDatabase.getInstance().getReference().child("Detail");
        buildNotification();
        requestLocationUpdates();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        client.removeLocationUpdates(locationCallback);
    }

    private void buildNotification() {
        String stop = "stop";
        registerReceiver(stopReceiver, new IntentFilter(stop));
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);
        // Create the persistent notification

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL, "PRIMARY_CHANNEL_ID", NotificationManager.IMPORTANCE_LOW);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "PRIMARY_CHANNEL_ID")
                .setAutoCancel(false)
                .setContentTitle("Tracking Ride")
                .setContentText("Ride is in progress")

                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_launcher)


                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setWhen(System.currentTimeMillis());


        startForeground(555, builder.build());
    }

    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            // Stop the service when the notification is tapped
            unregisterReceiver(stopReceiver);
            stopSelf();
        }
    };
    String uid;
    private void requestLocationUpdates() {
        // Functionality coming next step

        LocationRequest request = new LocationRequest();

        request.setInterval(10000);
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        client = LocationServices.getFusedLocationProviderClient(this);

        final String path = "locations" + "/" + uid;
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                final Location location = locationResult.getLastLocation();

                if (location != null) {
                    if(lat!=0.0&&lng!=0.0){
                        Location.distanceBetween(lat,lng,location.getLatitude(),location.getLongitude(),distance);
                        Log.e("lat lng",trackingdb.getKey()+""+lat+","+lng+","+location.getLatitude()+","+location.getLongitude()+","+distance[0]);

                        Detail_Model model=new Detail_Model(Constatns.t_ride_id,"1",Constatns.t_driver_id,lat+"",lng+"","195","Sargodha","Lahore",1212);
                        trackingdb.push().setValue(model);

                    }


                    lat = location.getLatitude();
                    lng = location.getLongitude();
                    Log.d(TAG, "location update " + location);

                }
            }
        };
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            // Request location updates and when an update is
            // received, store the location in Firebase

            client.requestLocationUpdates(request,locationCallback , null);
        }
    }

}