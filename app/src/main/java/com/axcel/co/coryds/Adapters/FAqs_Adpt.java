package com.axcel.co.coryds.Adapters;

import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.R;

import java.util.ArrayList;

public class FAqs_Adpt extends RecyclerView.Adapter<FAqs_Adpt.MyViewHolder> {

    ArrayList<String> question,answer,media;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView question,answer;
        WebView mWebView;

        public MyViewHolder(View view) {
            super(view);

            answer = (TextView) view.findViewById(R.id.answer);
            question = (TextView) view.findViewById(R.id.question);
            mWebView=view.findViewById(R.id.web);


        }
    }


    public FAqs_Adpt(ArrayList<String> question, ArrayList<String> answer,ArrayList<String> media) {
        this.question = question;
        this.answer = answer;
        this.media = media;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faqs_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {



        holder.question.setText(question.get(position));
        holder.answer.setText(answer.get(position));
        if (media.get(position).length()>0)
        {
            String path="<iframe src='"+media.get(position)+"' width='100%' height='100%' style='border: none;'></iframe>";
            holder.mWebView.loadData(path,"text/html","utf-8");
            holder.mWebView.getSettings().setJavaScriptEnabled(true);
            holder.mWebView.setWebChromeClient(new WebChromeClient());
        }
        else
        {
            holder.mWebView.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return question.size();
    }
}