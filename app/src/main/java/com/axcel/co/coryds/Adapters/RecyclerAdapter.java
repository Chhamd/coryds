package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;
import com.squareup.picasso.Picasso;


import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private List<Model> moviesList;
    Context ct;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView fname,pick,drop,date,time,price,seats,cartype;
        Button message;
        ImageView status;
        CircleImageView dp;
        RelativeLayout myrow;


        public MyViewHolder(View view) {
            super(view);
            fname = (TextView) view.findViewById(R.id.fname);
            pick = (TextView) view.findViewById(R.id.pick);
            drop = (TextView) view.findViewById(R.id.dropl);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            seats = (TextView) view.findViewById(R.id.seats);
            message=  (Button) view.findViewById(R.id.message);
            status=  (ImageView) view.findViewById(R.id.status);
            cartype  =  (TextView) view.findViewById(R.id.cartype);
            price=   (TextView) view.findViewById(R.id.price);
            myrow=  view.findViewById(R.id.myrow);
            dp=(CircleImageView)view.findViewById(R.id.dp);

        }
    }


    public RecyclerAdapter(List<Model> moviesList, Context ct) {
        this.moviesList = moviesList;
        this.ct=ct;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Model movie = moviesList.get(position);
        holder.fname.setText(movie.getFname());
        holder.pick.setText(movie.getPick());
        holder.drop.setText(movie.getDrop());
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());
        holder.seats.setText(movie.getSeats()+" seats");
        holder.cartype.setText(movie.getCarname()+" | "+movie.getCarnumber());
        holder.price.setText("PKR "+movie.getPrice());
        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, RideDetail.class);
                i.putExtra("ride_id",movie.getRideid());
                i.putExtra("status",movie.getStatus());
                ct.startActivity(i);
            }
        });
        Picasso.get()
                .load(AppConfig.BASE_URL+movie.getpic())
                .into(holder.dp);

         if(movie.getStatus()==4)
        {

                holder.message.setText("Edit");
                holder.status.setImageResource(R.drawable.pending);

        }
        else if(movie.getStatus()==9)
        {

            holder.message.setText("Completed");
            holder.status.setImageResource(R.drawable.droped);

        }


        holder.myrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, RideDetail.class);
                i.putExtra("ride_id",movie.getRideid());
                i.putExtra("status",movie.getStatus());
                ct.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}