package com.axcel.co.coryds.Config;

public class Chat_Model {
    String thread_id, from_,to_,user_name, dp, message,  date,time;


    public Chat_Model() {
    }

    public Chat_Model(String thread_id, String from_,String to_, String user_name, String dp, String message,String date,String time) {
        this.thread_id = thread_id;
        this.from_ = from_;
        this.to_ = to_;
        this.user_name = user_name;
        this.dp=dp;
        this.message = message;
        this.date=date;
        this.time = time;



    }

    public String getThread_id() {
        return this.thread_id;
    }
    public String getFrom_() {
        return this.from_;
    }
    public String getTo_() {
        return this.to_;
    }
    public String getMessage() {
        return this.message;
    }
    public String getUser_name() {
        return this.user_name;
    }
    public String getDp() {
        return this.dp;
    }
    public String getDate() {
        return this.date;
    }
    public String getTime() {
        return this.time;
    }

}
