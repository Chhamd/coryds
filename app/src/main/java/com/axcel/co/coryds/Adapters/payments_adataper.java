package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Activity.Ride_Detail_Payment;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Chat_Users_Model;
import com.axcel.co.coryds.Config.payment_Model;
import com.axcel.co.coryds.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class payments_adataper extends RecyclerView.Adapter<payments_adataper.MyViewHolder> {

    ArrayList<payment_Model> payments;

    Context ct;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,ride,seats,price,date;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.ct_name);
            ride = (TextView) view.findViewById(R.id.ride);
            seats = (TextView) view.findViewById(R.id.seats);
            price = (TextView) view.findViewById(R.id.price);
            date = (TextView) view.findViewById(R.id.date);
        }
    }


    public payments_adataper(ArrayList<payment_Model> payments, Context ct) {
        this.payments = payments;
        this.ct=ct;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.name.setText(payments.get(position).getName());
        holder.ride.setText(payments.get(position).getRide());
        holder.seats.setText(payments.get(position).getSeats());
        holder.price.setText(payments.get(position).getPrice());
        holder.date.setText(payments.get(position).getDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, Ride_Detail_Payment.class);
                i.putExtra("ride_id",payments.get(position).getRide_id());
                i.putExtra("seats",payments.get(position).getSeats());
                i.putExtra("price",payments.get(position).getPrice());
                i.putExtra("name",payments.get(position).getName());
                i.putExtra("email",payments.get(position).getEmail());
                i.putExtra("mobile",payments.get(position).getMobile());
                i.putExtra("dp",payments.get(position).getDp());
                i.putExtra("status",payments.get(position).getStatus());
                ct.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }
}