package com.axcel.co.coryds.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.axcel.co.coryds.Adapters.MyRide_PagerAdapter;
import com.axcel.co.coryds.Adapters.Past_Adapter;
import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Adapters.Ride_detail_adpt;
import com.axcel.co.coryds.ChildFragments.Customers_all;
import com.axcel.co.coryds.ChildFragments.faqs;
import com.axcel.co.coryds.ChildFragments.privacy_policy;
import com.axcel.co.coryds.ChildFragments.terms_of_use;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.Location.FetchURL;
import com.axcel.co.coryds.Location.TaskLoadedCallback;
import com.axcel.co.coryds.R;

import com.axcel.co.coryds.Utils.Internet;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class RideDetail extends AppCompatActivity implements OnMapReadyCallback, TaskLoadedCallback {
    private GoogleMap mMap;
    LocationManager locationManager;
    private List<Model> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Ride_detail_adpt mAdapter;
    private ProgressDialog dialog;
    String trip_id;
    TextView username,date,time,from,seats,price,v_name,v_number,d_name,d_phone,d_email;
    String name,cost,Date_,Time,From,Seats;
    String p_lat,p_long,d_lat,d_long;
    String start,end;
    String vehicle_name,Vehicle_number,Driver_name,Driver_phone,Driver_email,Driver_id,Price;
    int seats_number;
    Button book_ride;
    ArrayList<String> CUSTOMER_IS_RATED;
    ArrayList<String> CUSTOMER_IS_PICKED;
    ArrayList<String> CUSTOMER_IS_DROPED;
    ArrayList<String> CUSTOMER_IS_ACTIVE;
    ArrayList<String> CUSTOMER_PAYMENT_METHOD;
    ArrayList<String> CUSTOMER_PAYMENT_status;
    ArrayList<String> CUSTOMER_RATING;
    ArrayList<String> CUSTOMER_ID;
    ArrayList<String> CUSTOMER_NAME;
    ArrayList<String> CUSTOMER_PIC;
    ArrayList<String> CUSTOMER_MOBILE;
    ArrayList<String> PRICE;
    ArrayList<String> FROM;
    ArrayList<String> TO;
    ArrayList<String> Payment_Method;
    ArrayList<String> pick_lat,pick_long;
    ArrayList<String> Ride_id;
    String dp;

int status;
String ride_id;
CircleImageView img;
int flag;
TextView nodata;
    TabLayout tab_myride;
    ViewPager mviewPager;
    private Polyline currentPolyline;
    Button locations;
    Button share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_detail_for_rate);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
         trip_id=getIntent().getStringExtra("ride_id");
        status=getIntent().getIntExtra("status",0);
        img=findViewById(R.id.dp);
        share=findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "CoRyds ");
                String shareMessage= "\nLet me recommend you this Trip\n\n";
                shareMessage = shareMessage + "https://stagging.coryds.com/trip/tripdetails/"+trip_id;
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        dialog = new ProgressDialog(RideDetail.this);
         username=findViewById(R.id.name);
        recyclerView=findViewById(R.id.recycler_view);
        date=findViewById(R.id.date);

        from=findViewById(R.id.from);

        v_name=findViewById(R.id.veichle);
        v_number=findViewById(R.id.veichlenumber);
        nodata=findViewById(R.id.nodata);
        nodata.setVisibility(View.GONE);
flag=getIntent().getIntExtra("flag",9);
time=findViewById(R.id.time);
//        tab_myride=findViewById(R.id.tab_myride);
//        mviewPager=findViewById(R.id.mviewPager);
//
//        MyRide_PagerAdapter adapter=new MyRide_PagerAdapter(getSupportFragmentManager());
//        adapter.addNewTab("CUSTOMERS's",new Customers_all());
//      adapter.addNewTab("MAP VIEW",new terms_of_use());
//
        // adapter.addNewTab("My Cars",new Mycars());
//        mviewPager.setAdapter(adapter);
//        tab_myride.setupWithViewPager(mviewPager);



        if(Internet.isInternetAvailable(RideDetail.this)) {
            new Single_Ride_Detail().execute();
        }
        else
        {
            Toast.makeText(RideDetail.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
        locations=findViewById(R.id.locations);
        locations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CUSTOMER_ID.size()==0)
                {
                    Toast.makeText(RideDetail.this, "There Is No Customer To Show", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if (Build.VERSION.SDK_INT >= 26) {
                        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(150,10));
                    } else {
                        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(150);
                    }
                    Intent i=new Intent(RideDetail.this,Customers_Map.class);
                    i.putStringArrayListExtra("names",CUSTOMER_NAME);
                    i.putStringArrayListExtra("long",pick_long);
                    i.putStringArrayListExtra("lat",pick_lat);
                    i.putStringArrayListExtra("pic",CUSTOMER_PIC);

                    i.putExtra("my_plat",p_lat);
                    i.putExtra("my_plong",p_long);

                    startActivity(i);
                }
            }
        });
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }


    public  void showMarker(String plat,String plong,String dlat,String dlong){
        ;

        LatLng pick = new LatLng(Double.parseDouble(plat), Double.parseDouble(plong));
        mMap.addMarker(new MarkerOptions().position(pick).title("from "+start));

        LatLng route = new LatLng(Double.parseDouble("31.544699899999998"), Double.parseDouble("74.35073729999999"));
        mMap.addMarker(new MarkerOptions().position(route).title("Route "));

        LatLng drop = new LatLng(Double.parseDouble(dlat), Double.parseDouble(dlong));
        mMap.addMarker(new MarkerOptions().position(drop).title("To "+end));
        float zoomLevel = 6.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pick, zoomLevel));
        Log.e("hamdullahDistance",""+distance(pick,drop));
//        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
//
//            options.add(pick);
//        options.add(drop);
//
//
//        Polyline line = mMap.addPolyline(options);
        MarkerOptions place1 = new MarkerOptions().position(pick).title("Location 1");
        MarkerOptions place2 = new MarkerOptions().position(drop).title("Location 2");

        new FetchURL(RideDetail.this).execute(getUrl(place1.getPosition(), place2.getPosition(), "driving"), "driving");

    }
    class Single_Ride_Detail extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            pick_lat=new ArrayList<>();
            pick_long=new ArrayList<>();
            movieList = new ArrayList<>();
            CUSTOMER_ID=new ArrayList<>();
            CUSTOMER_NAME=new ArrayList<>();
            CUSTOMER_PIC=new ArrayList<>();
            Payment_Method=new ArrayList<>();
            Ride_id=new ArrayList<>();
            CUSTOMER_MOBILE=new ArrayList<>();
            PRICE=new ArrayList<>();
            FROM=new ArrayList<>();
            TO=new ArrayList<>();
            CUSTOMER_IS_PICKED=new ArrayList<>();
            CUSTOMER_IS_RATED=new ArrayList<>();
            CUSTOMER_IS_DROPED=new ArrayList<>();
            CUSTOMER_IS_ACTIVE=new ArrayList<>();
            CUSTOMER_PAYMENT_METHOD=new ArrayList<>();
            CUSTOMER_PAYMENT_status=new ArrayList<>();
            CUSTOMER_RATING=new ArrayList<>();
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);

            username.setText(name);

            date.setText("Date: "+Date_);

            from.setText(From);

            time.setText("Time: "+timeConversion(Time).toUpperCase());


            v_name.setText(vehicle_name+"  "+Vehicle_number);
            Picasso.get()
                    .load(AppConfig.BASE_URL+dp)
                    .into(img);
            mAdapter = new Ride_detail_adpt(CUSTOMER_ID,CUSTOMER_NAME,CUSTOMER_PIC,PRICE,FROM,TO,vehicle_name,Vehicle_number,Date_,trip_id,ride_id,Driver_id,CUSTOMER_MOBILE,flag,CUSTOMER_IS_PICKED,CUSTOMER_IS_ACTIVE,CUSTOMER_IS_DROPED,CUSTOMER_RATING,CUSTOMER_IS_RATED,CUSTOMER_PAYMENT_status,CUSTOMER_PAYMENT_METHOD,Ride_id,RideDetail.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RideDetail.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(RideDetail.this, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();



            showMarker(p_lat,p_long,d_lat,d_long);
            if(CUSTOMER_ID.size()==0)
            {
                nodata.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            else
            {
                recyclerView.setVisibility(View.VISIBLE);
                nodata.setVisibility(View.GONE);
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("trip_id", trip_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_RIDE_DETAIL_Completed, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");

                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                         name= c.getString("name")+" ";
                        Price=c.getString("price");
                        cost= "PKR "+c.getString("price")+" per passenger";
                        Date_= c.getString("date");
                        Time= c.getString("expected_time");
                        start=c.getString("pick");
                        end=c.getString("drop");
                        From= c.getString("pick")+" -> "+c.getString("drop");
                        Seats= "Total Available seats : "+c.getString("seats");
                        seats_number=Integer.parseInt(c.getString("seats"));
                        p_lat= c.getString("p_lat");
                        p_long= c.getString("p_long");
                        d_lat= c.getString("d_lat");
                        d_long= c.getString("d_long");

                     vehicle_name=c.getString("vehicle_name");
                        Vehicle_number=c.getString("vehicle_number");
                        Driver_name=c.getString("name");
                        Driver_phone=c.getString("phone");
                        Driver_email=c.getString("email");
                        Driver_id=c.getString("driver_id");
                        dp=c.getString("driver_dp");


                    }
                    JSONArray users = obj.getJSONArray("new_users");
Log.e("hamdullah","yrsess"+users.toString());
JSONArray users_sub=users.getJSONArray(0);
                    for (int i = 0; i < users_sub.length(); i++) {
                        JSONObject c = users_sub.getJSONObject(i);
                        CUSTOMER_ID.add(c.getString("user_id"));
                        CUSTOMER_NAME.add(c.getString("user_name"));
                        CUSTOMER_PIC.add(c.getString("user_dp"));
                        CUSTOMER_MOBILE.add(c.getString("customer_phone"));
                        PRICE.add(c.getString("price"));
                        FROM.add(c.getString("from"));
                        TO.add(c.getString("to"));
                        ride_id=c.getString("ride_id");
                        CUSTOMER_IS_PICKED.add(c.getString("is_pick"));
                        CUSTOMER_IS_DROPED.add(c.getString("is_drop"));
                        CUSTOMER_IS_ACTIVE.add(c.getString("is_active"));
                        CUSTOMER_PAYMENT_METHOD.add(c.getString("payment_method"));
                        CUSTOMER_PAYMENT_status.add(c.getString("payment_status"));
                        CUSTOMER_IS_RATED.add(c.getString("is_rated"));
                        CUSTOMER_RATING.add(c.getString("rating"));
                        pick_lat.add(c.getString("pickup_lat"));
                        pick_long.add(c.getString("pickup_long"));
                        Ride_id.add(c.getString("ride_id"));


//                        CUSTOMER_IS_PICKED.add("0");
//                        CUSTOMER_IS_DROPED.add("0");
//                        CUSTOMER_IS_ACTIVE.add("1");
//                        CUSTOMER_PAYMENT_METHOD.add("0");


                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }

    public String getdate()
    {
        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public String gettime()
    {
        SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm:ss a");
        String time = formatDate.format(new Date()).toString();
        return  time;
    }
    public  double distance(LatLng start, LatLng end) {
        try {
            Location location1 = new Location("locationA");
            location1.setLatitude(start.latitude);
            location1.setLongitude(start.longitude);
            Location location2 = new Location("locationA");
            location2.setLatitude(end.latitude);
            location2.setLongitude(end.longitude);

            double distance = location1.distanceTo(location2);
            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            DecimalFormatSymbols custom = new DecimalFormatSymbols();
            custom.setDecimalSeparator('.');
            decimalFormat.setDecimalFormatSymbols(custom);
            double distanceFormated = Double.parseDouble(decimalFormat.format(distance));
            return distanceFormated/1000;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode

        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest+"&waypoints=31.4710169,74.2415515"+ "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyBXt1WR2GLPkXgdkBmUaTRB2uznNzUkKUg";
        return url;
    }
    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }
    public static String timeConversion(String Time) {
        DateFormat f1 = new SimpleDateFormat("HH:mm");
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("hh:mm a");
        String x = f2.format(d); // "23:00"

        return x;
    }
}
