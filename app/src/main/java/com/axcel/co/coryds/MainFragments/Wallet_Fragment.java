package com.axcel.co.coryds.MainFragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Adapters.chat_users_dataper;
import com.axcel.co.coryds.Adapters.payments_adataper;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Chat_Users_Model;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.payment_Model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Wallet_Fragment extends Fragment {
    private ProgressDialog dialog;
    private ArrayList<payment_Model> payments;
    private RecyclerView recyclerView;
    private payments_adataper mAdapter;
    String total_balance;
    TextView balance;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.wallet_fragment,container,false);
        dialog=new ProgressDialog(getActivity());
        recyclerView=v.findViewById(R.id.recycler_view);
        balance=v.findViewById(R.id.balance);
        if(Internet.isInternetAvailable(getActivity())) {
            new get_payments().execute();
        }
        else
        {
            Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
        return v;
    }
    class get_payments extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            payments=new ArrayList<>();
            dialog.setMessage("UPDATING DATA . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(payments.size()>0)
            {
                balance.setText("PKR "+total_balance);
                mAdapter = new payments_adataper(payments,getActivity());
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }





        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("type", "0");
            HashMapParams.put("user_id", Constatns.user_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_PAYMENTS, HashMapParams);

            Log.e("ALl Paymetns",FinalData);
            if(!FinalData.contains("error")) {
                Log.e("Paymetns","yes");
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                        total_balance=c.getString("balance");
                        payment_Model movie = new payment_Model(c.getString("name"),c.getString("ride"),c.getString("seats"),c.getString("booking_price"),c.getString("date"),c.getString("ride_id"),c.getString("email"),c.getString("mobile"),c.getString("dp"),c.getString("status"));
                        payments.add(movie);






                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
}
