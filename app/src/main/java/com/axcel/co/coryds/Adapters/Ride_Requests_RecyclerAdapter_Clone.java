package com.axcel.co.coryds.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Accept_Ride;
import com.axcel.co.coryds.Activity.MainActivity_Driver;
import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Activity.Verification;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.BottomMenuHelper;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Ride_Requests_RecyclerAdapter_Clone extends RecyclerView.Adapter<Ride_Requests_RecyclerAdapter_Clone.MyViewHolder> {

    private List<Model> moviesList;
    Context ct;
    public ProgressDialog dialog;
String user_id;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView fname,pick,drop,date,time,price,seats,cartype;
        TextView reject;
        Button accept;
        ImageView status;
        RelativeLayout myrow;
        CircleImageView dp;
        RatingBar rating;
        TextView reviews;


        public MyViewHolder(View view) {
            super(view);
            fname = (TextView) view.findViewById(R.id.name);
            pick = (TextView) view.findViewById(R.id.pick);
            drop = (TextView) view.findViewById(R.id.drop);
            date = (TextView) view.findViewById(R.id.datetime);
          //  time = (TextView) view.findViewById(R.id.time);
            seats = (TextView) view.findViewById(R.id.seats);
            accept=  (Button) view.findViewById(R.id.accept);
            reject=  (TextView) view.findViewById(R.id.reject);
          //  status=  (ImageView) view.findViewById(R.id.status);
            cartype  =  (TextView) view.findViewById(R.id.vehicle);
          //  price=   (TextView) view.findViewById(R.id.price);
           // myrow=  view.findViewById(R.id.myrow);
            dp=view.findViewById(R.id.dp);
            rating=view.findViewById(R.id.rating);
            reviews=view.findViewById(R.id.txt_reviews);
        }
    }


    public Ride_Requests_RecyclerAdapter_Clone(List<Model> moviesList, Context ct) {

        this.moviesList = moviesList;
        this.ct=ct;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_requests_row_clone, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Model movie = moviesList.get(position);
        holder.fname.setText(movie.getFname());
        holder.pick.setText(movie.getPick());
        holder.drop.setText(movie.getDrop());
        holder.date.setText(movie.getDate()+" "+movie.getTime().toUpperCase());
       // holder.time.setText(movie.getTime());
        holder.seats.setText("Seats Requested: "+movie.getSeats()+"");
        holder.cartype.setText(movie.getCarname()+" | "+movie.getCarnumber());
       // holder.price.setText("PKR "+movie.getPrice());

        holder.rating.setRating(Float.parseFloat(movie.getRating()));
        holder.reviews.setText("( "+movie.getReviews()+" Reviews)");
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(Constatns.is_verified.equals("0"))
                {
                    if(Internet.isInternetAvailable(ct)) {
                        Constatns.accept=true;
                        Constatns.trip_id=movie.getRideid();
                        movie.setstatus(1);
                        dialog = new ProgressDialog(ct);
                        Log.e("ride_id",movie.getRideid()+"-----"+Constatns.accept);
                        Constatns.postition=position;
                        user_id=movie.getUser_id();
                        if(Constatns.rides>0)
                        {
                            Constatns.rides=Constatns.rides-1;
                            if(Constatns.rides>0)
                            {
                                BottomMenuHelper.showBadge(ct, MainActivity_Driver.btm_nav, R.id.menu_req, Constatns.rides+"");
                            }
                            else
                            {
                                BottomMenuHelper.removeBadge( MainActivity_Driver.btm_nav, R.id.menu_req);
                            }
                        }
                        new Accept_request().execute();
                    }
                    else
                    {
                        Toast.makeText(ct, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    if(Constatns.is_form_submit.equals("1")&&Constatns.is_admin_approved.equals("0"))
                    {
                        Toast.makeText(ct, "Your identity verification is inprogress, it may take 2 working days for admin to verify. If it takes longer please contact the support.", Toast.LENGTH_LONG).show();
                    }
                    else if(Constatns.is_form_submit.equals("0")&&Constatns.is_admin_approved.equals("0"))
                    {
                        Toast.makeText(ct, "Please Make Sure You are verified.", Toast.LENGTH_LONG).show();

//                    Intent i=new Intent(ct, Verification.class);
//                    ct.startActivity(i);
                    }
                    else if(Constatns.is_form_submit.equals("1")&&Constatns.is_admin_approved.equals("1"))
                    {
                        if(Internet.isInternetAvailable(ct)) {
                            Constatns.accept=true;
                            Constatns.trip_id=movie.getRideid();
                            movie.setstatus(1);
                            dialog = new ProgressDialog(ct);
                            Log.e("ride_id",movie.getRideid()+"-----"+Constatns.accept);
                            Constatns.postition=position;
                            user_id=movie.getUser_id();
                            if(Constatns.rides>0)
                            {
                                Constatns.rides=Constatns.rides-1;
                                if(Constatns.rides>0)
                                {
                                    BottomMenuHelper.showBadge(ct, MainActivity_Driver.btm_nav, R.id.menu_req, Constatns.rides+"");
                                }
                                else
                                {
                                    BottomMenuHelper.removeBadge( MainActivity_Driver.btm_nav, R.id.menu_req);
                                }
                            }
                            new Accept_request().execute();
                        }
                        else
                        {
                            Toast.makeText(ct, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                        }

                    }
                }





            }
        });
        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Internet.isInternetAvailable(ct)) {
                    dialog = new ProgressDialog(ct);
                    movie.setstatus(0);
                    Constatns.accept=false;
                    Constatns.trip_id=movie.getRideid();
                    Constatns.postition=position;
                    user_id=movie.getUser_id();
                    if(Constatns.rides>0)
                    {
                        Constatns.rides=Constatns.rides-1;
                        if(Constatns.rides>0)
                        {
                            Log.e("check bagde","if"+Constatns.rides);
                            BottomMenuHelper.showBadge(ct, MainActivity_Driver.btm_nav, R.id.menu_req, Constatns.rides+"");
                        }
                        else
                        {
                            Log.e("check bagde","else"+Constatns.rides);
                            BottomMenuHelper.removeBadge( MainActivity_Driver.btm_nav, R.id.menu_req);
                        }
                    }
                    new Accept_request().execute();
                }
                else
                {
                    Toast.makeText(ct, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

//    if(movie.getStatus()==0)
//    {
//        holder.status.setImageResource(R.drawable.inactive);
//    }
//    else
//    {
//        holder.status.setImageResource(R.drawable.pending);
//    }
        Picasso.get()
                .load(AppConfig.BASE_URL+movie.getpic())
                .into(holder.dp);





        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, RideDetail.class);
                i.putExtra("ride_id",movie.getRideid());
                i.putExtra("flag",1);
                ct.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
    class Accept_request extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
//            movieList = new ArrayList<>();
//
            Constatns.is_accept=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            Constatns.accept=false;
            Constatns.trip_id="";
            user_id="";
            if (Constatns.is_accept)
            {
                removeItem(Constatns.postition);
                notifyDataSetChanged();
            }
            else
            {
                removeItem(Constatns.postition);
                notifyDataSetChanged();
            }

//            mAdapter = new New_Ride_RecyclerAdapter(movieList,getActivity());
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//            recyclerView.setLayoutManager(mLayoutManager);
//            recyclerView.setItemAnimator(new DefaultItemAnimator());
//            recyclerView.setAdapter(mAdapter);
//            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
//            recyclerView.setAdapter(mAdapter);
//            mAdapter.notifyDataSetChanged();


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("user_id", user_id);
            HashMapParams.put("trip_id", Constatns.trip_id);
            if(Constatns.accept)
            {
                HashMapParams.put("accept","1");
            }
            else
            {
                HashMapParams.put("accept","0");
            }

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_ACCEPT_REQ, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.is_accept=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

//                        Model movie = new Model(c.getString("user_name"),c.getString("source"),c.getString("destination"),c.getString("booking_date"),c.getString("booking_time"),c.getString("booking_price"),c.getString("booking_seats"),"","",c.getString("trip_id"),Integer.parseInt(c.getString("is_active")));
//                        movieList.add(movie);




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    public void removeItem(int position) {
        moviesList.remove(position);
        notifyItemRemoved(position);
    }
}