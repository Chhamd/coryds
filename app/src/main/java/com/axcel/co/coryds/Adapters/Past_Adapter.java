package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Rate_now;
import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Past_model;
import com.axcel.co.coryds.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Past_Adapter extends RecyclerView.Adapter<Past_Adapter.ViewHolder> {
    private ArrayList<Past_model> dataList;
    Context ct;
    public Past_Adapter(ArrayList<Past_model> data, Context ct)
    {
        this.ct=ct;
        this.dataList = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView img_profile;
        ImageView img_status;
        TextView txt_pname,txt_vehicle,txt_fare,txt_seats,txt_date,stloc,endloc;
        Button rating;


        public ViewHolder(View itemView)
        {
            super(itemView);
            this.img_profile=itemView.findViewById(R.id.v_image);
            this.txt_pname=itemView.findViewById(R.id.txt_pname);
            this.txt_vehicle=itemView.findViewById(R.id.txt_vehicle);
            this.txt_fare =itemView.findViewById(R.id.txt_fare);
            this.txt_seats=itemView.findViewById(R.id.txt_seats);
            this.txt_date =itemView.findViewById(R.id.txt_date);
            this.stloc=itemView.findViewById(R.id.txt_strtloc);
            this.endloc=itemView.findViewById(R.id.txt_endloc);
            this.rating=itemView.findViewById(R.id.btn_rating);

        }
    }

    @Override
    public Past_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_itemview, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Past_Adapter.ViewHolder holder, final int position)
    {

        holder.txt_pname.setText(dataList.get(position).getFname());
        holder.txt_vehicle.setText(dataList.get(position).getCarname()+" | "+dataList.get(position).getCarnumber());
        holder.txt_fare.setText("Rs: "+dataList.get(position).getPrice());
        holder.txt_seats.setText(dataList.get(position).getSeats()+" Seats");
        holder.stloc.setText(dataList.get(position).getPick());
        holder.endloc.setText(dataList.get(position).getDrop());
        holder.txt_date.setText(dataList.get(position).getDate());
        Picasso.get()
                .load(AppConfig.BASE_URL+dataList.get(position).getpic())
                .into(holder.img_profile);
        holder.rating.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {



                Intent i=new Intent(ct, RideDetail.class);
                i.putExtra("ride_id",dataList.get(position).getRideid());
                i.putExtra("flag",0);

                ct.startActivity(i);

            }
        });





    }

    @Override
    public int getItemCount()
    {
        return dataList.size();
    }
}

