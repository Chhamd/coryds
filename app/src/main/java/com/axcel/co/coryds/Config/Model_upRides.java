package com.axcel.co.coryds.Config;

public class Model_upRides {
    String fname,  pick,  drop, date, time, price ,seats,post_id,carname,rideid,pic,is_enq;
    String is_Active,is_booked,is_payment,payment_status,payment_method,owner_id;
    int status;
    String is_admin_verified;
    public Model_upRides() {
    }

    public Model_upRides(String fname, String pick, String drop, String date, String time, String price , String seats, String post_id, String carname, String rideid, String pic, String is_enq, String is_Active, String is_booked, String is_payment, String payment_status, String payment_method, String is_admin_verified, String owner_id, int status) {
        this.fname = fname;
        this.pick = pick;
        this.drop = drop;
        this.date=date;
        this.time = time;
        this.price=price;
        this.seats=seats;
        this.post_id=post_id;
        this.carname=carname;
        this.status=status;
        this.rideid=rideid;
        this.pic=pic;

        this.is_enq=is_enq;
        this.is_Active=is_Active;
        this.is_booked=is_booked;
        this.is_payment=is_payment;
        this.payment_status=payment_status;
        this.payment_method=payment_method;
        this.is_admin_verified=is_admin_verified;

        this.owner_id=owner_id;


    }

    public String getFname() {
        return this.fname;
    }
    public String getPick() {
        return this.pick;
    }
    public String getDrop() {
        return this.drop;
    }
    public String getDate() {
        return this.date;
    }
    public String getTime() {
        return this.time;
    }
    public String getPrice() {
        return this.price;
    }
    public String getSeats() {
        return this.seats;
    }
    public String getPost_id() {
        return this.post_id;
    }
    public String getCarname() {
        return this.carname;
    }
    public String getRideid() {
        return this.rideid;
    }
    public void setstatus(int status) {
        this.status=status;
    }
    public int getStatus() {
        return this.status;
    }
    public String getpic() {
        return this.pic;
    }
    public String getIs_enq() {
        return this.is_enq;
    }
    public String getIs_Active() {
        return this.is_Active;
    }
    public String getIs_booked() {
        return this.is_booked;
    }
    public String getIs_payment() {
        return this.is_payment;
    }
    public String getPayment_status() {
        return this.payment_status;
    }
    public String getPayment_method() {
        return this.payment_method;
    }
    public String getIs_admin_verified() {
        return this.is_admin_verified;
    }
    public String getOwner_id() {
        return this.owner_id;
    }
}
