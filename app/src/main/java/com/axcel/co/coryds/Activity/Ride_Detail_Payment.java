package com.axcel.co.coryds.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.Location.FetchURL;
import com.axcel.co.coryds.Location.TaskLoadedCallback;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class Ride_Detail_Payment extends AppCompatActivity implements OnMapReadyCallback, TaskLoadedCallback {
    private GoogleMap mMap;
    LocationManager locationManager;
    private List<Model> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerAdapter mAdapter;
    private ProgressDialog dialog;
    String trip_id;
    TextView username, date, time, from, seats, price, v_name, v_number, d_name, d_phone, d_email, to;
    String name, cost, Date_, Time, From, Seats, TO;
    String p_lat, p_long, d_lat, d_long;
    String start, end;
    String vehicle_name, Vehicle_number, Driver_name, Driver_phone, Driver_email, Driver_id, Price;
    int seats_number;
    Button book_ride, enq, cancel;
    int booked_Seats;
    Location gps_loc, network_loc, final_loc;

    double latitude, longitude;
    int flag;
    String is_enq;
    boolean ispick, isdrop;
    boolean canceeld;
    String driver_Rating, dp, logo, color;
    String bookedseats;
    CircleImageView DP;
    ImageView LOGO;
    TextView COLOUR;
    CheckBox food, music, pet, smoke;
    RatingBar rating;
    String Music, Pets, Number, Smoke,Food;
    TextView totalreviews;
    String reviews;
    private Polyline currentPolyline;
    boolean IS_ENQ_;
    TextView status,fairetitle,seattitle;
    String Status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride__detail__payment);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        trip_id = getIntent().getStringExtra("ride_id");
        cost= getIntent().getStringExtra("price");
        bookedseats= getIntent().getStringExtra("seats");

        Driver_name= getIntent().getStringExtra("name");
        Driver_email= getIntent().getStringExtra("email");
        Driver_phone= getIntent().getStringExtra("mobile");
        dp=getIntent().getStringExtra("dp");
        Status=getIntent().getStringExtra("status").toUpperCase();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(Ride_Detail_Payment.this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        dialog = new ProgressDialog(Ride_Detail_Payment.this);
        //  username=findViewById(R.id.username);
        price = findViewById(R.id.price);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        from = findViewById(R.id.source);
        to = findViewById(R.id.destination);
        seats = findViewById(R.id.seats);
        v_name = findViewById(R.id.vehicle);
        v_number = findViewById(R.id.veichlenumber);
        d_name = findViewById(R.id.name);
        d_phone = findViewById(R.id.phone);
        d_email = findViewById(R.id.email);
        status=findViewById(R.id.statusnew);
        status.setText("STATUS : "+Status);



        music = findViewById(R.id.music);
        pet = findViewById(R.id.pet);
        smoke = findViewById(R.id.smoking);
        food = findViewById(R.id.food);

        DP = findViewById(R.id.dp);
        LOGO = findViewById(R.id.car_img);
        COLOUR = findViewById(R.id.color);
        rating = findViewById(R.id.rating);
        totalreviews = findViewById(R.id.totalreviews);
        fairetitle= findViewById(R.id.faretitle);
        seattitle= findViewById(R.id.seattitle);
        fairetitle.setText("TOTAL AMMOUNT");
        seattitle.setText("TOTAL SEATS");







        if (Internet.isInternetAvailable(Ride_Detail_Payment.this)) {
            new Single_Ride_Detail().execute();
        } else {
            Toast.makeText(Ride_Detail_Payment.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }




    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }


    public  void showMarker(String plat,String plong,String dlat,String dlong){
        ;

        LatLng pick = new LatLng(Double.parseDouble(plat), Double.parseDouble(plong));
        mMap.addMarker(new MarkerOptions().position(pick).title("from "+start));

        LatLng drop = new LatLng(Double.parseDouble(dlat), Double.parseDouble(dlong));
        mMap.addMarker(new MarkerOptions().position(drop).title("To "+end));
        float zoomLevel = 6.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pick, zoomLevel));
        MarkerOptions place1 = new MarkerOptions().position(pick).title("Location 1");
        MarkerOptions place2 = new MarkerOptions().position(drop).title("Location 2");

        new FetchURL(Ride_Detail_Payment.this).execute(getUrl(place1.getPosition(), place2.getPosition(), "driving"), "driving");
    }
    class Single_Ride_Detail extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            movieList = new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);


            //  username.setText(name);
            price.setText(cost+" PKR");
            date.setText(Date_);
            time.setText(timeConversion(Time.toUpperCase()).toUpperCase());
            to.setText(TO);
            from.setText(From);
            seats.setText(""+bookedseats+"");

            v_name.setText(vehicle_name);
            v_number.setText(Vehicle_number);
            d_name.setText(Driver_name);

            d_email.setText(Driver_email);
            rating.setRating(Float.parseFloat(driver_Rating));
            Picasso.get()
                    .load(AppConfig.BASE_URL+dp)
                    .into(DP);
            Log.e("dpurl",AppConfig.BASE_URL+dp);
            Picasso.get()
                    .load("https://coryds.com/uploads/vehicle/thumbnails/"+logo)
                    .into(LOGO);

            COLOUR.setText(color);


            if(Music.equals("0"))
            {
                music.setChecked(false);

//                music.setBackgroundColor(Color.argb(255, 203, 58, 0));


            }
            else
            {
//                music.setBackgroundColor(Color.argb(255, 40, 167, 2));

//green
                music.setChecked(true);

            }

            if(Pets.equals("0"))
            {
//                pet.setBackgroundColor(Color.argb(255, 203, 58, 0));
                pet.setChecked(false);
            }
            else
            {
                pet.setChecked(true);
//                pet.setBackgroundColor(Color.argb(255, 40, 167, 2));


            }
            if(Number.equals("0"))
            {
                d_phone.setText("PhoneNumber: NA");
            }
            else
            {
                d_phone.setText(Driver_phone);
            }
            d_phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(d_phone.getText().toString().length()>9&& !d_phone.getText().toString().contains("PhoneNumber"))
                    {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", d_phone.getText().toString(), null));
                        startActivity(intent);
                    }
                }
            });
            if(Smoke.equals("0"))
            {
                smoke.setChecked(false);

//                smoke.setBackgroundColor(Color.argb(255, 203, 58, 0));
            }
            else
            {
//                smoke.setBackgroundColor(Color.argb(255, 40, 167, 2));
                smoke.setChecked(true);


            }
            if(Food.equals("0"))
            {
                food.setChecked(false);

//                food.setBackgroundColor(Color.argb(255, 203, 58, 0));
            }
            else
            {
//                food.setBackgroundColor(Color.argb(255, 40, 167, 2));
                food.setChecked(true);


            }
            totalreviews.setText("( "+reviews+" Reviews )");





            showMarker(p_lat,p_long,d_lat,d_long);

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("trip_id", trip_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_RIDE_DETAIL, HashMapParams);

            Log.e("responsemehamdsingle",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                        name= c.getString("name")+" ";
                        Price=c.getString("price");
//                        cost= ""+c.getString("price");
                        Date_= c.getString("date");
                        Time= c.getString("expected_time");
                        start=c.getString("pick");
                        end=c.getString("drop");
                        From= c.getString("pick");
                        TO=c.getString("drop");
//                        Seats= ""+c.getString("seats");
//                        seats_number=Integer.parseInt(c.getString("seats"));
                        p_lat= c.getString("p_lat");
                        p_long= c.getString("p_long");
                        d_lat= c.getString("d_lat");
                        d_long= c.getString("d_long");

                        vehicle_name=c.getString("vehicle_name");
                        Vehicle_number=c.getString("vehicle_number");

                        Driver_id=c.getString("driver_id");

                        driver_Rating=c.getString("rating");;
                        reviews=c.getString("reviews");;
//                        dp=c.getString("dp");
                        logo=c.getString("logo");
                        color=c.getString("color");
                        Music=c.getString("is_music");
                        Pets=c.getString("is_pet");
                        Number=c.getString("is_phone");
                        Smoke=c.getString("is_smoking");
                        Food=c.getString("is_food");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyBXt1WR2GLPkXgdkBmUaTRB2uznNzUkKUg";
        return url;
    }
    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    public String getdate()
    {
        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public String gettime()
    {
        SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm:ss a");
        String time = formatDate.format(new Date()).toString();
        return  time;
    }
    public  long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getdate2()
    {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public static String timeConversion(String Time) {
        DateFormat f1 = new SimpleDateFormat("HH:mm"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("hh:mm a");
        String x = f2.format(d); // "23:00"

        return x;
    }
}


