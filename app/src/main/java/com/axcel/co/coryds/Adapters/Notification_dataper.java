package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.MainActivity_Driver;
import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.MainFragments.MyRequest_Fragment_Driver;
import com.axcel.co.coryds.R;

import java.util.ArrayList;
import java.util.List;

public class Notification_dataper extends RecyclerView.Adapter<Notification_dataper.MyViewHolder> {

   ArrayList<String> id;
    ArrayList<String> content;
    ArrayList<String> date;
    ArrayList<String> IsRead;
    Context ct;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView content,date_time;

        public RelativeLayout Row;

        public MyViewHolder(View view) {
            super(view);
            content = (TextView) view.findViewById(R.id.content);
            date_time = (TextView) view.findViewById(R.id.date_time);
            Row=view.findViewById(R.id.myrow);
        }
    }


    public Notification_dataper(ArrayList<String> id,ArrayList<String> content,ArrayList<String> date,ArrayList<String> IsRead, Context ct) {
        this.id = id;
        this.content=content;
        this.ct=ct;
        this.date=date;
        this.IsRead=IsRead;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.content.setText(content.get(position));
        holder.date_time.setText(date.get(position));

        if(Integer.parseInt(IsRead.get(position))==1)
        {
            holder.Row.setBackgroundResource(R.drawable.edt_grad2);
        }
        else
        {
//            holder.Row.setBackgroundResource(R.drawable.edt_grad3);
            holder.Row.setBackgroundResource(R.drawable.edt_grad2);
        }
        holder.Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(content.get(holder.getLayoutPosition()).contains("request"))
                    {
                       Intent i=new Intent(ct, MainActivity_Driver.class);
                       i.putExtra("flag",1);
                       ct.startActivity(i);
                    }
                if(content.get(holder.getLayoutPosition()).contains("enquiry"))
                {
                    Intent i=new Intent(ct, MainActivity_Driver.class);
                    i.putExtra("flag",1);
                    ct.startActivity(i);
                }
                if(content.get(holder.getLayoutPosition()).contains("message"))
                {
                    Intent i=new Intent(ct, MainActivity_Driver.class);
                    i.putExtra("flag",2);
                    ct.startActivity(i);
                }
                if(content.get(holder.getLayoutPosition()).contains("payment"))
                {
                    Intent i=new Intent(ct, MainActivity_Driver.class);
                    i.putExtra("flag",4);
                    ct.startActivity(i);
                }
                if(content.get(holder.getLayoutPosition()).contains("rated"))
                {
                    Intent i=new Intent(ct, MainActivity_Driver.class);
                    i.putExtra("flag",8);
                    ct.startActivity(i);
                }
            }
        });




    }

    @Override
    public int getItemCount() {
        return content.size();
    }
}