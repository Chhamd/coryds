package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Edit_a_Ride;
import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Today_model;
import com.axcel.co.coryds.Config.Upcoming_model;
import com.axcel.co.coryds.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class Upcoming_Adapter extends RecyclerView.Adapter<Upcoming_Adapter.ViewHolder> {
    private ArrayList<Upcoming_model> dataList;
    Context ct;
    public Upcoming_Adapter(ArrayList<Upcoming_model> data, Context ct)
    {
        this.ct=ct;
        this.dataList = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView img_profile;
        ImageView img_status;
        TextView txt_pname,txt_vehicle,txt_fare,txt_seats,txt_date,stloc,endloc,total;
        ImageView edit;
        Button btn_detail;



        public ViewHolder(View itemView)
        {
            super(itemView);
            this.img_profile=itemView.findViewById(R.id.v_image);
            this.txt_pname=itemView.findViewById(R.id.txt_pname);
            this.txt_vehicle=itemView.findViewById(R.id.txt_vehicle);
            this.txt_fare =itemView.findViewById(R.id.txt_fare);
            this.txt_seats=itemView.findViewById(R.id.txt_seats);
            this.txt_date =itemView.findViewById(R.id.txt_date);
            this.stloc=itemView.findViewById(R.id.txt_strtloc);
            this.endloc=itemView.findViewById(R.id.txt_endloc);
            this.edit=itemView.findViewById(R.id.btn_edit);
            img_status=itemView.findViewById(R.id.status);
            total=itemView.findViewById(R.id.total);
            btn_detail=itemView.findViewById(R.id.btn_detail);
        }
    }

    @Override
    public Upcoming_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_itemview, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Upcoming_Adapter.ViewHolder holder, final int position)
    {

        holder.txt_pname.setText(dataList.get(position).getFname());
        holder.txt_vehicle.setText(dataList.get(position).getCarname()+"  "+dataList.get(position).getCarnumber());
        holder.txt_fare.setText(" Rs: "+dataList.get(position).getPrice());
        holder.txt_seats.setText("Available: "+dataList.get(position).getSeats());

        holder.stloc.setText(dataList.get(position).getPick());
        holder.endloc.setText(dataList.get(position).getDrop());
        holder.txt_date.setText(dataList.get(position).getDate()+"  AT : "+dataList.get(position).getTime().toUpperCase());
        Picasso.get()
                .load(AppConfig.BASE_URL+dataList.get(position).getpic())
                .into(holder.img_profile);
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(ct, RideDetail.class);
                i.putExtra("ride_id",dataList.get(position).getRideid());
                i.putExtra("flag",5);
                ct.startActivity(i);
            }
        });
        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ct, RideDetail.class);
                i.putExtra("ride_id",dataList.get(position).getRideid());
                i.putExtra("flag",5);
                ct.startActivity(i);
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(dataList.get(position).getStatus()== 1 && dataList.get(position).getPayment_status()==1)
                if(dataList.get(position).getStatus()== 1)
                {
                    Toast.makeText(ct, "You can not proceed", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent edit=new Intent(ct, Edit_a_Ride.class);
                    edit.putExtra("pick_add",dataList.get(position).getPick());
                    edit.putExtra("pick_time",dataList.get(position).getTime());
                    edit.putExtra("drop_add",dataList.get(position).getDrop());
                    edit.putExtra("drop_time",dataList.get(position).getTime());
                    edit.putExtra("date",dataList.get(position).getDate());
                    edit.putExtra("is_onetime",true);
                    edit.putExtra("seats",dataList.get(position).getSeats()+"");
                    edit.putExtra("price",dataList.get(position).getPrice());
                    edit.putExtra("trip_id",dataList.get(position).getRideid());
                    ct.startActivity(edit);
                }

            }
        });
        if(dataList.get(position).getStatus()==0 && dataList.get(position).getPayment_status()==0)
        {
            holder.total.setText("In Active: "+dataList.get(position).getBookig_Seats());
            holder.img_status.setImageResource(R.drawable.inactive);
        }
        if(dataList.get(position).getStatus()==1 && dataList.get(position).getPayment_status()==0)
        {
            holder.total.setText("Waiting for payment: "+dataList.get(position).getBookig_Seats());
            holder.img_status.setImageResource(R.drawable.waiting);
        }
        if(dataList.get(position).getStatus()== 1 && dataList.get(position).getPayment_status()==1)
        {
            holder.total.setText("Confirmed: "+dataList.get(position).getBookig_Seats());
            holder.img_status.setImageResource(R.drawable.confirmed);
        }

    }

    @Override
    public int getItemCount()
    {
        return dataList.size();
    }
}

