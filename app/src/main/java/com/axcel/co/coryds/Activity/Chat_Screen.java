package com.axcel.co.coryds.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.axcel.co.coryds.Adapters.chat_users_dataper;
import com.axcel.co.coryds.Adapters.single_chat_users_dataper;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Chat_Model;
import com.axcel.co.coryds.Config.Chat_Users_Model;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.MainFragments.Chat_Fragment;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class Chat_Screen extends AppCompatActivity {
    private ProgressDialog dialog;
    RecyclerView users;
    public static single_chat_users_dataper mAdapter;
    public static ArrayList<Chat_Model> chat;
    Button send;
    EditText content;
    String username,thread_id,from_,to_,Message;
    TextView title;
    CircleImageView dp;
    ImageView back;
    FloatingActionButton  down;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat__screen);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dialog=new ProgressDialog(Chat_Screen.this);
        username=getIntent().getStringExtra("username");
        thread_id=getIntent().getStringExtra("thread_id");
        from_=getIntent().getStringExtra("from_");
        to_=getIntent().getStringExtra("to_");
        back=findViewById(R.id.back);
        dp=findViewById(R.id.dp);
        Picasso.get()
                .load(AppConfig.BASE_URL+getIntent().getStringExtra("dp"))
                .into(dp);
        Log.e("checkdp",AppConfig.BASE_URL+getIntent().getStringExtra("dp"));
        title=findViewById(R.id.title);
        title.setText(username);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        users=findViewById(R.id.chat);


        send=findViewById(R.id.send);
        content=findViewById(R.id.content);
        if(Internet.isInternetAvailable(Chat_Screen.this)) {
            new GET_SINGLE_CHAT().execute();
        }
        else
        {
            Toast.makeText(Chat_Screen.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(content.getText().toString().length()>0)
                {
                    Message=content.getText().toString();

                    Chat_Model movie = new Chat_Model(thread_id, Constatns.user_id,to_,"","",content.getText().toString(),getdate(0),getdate(1));
                    chat.add(movie);

                    mAdapter = new single_chat_users_dataper(chat,Chat_Screen.this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Chat_Screen.this);
                    users.setLayoutManager(mLayoutManager);
                    users.setAdapter(mAdapter);

                    mAdapter.notifyDataSetChanged();
                    users.scrollToPosition(chat.size()-1);
                    content.setText("");
                    if(Internet.isInternetAvailable(Chat_Screen.this)) {
                        new Send_Message().execute();
                    }
                    else
                    {
                        Toast.makeText(Chat_Screen.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
          down = (FloatingActionButton) findViewById(R.id.down);
          down.setVisibility(View.GONE);
//        down.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
//        down.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                users.scrollToPosition(chat.size()-1);
//
//
//            }
//        });
    }
    class GET_SINGLE_CHAT extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            chat=new ArrayList<>();
            dialog.setMessage("UPDATING DATA . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(chat.size()>0)
            {

                mAdapter = new single_chat_users_dataper(chat,Chat_Screen.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Chat_Screen.this);
                users.setLayoutManager(mLayoutManager);
                users.setAdapter(mAdapter);
                users.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                users.scrollToPosition(chat.size()-1);
            }






        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("thread_id", thread_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_ONE_CHATS, HashMapParams);

            Log.e("ALL_CHATS",FinalData);
            if(!FinalData.contains("error")) {
                Log.e("ALL_CHATS","yes");
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Chat_Model movie = new Chat_Model(c.getString("thread_id"),c.getString("from_"),c.getString("to_"),c.getString("user_name"),c.getString("dp"),c.getString("message"),c.getString("date"),c.getString("time"));
                        chat.add(movie);





                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    class Send_Message extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);






        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();

Log.e("tofrom",from_+"::"+Constatns.user_id);

            HashMapParams.put("thread_id",thread_id);
            HashMapParams.put("to_", from_);
            HashMapParams.put("from_",Constatns.user_id);
            HashMapParams.put("message", Message);
            HashMapParams.put("date", getdate(0));
            HashMapParams.put("time",getdate(1));
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_SEND_MESSAGE, HashMapParams);

            Log.e("ALL_CHATS",FinalData);
            if(!FinalData.contains("error")) {
                Log.e("ALL_CHATS","yes");
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Chat_Model movie = new Chat_Model(c.getString("thread_id"),c.getString("from_"),c.getString("to_"),c.getString("user_name"),c.getString("dp"),c.getString("message"),c.getString("date"),c.getString("time"));
                        chat.add(movie);





                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }

    }
    public String getdate(int a)
    {  String currentDate="";
        if(a==0)
        {
            currentDate = new SimpleDateFormat("yyyy/MM/dd ", Locale.getDefault()).format(new Date());
        }
        if(a==1)
        {
            currentDate = new SimpleDateFormat("hh:mm:a", Locale.getDefault()).format(new Date());
        }

        return currentDate;
    }
}
