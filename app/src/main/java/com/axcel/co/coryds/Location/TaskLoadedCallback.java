package com.axcel.co.coryds.Location;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
