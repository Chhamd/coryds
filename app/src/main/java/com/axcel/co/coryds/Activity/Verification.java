package com.axcel.co.coryds.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.axcel.co.coryds.Adapters.Ride_detail_adpt;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static androidx.core.content.FileProvider.getUriForFile;

public class Verification extends AppCompatActivity {

    RelativeLayout front,back;
    TextView expiry;
    Spinner type;
    String filename;
    Uri mImageUri;
    private int  FRONT = 1,BACK=2;
    ArrayAdapter<CharSequence> langAdapter;
    String types[]={"Select Doccument Type","CNIC","Driving License","Passport"};
    DatePickerDialog startDatePicker;
    int    year,month,day;
    ImageView front_img,back_img;
    String FRONT_CODE,BACK_CODE;
    Button submit;
    ProgressDialog dialog;
    EditText number,auth;

String Server_response;

    String DOC_TYPE,NUMBER,EXPIRY,AUTH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        front=findViewById(R.id.front);
        back=findViewById(R.id.back);
        expiry=findViewById(R.id.expiry_date);
        type=findViewById(R.id.type);
        front_img=findViewById(R.id.front_img);
        back_img=findViewById(R.id.back_img);
        submit=findViewById(R.id.submit);
        number=findViewById(R.id.number);
        auth=findViewById(R.id.auth);
        dialog=new ProgressDialog(Verification.this);
        langAdapter = new ArrayAdapter<CharSequence>(Verification.this, R.layout.spinner_text, types );
        langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        type.setAdapter(langAdapter);


        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position==1)
                {
                    front.setVisibility(View.VISIBLE);
                    back.setVisibility(View.VISIBLE);
                }
                if (position==2)
                {
                    front.setVisibility(View.VISIBLE);
                    back.setVisibility(View.VISIBLE);
                }
                if (position==3)
                {
                    front.setVisibility(View.VISIBLE);
                    back.setVisibility(View.GONE);
                    front_img.setImageBitmap(null);
                    back_img.setImageBitmap(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera(1);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera(2);
            }
        });
        final Calendar myCalendar = Calendar.getInstance();
        startDatePicker = new DatePickerDialog(Verification.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
                expiry.setText(sdf.format(myCalendar.getTime()));

            }
        }, year, month, day);
        expiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() +999999999);
                startDatePicker.show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(number.getText().toString().length()<11)
                {
                    number.setError("Please Enter Valid Number");
                }

                if(type.getSelectedItemPosition()>0 && expiry.getText().toString().contains("/")&&number.getText().toString().length()>10)
                {
                    AUTH=auth.getText().toString();
                    DOC_TYPE=types[type.getSelectedItemPosition()];
                    NUMBER=number.getText().toString();
                    EXPIRY=expiry.getText().toString();
                    if(type.getSelectedItemPosition()==1||type.getSelectedItemPosition()==2)
                    {
                        if(BACK_CODE.length()>5&&FRONT_CODE.length()>5)
                        {
                            if(Internet.isInternetAvailable(Verification.this)) {
                                new upload_image().execute();
                            }
                            else
                            {
                                Toast.makeText(Verification.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            Toast.makeText(Verification.this, "Please Add Pictures", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if(type.getSelectedItemPosition()==3)
                    {
                        if(FRONT_CODE.length()>5)
                        {

                            if(Internet.isInternetAvailable(Verification.this)) {
                                new upload_image().execute();
                            }
                            else
                            {
                                Toast.makeText(Verification.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(Verification.this, "Please Add Picture", Toast.LENGTH_SHORT).show();
                        }
                    }

                }


            }
        });

    }
    public void takePhotoFromCamera(int a) {

        filename="";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        filename=System.currentTimeMillis() + ".jpg";
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(filename));
        mImageUri=getCacheImagePath(filename);

        startActivityForResult(takePictureIntent, a);

    }
    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(Verification.this, getPackageName() + ".provider", image);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        Log.e("requestcode",requestCode+"");
        if (requestCode == FRONT) {


            this.getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = this.getContentResolver();
            Bitmap bitmap;
            try
            {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                 FRONT_CODE =getStringImage(bitmap);
                Log.e("froncodecheck",FRONT_CODE);
            front_img.setImageBitmap(bitmap);
            }
            catch (Exception e)
            {

            }



        }
        if (requestCode == BACK)
        {
            this.getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = this.getContentResolver();
            Bitmap bitmap;
            try
            {
                bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                BACK_CODE = getStringImage(bitmap);
                Log.e("backcodecheck",FRONT_CODE);
                back_img.setImageBitmap(bitmap);
            }
            catch (Exception e)
            {

            }
        }




    }
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();

        // Get the Base64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    class upload_image extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {


            dialog.setMessage("Uploading . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {

            dialog.dismiss();
            super.onPostExecute(string1);
            if(Server_response.contains("error"))
            {
                Toast.makeText(Verification.this, "Server Error", Toast.LENGTH_SHORT).show();

            }
            else
            {
                Constatns.is_form_submit="1";
                Toast.makeText(Verification.this, "Your identity verification is inprogress, it may take 2 working days for admin to verify. If it takes longer please contact the support.", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("doc_type", DOC_TYPE);
            HashMapParams.put("number", NUMBER);
            HashMapParams.put("expiry", EXPIRY);
            HashMapParams.put("auth", AUTH);
            HashMapParams.put("user_id",Constatns.user_id);



            if(DOC_TYPE.equals("Passport"))
            {
                HashMapParams.put("from_image", FRONT_CODE);
                HashMapParams.put("front_name", Constatns.user_id+getdate()+"front.png".replace(" ",""));

            }
            else
            {
                HashMapParams.put("from_image", FRONT_CODE);
                HashMapParams.put("front_name", Constatns.user_id+getdate()+"front.png".replace(" ",""));
                HashMapParams.put("back_image", BACK_CODE);
                HashMapParams.put("back_name", Constatns.user_id+getdate()+"back.png".replace(" ",""));

            }
         //            HashMapParams.put("front_name","asw.png");
//            HashMapParams.put("back_name", "as.png");
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.verification, HashMapParams);
            Server_response=FinalData;
            Log.e("verify",DOC_TYPE+":"+FinalData);



            return FinalData;
        }
    }
    public String getdate()
    {  String currentDate="";

            currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        return currentDate;
    }
}
