package com.axcel.co.coryds.Config;

public class AppConfig {
    // Server user login url
//    public static String BaseURL ="http://axcelworld.com/coryds/mobile_apps_api";
//    public static String BaseURL="https://coryds.com/mobile_apps_api/";
    public static String BaseURL="https://coryds.com/mobile_apps_api/";
//    public static String BaseURL="https://stagging.coryds.com/mobile_apps_api/";

    public static String URL_LOGIN_number = BaseURL+"login_number.php";
    public static String URL_LOGOUT = BaseURL+"logout.php";
    public static String URL_LOGIN = BaseURL+"login.php";
    public static String URL_LOGIN_GMAIL = BaseURL+"signin_gmail.php";
    public static String URL_Signup = BaseURL+"signup.php";
    public static String URL_ALL_CAR_BRANDS = BaseURL+"get_all_car_brands.php";
    public static String URL_ADD_NEW_CAR = BaseURL+"add_new_car.php";
    public static String URL_UPDATE_CAR = BaseURL+"update_car.php";
    public static String URL_UPDATE_PROFILE = BaseURL+"update_profile.php";
    public static String URL_upcommingrides = BaseURL+"rides.php";
    public static String URL_TODAY_RIDES = BaseURL+"today_rides.php";
    public static String URL_Ongoing_RIDES = BaseURL+"ongoing_rides.php";
    public static String URL_SET_Settings = BaseURL+"getsettings.php";
    public static String URL_POST_RIDE = BaseURL+"post_a_ride.php";
    public static String URL_POST_RIDE_For_user = BaseURL+"post_a_ride_driver_for_user.php";
    public static String URL_POST_two_RIDE = BaseURL+"post_two_ride.php";
    public static String URL_EDIT_RIDE = BaseURL+"edit_a_ride.php";
    public static String URL_GET_CARS = BaseURL+"get_cars.php";
    public static String URL_DEL_CARS = BaseURL+"delete_car.php";
    public static String URL_BOOK_RIDE = BaseURL+"book_ride.php";
    public static String URL_SEARCH_RIDE=BaseURL+"search_ride.php";
    public static String URL_RIDE_DETAIL_Completed=BaseURL+"ride_detail_Completed.php";
    public static String URL_GET_RATINGS=BaseURL+"get_ratings.php";
    public static String URL_PICK_CUSTOMER=BaseURL+"pick_customer.php";
    public static String URL_DROP_CUSTOMER=BaseURL+"drop_a_customer.php";
    public static String URL_RIDE_DETAIL=BaseURL+"ride_detail.php";
    public static String URL_BOOKED_RIDE_DETAIL=BaseURL+"booked_ride_detail.php";
    public static String URL_RIDE_REQUESRS=BaseURL+"book_ride_requests.php";
    public static String URL_ENQ=BaseURL+"get_enq.php";
    public static String URL_ACCEPT_REQ=BaseURL+"accept_ride.php";
    public static String URL_PAST_RIDES=BaseURL+"past_rides.php";
    public static String URL_NOTIFICATIONS=BaseURL+"get_notifications.php";
    public static String URL_ALL_CHATS=BaseURL+"get_chat_threads.php";
    public static String URL_GET_PAYMENTS=BaseURL+"get_payments.php";
    public static String URL_ONE_CHATS=BaseURL+"get_single_chat.php";
    public static String URL_SEND_MESSAGE=BaseURL+"send_message.php";
    public static String URL_CHAT_IF_ELSE=BaseURL+"chat_initiate.php";
    public static String verification=BaseURL+"verification.php";
    public static String UPDATE_PROFILE_IMG=BaseURL+"update_profile_pic.php";
    public static String URL_GET_FAQS = BaseURL+"get_faqs.php";
    public static String URL_FORGOT = BaseURL+"forgot.php";
    // Server user register url
    public static String URL_REGISTER = BaseURL+"register.php";
    public static String RATE_CUSTOMER = BaseURL+"rate_a_customer.php";
    public static String URL_verified = BaseURL+"set_number_verified.php";
    public static String BASE_URL="https://coryds.com/uploads/profile/source/";
    public static String BASE_URL_logo=" https://coryds.com/uploads/vehicle/thumbnails/";
}