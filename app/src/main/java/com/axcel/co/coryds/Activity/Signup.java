package com.axcel.co.coryds.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.upload_img;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Signup extends AppCompatActivity {
    private ProgressDialog dialog;
    TextView login;
    Button btn_login;
    EditText Fname,lname,email,password,cpassword,mobile;
    String firstname,lastname,Email,pswd,number,cpswd;
    ImageView pic;
    private final int IMG_REQUEST=1;
    private Bitmap bitmap;
    boolean alreadyexist,verify;
    String isactive,user_admin_status;
    LinearLayout p1,p2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dialog = new ProgressDialog(Signup.this);
        btn_login=(Button)findViewById(R.id.btn_login);
        Fname=(EditText)findViewById(R.id.fname);
        lname=(EditText)findViewById(R.id.lname);
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        cpassword=(EditText)findViewById(R.id.cpassword);
        p1=(LinearLayout)findViewById(R.id.p1);
        p2=(LinearLayout)findViewById(R.id.p2);
        mobile=(EditText)findViewById(R.id.number);
        login=(TextView)findViewById(R.id.login);
        if(getIntent().hasExtra("first_name"))
        {
            Fname.setText(getIntent().getStringExtra("first_name"));;
            lname.setText(getIntent().getStringExtra("last_name"));;
            email.setText(getIntent().getStringExtra("email"));;

            isactive=1+"";
            user_admin_status=1+"";
            password.setText("123");
            cpassword.setText("123");

            p1.setVisibility(View.GONE);
            p2.setVisibility(View.GONE);

        }
        else
        {
            user_admin_status="0";
            isactive=0+"";
        }



        Fname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        });

//        pic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent();
//                intent.setType("*/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"),IMG_REQUEST);
//            }
//        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Signup.this,Login_Activity.class);
                startActivity(i);
                finish();
            }
        });



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 firstname=Fname.getText().toString();
                 lastname=lname.getText().toString();
                 Email=email.getText().toString();
                 pswd=password.getText().toString();
                cpswd=cpassword.getText().toString();
                 number="+92"+mobile.getText().toString();
                if(firstname.length()>0&&lastname.length()>0&& isValidEmailId(Email)&&number.length()==13&&pswd.length()>0&&pswd.equals(cpswd))
                {
                    if(Internet.isInternetAvailable(Signup.this)) {
                        new signup().execute();
                    }
                    else
                    {
                        Toast.makeText(Signup.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {

                    Toast.makeText(Signup.this, "Check Your Credentials", Toast.LENGTH_SHORT).show();

                }





            }
        });



    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        // bye
    }
    class signup extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.is_success=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

            SharedPreferences.Editor editor = getSharedPreferences("LoginValues", MODE_PRIVATE).edit();
            editor.putString("username", " ");
            editor.putString("phone", " ");
            editor.putString("password", " ");
            editor.putString("token", " ");

            editor.apply();
            alreadyexist=false;
                    verify=false;
        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(alreadyexist)
            {
                Toast.makeText(Signup.this, "Email Already Registered", Toast.LENGTH_SHORT).show();
            }
            else if(verify)
            {

                if(getIntent().hasExtra("first_name"))
                {

                    Intent home=new Intent(Signup.this, Login_Activity.class);
                    home.putExtra("email",email.getText().toString());
                    startActivity(home);
                    finish();

                }
                else
                {
                    Toast.makeText(Signup.this, "Check Your Email To Activate", Toast.LENGTH_SHORT).show();
                    Intent home=new Intent(Signup.this, Login_Activity.class);
                    startActivity(home);
                    finish();
                }


            }
            else
            {

            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("email",Email);
            HashMapParams.put("password",pswd);
            HashMapParams.put("fname",firstname);
            HashMapParams.put("lname", lastname);
            HashMapParams.put("mobile",number);
            HashMapParams.put("gender", "1");
            HashMapParams.put("type", "1");
            HashMapParams.put("is_active", isactive);
            HashMapParams.put("user_admin_status", user_admin_status);
            HashMapParams.put("is_number_verified", "0");
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_Signup, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(FinalData.contains("alreadyexist")) {
                alreadyexist=true;
                    Constatns.is_success=true;

            }
            else if(FinalData.contains("success"))
            {
                verify=true;
            }
            else
            {
                Constatns.is_success=false;
            }

            return FinalData;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();

        if (requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri path= data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                pic.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private  void uploadImage(){

        StringRequest stringRequest=new StringRequest(Request.Method.POST, "UploadUrl",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String Response=jsonObject.getString("response");
                            Toast.makeText(Signup.this, Response, Toast.LENGTH_SHORT).show();
                            pic.setImageResource(0);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            protected Map<String,String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("name","");
                params.put("image",imageToString(bitmap));
                return params;
            }
        };
        upload_img.getInstance(Signup.this).addToRequestQue(stringRequest);




    }
    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes=byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes,Base64.DEFAULT);
    }
    public boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
}
