package com.axcel.co.coryds.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Rating_RecyclerAdapter extends RecyclerView.Adapter<Rating_RecyclerAdapter.MyViewHolder> {

    ArrayList<String> USERNAME,Date,LOC,Rate,Feedback,Img,Publish_Date;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView v_image;
        TextView txt_pname,location,Date_time,feedback,published;
                RatingBar rating;

        public MyViewHolder(View view) {
            super(view);
            v_image =  (CircleImageView) view.findViewById(R.id.v_image);
            txt_pname = (TextView) view.findViewById(R.id.txt_pname);
            location = (TextView) view.findViewById(R.id.location);
            Date_time=  (TextView) view.findViewById(R.id.Date_time);
            feedback=  (TextView) view.findViewById(R.id.feedback);
            published=  (TextView) view.findViewById(R.id.published);
            rating=  (RatingBar) view.findViewById(R.id.rating);

        }
    }


    public Rating_RecyclerAdapter(ArrayList USERNAME,ArrayList Date,ArrayList LOC,ArrayList Rate,ArrayList Feedback,
                                  ArrayList Img,ArrayList Publish_Date) {
        this.USERNAME = USERNAME;
        this.Date = Date;
        this.LOC = LOC;
        this.Rate = Rate;
        this.Feedback = Feedback;
        this.Img = Img;
        this.Publish_Date = Publish_Date;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_review, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {



        holder.txt_pname.setText(USERNAME.get(position));
        holder.location.setText(LOC.get(position));
        holder.Date_time.setText(Date.get(position));
        holder.feedback.setText(Feedback.get(position));
        holder.published.setText(Publish_Date.get(position));
        holder.rating.setRating(Float.parseFloat(Rate.get(position)));

        Picasso.get()
                .load(AppConfig.BASE_URL+Img.get(position))
                .into(holder.v_image);





//        final Model movie = moviesList.get(position);
//        holder.title.setText(movie.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
//        holder.message.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.e("message","yes clicked"+movie.getTitle());
//            }
//        });
//        if(movie.getStatus()==0)
//        {
//            holder.message.setText("Rate Rider");
//            holder.status.setImageResource(R.drawable.droped);
//        }
//        else if(movie.getStatus()==1)
//        {
//            holder.message.setText("Message");
//            holder.status.setImageResource(R.drawable.pending);
//
//        }
//        else
//        {
//            holder.message.setText("Request Ride");
//            holder.status.setVisibility(View.INVISIBLE);
//        }
    }

    @Override
    public int getItemCount() {
        return USERNAME.size();
    }
}