package com.axcel.co.coryds.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.axcel.co.coryds.Activity.AddNewCar;
import com.axcel.co.coryds.Activity.EditCar;
import com.axcel.co.coryds.Activity.My_Cars;
import com.axcel.co.coryds.Activity.Verification;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class CarAdd_Adapter extends RecyclerView.Adapter<CarAdd_Adapter.ViewHolder>{
    private ArrayList<AddCar_Model> dataList;
    Context mctx;
    Activity activity;
    String car_id;
    private ProgressDialog dialog;
    String message;
    int myposition;
    public CarAdd_Adapter(ArrayList<AddCar_Model> data, Activity activity)
    {
        this.activity=activity;
        this.dataList = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {


        TextView txt_model,txt_vname,txt_vno,txt_vseats,color;
        ImageView img;
        ImageButton edit,del;


        public ViewHolder(View itemView)
        {
            super(itemView);
            this.img=itemView.findViewById(R.id.v_image);
            this.txt_model=itemView.findViewById(R.id.txt_model);
            this.txt_vname=itemView.findViewById(R.id.txt_vname);
            this.txt_vno =itemView.findViewById(R.id.txt_vno);
            this.color =itemView.findViewById(R.id.color);
            this.edit =itemView.findViewById(R.id.edit);
            this.del =itemView.findViewById(R.id.del);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.addcar_itemview, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
      ;
        holder.txt_model.setText(dataList.get(position).getVmodel());
        holder.txt_vname.setText(dataList.get(position).getVname());
        holder.txt_vno.setText(dataList.get(position).getVno());
        holder.color.setText(dataList.get(position).getColor());

        Picasso.get()
                .load(dataList.get(position).getVimage()).placeholder(R.drawable.default_car)
                .into(holder.img);
        holder.edit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(activity, EditCar.class);
                i.putExtra("id",dataList.get(position).getId());
                i.putExtra("number",dataList.get(position).getVno());
                i.putExtra("name",dataList.get(position).getVname());
                i.putExtra("brand",dataList.get(position).getVmodel());
                i.putExtra("color",dataList.get(position).getColor());
                i.putExtra("comfort",dataList.get(position).getComfort());
                i.putExtra("image",dataList.get(position).vimage);
                activity.startActivity(i);
//                Toast.makeText(activity, "In Progress", Toast.LENGTH_SHORT).show();
            }
        });
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myposition=position;
           dialog = new ProgressDialog(activity);
                car_id=dataList.get(position).getId();
                if(Internet.isInternetAvailable(activity)) {
                    new Delete_A_Car().execute();
                }
                else
                {
                    Toast.makeText(activity, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
    class Delete_A_Car extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();



        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            Toast.makeText(activity, message+"", Toast.LENGTH_SHORT).show();
if(message.contains("Deleted"))
{
//    Toast.makeText(activity, message+"", Toast.LENGTH_SHORT).show();

    dataList.remove(myposition);
   notifyDataSetChanged();
}

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("vehicle_id",car_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_DEL_CARS, HashMapParams);
            message=FinalData;
            Log.e("responsemehamddelete",FinalData);
            if(!FinalData.contains("error")) {


            }
            else
            {

            }

            return FinalData;
        }
    }
    @Override
    public int getItemCount()
    {
        return dataList.size();
    }
}
