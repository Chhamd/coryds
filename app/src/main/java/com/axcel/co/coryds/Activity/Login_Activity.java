package com.axcel.co.coryds.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.FlingAnimation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public class Login_Activity extends AppCompatActivity {
    Button btn_login;
    EditText username,password;
    TextView signup;
    String email,pswd;
    private ProgressDialog dialog;
String token;
    private ProgressBar progressBar;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;

    SignInButton googleSignInButton;
    GoogleSignInClient googleSignInClient;
    String First_Name,Last_Name,Email;
    TextView txt_forgot;
    String Email_new="";
    boolean is_mail_sent;
    boolean isreg;
    String Phone_Number;
//    CallbackManager callbackManager;
    RelativeLayout bgImg ;
    RelativeLayout layout;
    TextInputEditText mobEt;
    float height;
    boolean isAnimating;
    Button fab;
    Button back;

RelativeLayout laylast;
TextView google;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.login);
        setContentView(R.layout.login_demo);

//        loading=findViewById(R.id.load);
//        loading.setVisibility(View.GONE);

//        callbackManager = CallbackManager.Factory.create();

        overridePendingTransition(R.anim.slideinright, R.anim.slideoutright);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dialog = new ProgressDialog(Login_Activity.this);
//        btn_login=(Button)findViewById(R.id.btn_login);
//        username=(EditText)findViewById(R.id.username);
//        password=(EditText)findViewById(R.id.password);
        txt_forgot=findViewById(R.id.txt_forgot);
        signup=(TextView)findViewById(R.id.btn_signup);
        googleSignInButton = findViewById(R.id.sign_in_button);

        bgImg=findViewById(R.id.bg);
        layout=findViewById(R.id.layout);
        mobEt=findViewById(R.id.mob_et);
        fab=findViewById(R.id.registerFab);
        laylast=findViewById(R.id.laylast);
        google=findViewById(R.id.google);
        fab.setVisibility(View.GONE);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels-metrics.heightPixels/50.0f;

        mobEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAnimating) {
                    isAnimating = true;

                    animate(-1, 0f);
                }
            }
        });
//


        //
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Phone_Number="+92"+mobEt.getText().toString();
                if(Phone_Number.length()==13)
                {
                    Constatns.islogedin=false;
                    if(Internet.isInternetAvailable(Login_Activity.this)) {
                        new checklogin_number().execute();


                    }
                    else
                    {
                        Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }else
                {
                    Toast.makeText(Login_Activity.this, "Please Enter Valid Credentials", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        mobEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//
//            }
//        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = "1929262";
            String channelName = "dsf";
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {

                            return;
                        }

                        // Get new Instance ID token
                         token = task.getResult().getToken();
                        Log.e("firebaseme",token);
                        SharedPreferences prefs = getSharedPreferences("LoginValues", MODE_PRIVATE);
                        String username1 = prefs.getString("phone", " ");//"No name defined" is the default value.
                        String email = prefs.getString("username", " ");
//                        String password1 = prefs.getString("password", " ");//"No name defined" is the default value.
//                        Log.e("myvalues",username1+":"+password1);
                        if(username1.length()>3)
                        {
//                            Log.e("myvalues",username1+":"+password1);
                            updateall();
                            Phone_Number=username1;
//                            email=username1;
//                            pswd=password1;
                            Constatns.islogedin=false;

                            if(Internet.isInternetAvailable(Login_Activity.this))
                            {

                                new checklogin_number().execute();
//                                new checklogin().execute();

                            }
                            else
                            {
                                Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }



                        }
                        else if(getIntent().hasExtra("email"))
                        {
                            Email=getIntent().getStringExtra("email");
                            if(Internet.isInternetAvailable(Login_Activity.this))
                            {
                                new Signin_with_gmail().execute();
                            }
                            else
                            {
                                Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                            }


                        }
                        else if(email.length()>3)
                        {
                            Email=email;
                            if(Internet.isInternetAvailable(Login_Activity.this))
                            {
                                new Signin_with_gmail().execute();
                            }
                            else
                            {
                                Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                            }

                        }



                    }
                });





     /* btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Phone_Number="+92"+username.getText().toString();
//                email=username.getText().toString();
//                pswd=password.getText().toString();
                if(Phone_Number.length()==13)
                {
                    Constatns.islogedin=false;
                    if(Internet.isInternetAvailable(Login_Activity.this)) {
                        new checklogin_number().execute();
//                        new checklogin().execute();

                    }
                    else
                    {
                        Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }else
                {
                    Toast.makeText(Login_Activity.this, "Please Enter Valid Credentials", Toast.LENGTH_SHORT).show();
                }


            }
        });
*/
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(Login_Activity.this,Signup.class);
                startActivity(home);
                finish();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 101);
            }
        });
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 101);
            }
        });

        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_mail_sent=false;
                final View Text_b = View.inflate(Login_Activity.this, R.layout.forgot_password, null);
                final EditText mail =  Text_b.findViewById(R.id.mail);
//                mail.setText("Email");

                AlertDialog.Builder builder = new AlertDialog.Builder(Login_Activity.this);
//                builder.setTitle("Please Enter Your Email");
                builder
                        .setView(Text_b)
                        .setCancelable(false)
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if(isValidEmailId(mail.getText().toString()))
                                {
                                    Email_new=mail.getText().toString();
                                    if(Internet.isInternetAvailable(Login_Activity.this)) {
                                        new forgot().execute();
                                    }
                                    else
                                    {
                                        Toast.makeText(Login_Activity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                                    }



                                }
                                else
                                {
                                    Toast.makeText(Login_Activity.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
                                }

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
            }
        });

/*

//     String EMAIL = "email";

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("checfb1",loginResult.toString());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e("checfb2",exception.toString());
            }
        });

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("checfb2",loginResult.toString());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("checfb2",exception.toString());
                    }
                });
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        Log.e("checfb23",isLoggedIn+"");*/

    }
    @Override
    public void onBackPressed() {

        if(!isAnimating){
//            super.onBackPressed();
        }
        else{
            animate(1,1f);
            isAnimating=false;
            mobEt.clearFocus();
//            googleSignInButton.setVisibility(View.VISIBLE);
//            googleSignInButton.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            laylast.setVisibility(View.VISIBLE);
            google.setVisibility(View.VISIBLE);
        }
    }
    private void animate(int direction, float alpha) {
        googleSignInButton.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
        laylast.setVisibility(View.GONE);
        google.setVisibility(View.GONE);
        FlingAnimation flingX =
                new FlingAnimation(layout, DynamicAnimation.Y)
                        .setStartVelocity(direction*height)
                        .setFriction(0.5f);
        flingX.start();

        bgImg.animate().alpha(alpha).setDuration(1000).start();

    }
    class forgot extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");
            is_mail_sent=false;

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(is_mail_sent)
            {
                Toast.makeText(Login_Activity.this, "New Password Is Sent To Your Email", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(Login_Activity.this, "Please Try Again Later", Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("email",Email_new);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_FORGOT, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                is_mail_sent=true;


            }
            else
            {
                is_mail_sent=false;
            }

            return FinalData;
        }
    }
    public boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case 101:
                    try {
                        // The Task returned from this call is always completed, no need to attach
                        // a listener.
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                        GoogleSignInAccount account = task.getResult(ApiException.class);
                        onLoggedIn(account);
                    } catch (ApiException e) {
                        // The ApiException status code indicates the detailed failure reason.
                        Log.w("me", "signInResult:failed code=" + e.getStatusCode());
                    }
                    break;
            }
//        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {

        Log.e("profileImage",googleSignInAccount.getPhotoUrl()+"");
        Log.e("profileName",googleSignInAccount.getDisplayName()+"");
        Log.e("profileEmail",googleSignInAccount.getEmail()+"");
        String[] temp=googleSignInAccount.getDisplayName().split(" ");

        First_Name=temp[0];
        Last_Name=temp[1];
        Email=googleSignInAccount.getEmail()+"";

        if(Internet.isInternetAvailable(Login_Activity.this)) {
            new Signin_with_gmail().execute();
        }
        else
        {
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }
    class checklogin_number extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
//            loading.setVisibility(View.VISIBLE);
            super.onPreExecute();
            Log.e("called","yes00");
//            myalert(1);

        }

        @Override
        protected void onPostExecute(String string1) {
//            loading.setVisibility(View.GONE);
            dialog.dismiss();
            super.onPostExecute(string1);
//            myalert(0);
            if(!Constatns.islogedin)
            {

                Toast.makeText(Login_Activity.this, "Invalid Phone Number", Toast.LENGTH_SHORT).show();
            }else
            {
                SharedPreferences.Editor editor = getSharedPreferences("LoginValues", MODE_PRIVATE).edit();
                editor.putString("phone", Phone_Number);
//                editor.putString("password", pswd);
                editor.putString("token", token);

                editor.apply();
                if(Constatns.user_type.contains("2"))
                {

                    Toast.makeText(Login_Activity.this, "Please Use Customer App", Toast.LENGTH_SHORT).show();
                }
                else if(Constatns.user_type.contains("1"))
                {
                    if(Constatns.is_active.contains("0")) {
                        Toast.makeText(Login_Activity.this, "Check Your Email To Activate Account", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            checkPermission();
                        }
                        else
                        {
                            if(Constatns.is_number_verified.contains("0"))
                            {
                                Intent home=new Intent(Login_Activity.this, PhoneVerification.class);
                                startActivity(home);
                                finish();
                            }
                            else
                            {
                                Intent home=new Intent(Login_Activity.this, MainActivity_Driver.class);
                                home.putExtra("flag",0);
                                startActivity(home);
                                finish();
                            }

                        }

                    }



                }

            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("phone",Phone_Number);
//            HashMapParams.put("password", pswd);
            HashMapParams.put("token", token);
            Log.e("token",Phone_Number+""+token);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_LOGIN_number, HashMapParams);
Log.e("checl",FinalData);
            Log.e("responsemehamdnum",FinalData);
            if(!FinalData.contains("Invalid username or password")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Constatns.email= c.getString("username");;
                        Constatns.fname= c.getString("fname");
                        Constatns.lname= c.getString("lname");
                        Constatns.type= c.getString("type");
                        Constatns.mobile= c.getString("mobile");
                        Constatns.about= c.getString("about");
                        Constatns.dob= c.getString("dob");
                        Constatns.user_id= c.getString("user_id");
                        Constatns.user_type= c.getString("user_type");
                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));
                        Constatns.is_food= Integer.parseInt(c.getString("is_food"));
                        Constatns.dp= c.getString("dp");
                        Constatns.rating= c.getString("rating");
                        Constatns.reviews= c.getString("reviews");
                        Constatns.is_form_submit=c.getString("is_form_submit");
                        Constatns.is_admin_approved=c.getString("is_admin_approved");
                        Constatns.is_verified=c.getString("is_verified");

                        Constatns.is_number_verified= c.getString("is_number_verified");
                        Constatns.is_active= c.getString("is_active");


                        Constatns.faq=c.getString("faq");
                        Constatns.terms=c.getString("terms");
                        Constatns.privacy=c.getString("privacy");
                        Constatns.contact=c.getString("contact");
                        Constatns.notifications=Integer.parseInt(c.getString("notifications"));
                        Constatns.rides=Integer.parseInt(c.getString("rides"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }
    class checklogin extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(!Constatns.islogedin)
            {

                Toast.makeText(Login_Activity.this, "Invalid Email Or Password", Toast.LENGTH_SHORT).show();
            }else
            {
                SharedPreferences.Editor editor = getSharedPreferences("LoginValues", MODE_PRIVATE).edit();
                editor.putString("username", email);
                editor.putString("password", pswd);
                editor.putString("token", token);

                editor.apply();
                if(Constatns.user_type.contains("2"))
                {

                    Toast.makeText(Login_Activity.this, "Please Use Customer App", Toast.LENGTH_SHORT).show();
                }
                else if(Constatns.user_type.contains("1"))
                {
                    if(Constatns.is_active.contains("0")) {
                        Toast.makeText(Login_Activity.this, "Check Your Email To Activate Account", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            checkPermission();
                        }
                        else
                        {

                            Intent home=new Intent(Login_Activity.this, MainActivity_Driver.class);
                            home.putExtra("flag",0);
                            startActivity(home);
                            finish();
                        }

                    }



                }

            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("email",email);
            HashMapParams.put("password", pswd);
            HashMapParams.put("token", token);
            Log.e("token",token);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_LOGIN, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("Invalid username or password")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Constatns.email= email;
                        Constatns.fname= c.getString("fname");
                        Constatns.lname= c.getString("lname");
                        Constatns.type= c.getString("type");
                        Constatns.mobile= c.getString("mobile");
                        Constatns.about= c.getString("about");
                        Constatns.dob= c.getString("dob");
                        Constatns.user_id= c.getString("user_id");
                        Constatns.user_type= c.getString("user_type");
                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));
                        Constatns.is_food= Integer.parseInt(c.getString("is_food"));
                        Constatns.dp= c.getString("dp");
                        Constatns.rating= c.getString("rating");
                        Constatns.reviews= c.getString("reviews");
                        Constatns.is_form_submit=c.getString("is_form_submit");
                        Constatns.is_admin_approved=c.getString("is_admin_approved");
                        Constatns.is_verified=c.getString("is_verified");

                        Constatns.is_number_verified= c.getString("is_number_verified");
                        Constatns.is_active= c.getString("is_active");


                        Constatns.faq=c.getString("faq");
                        Constatns.terms=c.getString("terms");
                        Constatns.privacy=c.getString("privacy");
                        Constatns.contact=c.getString("contact");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }

    public void updateall()
    {
        Constatns.is_refresh=false;
        Constatns.rides=0;
        Constatns.notifications=0;
        Constatns.rating="";
        Constatns.reviews="";
        Constatns.email="";
        Constatns.fname="";
        Constatns.lname="";
        Constatns.type="";
        Constatns.mobile="";
        Constatns.user_id="";
        Constatns.about="";
        Constatns.dob="";
        Constatns.user_type="";
        Constatns.islogedin=false;
        Constatns.isposted=false;
        Constatns.is_success=false;
        Constatns.is_driver=false;
        Constatns.is_booked=false;
        Constatns.is_number_verified="";
        Constatns.is_active="";
        Constatns.is_form_submit="";
        Constatns.is_admin_approved="";
        Constatns.is_verified="";
        Constatns.faq="";
        Constatns.terms="";
        Constatns.privacy="";
        Constatns.contact="";
        Constatns.actitve_ride_index=0;
    }
    @Override
    public void onDestroy() {


        super.onDestroy();
    }
    protected void checkPermission(){
        if(ContextCompat.checkSelfPermission(Login_Activity.this, Manifest.permission.CAMERA)

                + ContextCompat.checkSelfPermission(
                Login_Activity.this,Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){

            // Do something, when permissions not granted
            if(ActivityCompat.shouldShowRequestPermissionRationale(
                    Login_Activity.this,Manifest.permission.CAMERA)

                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    Login_Activity.this,Manifest.permission.ACCESS_FINE_LOCATION)){
                // If we should give explanation of requested permissions

                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(Login_Activity.this);
                builder.setMessage("Camera and Location permissions are required to use this app.");
                builder.setTitle("Please grant those permissions");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                Login_Activity.this,
                                new String[]{
                                        Manifest.permission.CAMERA,

                                        Manifest.permission.ACCESS_FINE_LOCATION
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNeutralButton("Cancel",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        Login_Activity.this,
                        new String[]{
                                Manifest.permission.CAMERA,

                                Manifest.permission.ACCESS_FINE_LOCATION
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        }else {
            if(Constatns.is_number_verified.contains("0"))
            {
                Intent home=new Intent(Login_Activity.this, PhoneVerification.class);
                startActivity(home);
                finish();
            }
            else
            {
                Intent home=new Intent(Login_Activity.this, MainActivity_Driver.class);
                home.putExtra("flag",0);
                startActivity(home);
                finish();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CODE:{
                // When request is cancelled, the results array are empty
                if(
                        (grantResults.length >0) &&
                                (grantResults[0]
                                        + grantResults[1]

                                        == PackageManager.PERMISSION_GRANTED
                                )
                ){
                    if(Constatns.is_number_verified.contains("0"))
                    {
                        Intent home=new Intent(Login_Activity.this, PhoneVerification.class);
                        startActivity(home);
                        finish();
                    }
                    else
                    {
                        Intent home=new Intent(Login_Activity.this, MainActivity_Driver.class);
                        home.putExtra("flag",0);
                        startActivity(home);
                        finish();
                    }

                }else {
                    // Permissions are denied
                    Toast.makeText(Login_Activity.this,"Permissions denied.",Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
    class Signin_with_gmail extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");
            Log.e("signinwith_gmail","yes");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(!Constatns.islogedin)
            {

//                Toast.makeText(Login_Activity.this, "Invalid EMAIL OR PASSWORD", Toast.LENGTH_SHORT).show();
                Intent home=new Intent(Login_Activity.this,Signup.class);
                home.putExtra("first_name",First_Name);
                home.putExtra("last_name",Last_Name);
                home.putExtra("email",Email);
                startActivity(home);
//                new signup().execute();
            }else
            {
                SharedPreferences.Editor editor = getSharedPreferences("LoginValues", MODE_PRIVATE).edit();
                editor.putString("username", Email);
                editor.putString("password", "");
                editor.putString("token", token);
                ;

                editor.apply();
                if(Constatns.user_type.contains("2"))
                {

                    Toast.makeText(Login_Activity.this, "Please Use Customer App", Toast.LENGTH_SHORT).show();
                }
                else if(Constatns.user_type.contains("1"))
                {
                    if(Constatns.is_active.contains("0")) {
                        Toast.makeText(Login_Activity.this, "Check Your Email To Activate Account", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            checkPermission();
                        }
                        else
                        {

                            if(Constatns.is_number_verified.contains("0"))
                            {
                                Intent home=new Intent(Login_Activity.this, PhoneVerification.class);
                                startActivity(home);
                                finish();
                            }
                            else
                            {
                                Intent home=new Intent(Login_Activity.this, MainActivity_Driver.class);
                                home.putExtra("flag",0);
                                startActivity(home);
                                finish();
                            }
                        }

                    }



                }

            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("email",Email);
            HashMapParams.put("token", token);
            if(token.length()>0)
            { Log.e("token",Email+token);}


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_LOGIN_GMAIL, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("Invalid username or password")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Constatns.email= Email;
                        Constatns.fname= c.getString("fname");
                        Constatns.lname= c.getString("lname");
                        Constatns.type= c.getString("type");
                        Constatns.mobile= c.getString("mobile");
                        Constatns.about= c.getString("about");
                        Constatns.dob= c.getString("dob");
                        Constatns.user_id= c.getString("user_id");
                        Constatns.user_type= c.getString("user_type");
                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));
                        Constatns.is_food= Integer.parseInt(c.getString("is_food"));
                        Constatns.dp= c.getString("dp");
                        Constatns.rating= c.getString("rating");
                        Constatns.reviews= c.getString("reviews");
                        Constatns.is_form_submit=c.getString("is_form_submit");
                        Constatns.is_admin_approved=c.getString("is_admin_approved");
                        Constatns.is_verified=c.getString("is_verified");

                        Constatns.is_number_verified= c.getString("is_number_verified");
                        Constatns.is_active= c.getString("is_active");


                        Constatns.faq=c.getString("faq");
                        Constatns.terms=c.getString("terms");
                        Constatns.privacy=c.getString("privacy");
                        Constatns.contact=c.getString("contact");
                        Constatns.notifications=Integer.parseInt(c.getString("notifications"));

                        Constatns.rides=Integer.parseInt(c.getString("rides"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }
    class signup extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.is_success=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");
            Log.e("signinwith_gmail:signup","yes");
            isreg=false;
        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
if(isreg)
{
    new Signin_with_gmail().execute();
}
else
{
    Toast.makeText(Login_Activity.this, "Server Error", Toast.LENGTH_SHORT).show();
}


         //
        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("email",Email);
            HashMapParams.put("password", "");
            HashMapParams.put("fname",First_Name);
            HashMapParams.put("lname", Last_Name);
            HashMapParams.put("mobile","");
            HashMapParams.put("gender", "1");
            HashMapParams.put("type", "1");
            HashMapParams.put("is_active", "1");
            HashMapParams.put("user_admin_status", "1");
            HashMapParams.put("is_number_verified", "1");
            Log.e("checme;",Email+"yes");
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_Signup, HashMapParams);

            Log.e("responsemehamd2",FinalData);
            if(FinalData.contains("success"))
            {
                isreg=true;
            }


            return FinalData;
        }
    }
    private void sendFCMPush() {

        final String Legacy_SERVER_KEY = "AAAAiiuutQo:APA91bEZ8e33STTv6IOYVdgATsxjcsniHd1j3wL7L5VFYeQ4_gH2BkFt49QM7vJw4TPFlZhhmrhU_BjTuOhQuHsfmNWrG5WziJ99RBuCed_UegLmkoxVwa7KdnX8IxiIMga32pdfxtNh";
        String msg = "just checking";
        String title = "my hello";
       final String token = "dFWJCnFmTWml4VxacET66N:APA91bHgJh2ZS4dOj1Y5TmDjz-XTyIAFgahNiNWQJdasHb-IrxZlBW7mrqQaCsTXGDnc5xhQ3ZUj_-HSgOsz6inshW2PlA2-NmQplkZZE9YldJBL7IxPyuJbNzPvIdCH5-TCRZbdilF6";

        JSONObject obj = null;
        JSONObject objData = null;
        JSONObject dataobjData = null;

        try {
            obj = new JSONObject();
            objData = new JSONObject();

            objData.put("body", msg);
            objData.put("title", title);
            objData.put("action", "dsfdsfdsfds");
            objData.put("sound", "default");
            objData.put("icon", "icon_name"); //   icon_name image must be there in drawable
            objData.put("tag", token);
            objData.put("priority", "high");

            dataobjData = new JSONObject();
            dataobjData.put("text", msg);
            dataobjData.put("title", title);

            obj.put("to", token);
            //obj.put("priority", "high");

            obj.put("notification", objData);
            obj.put("data", dataobjData);
            Log.e("!_@rj@_@@_PASS:>", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, "https://fcm.googleapis.com/", obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("!_@@_SUCESS", response + "");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("!_@@_Errors--", error + "");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "key=" + Legacy_SERVER_KEY);
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 1000 * 60;// 60 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsObjRequest.setRetryPolicy(policy);
        requestQueue.add(jsObjRequest);
    }
}
