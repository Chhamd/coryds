package com.axcel.co.coryds.ChildFragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.axcel.co.coryds.Activity.Login_Activity;
import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Activity.Signup;
import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class about extends Fragment {

    private List<Model> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerAdapter mAdapter;
    EditText name,last_name,email,mobile,about;
    TextView Dob;
    ProgressDialog dialog;
    Button update;
    String firstname,lastname,Email,number,about_,dob;
    DatePickerDialog.OnDateSetListener DOB;
    Button edit;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.about,container,false);
        name=(EditText)v.findViewById(R.id.first_name);;
        edit=v.findViewById(R.id.edit);


        last_name=v.findViewById(R.id.last_name);
        Dob=v.findViewById(R.id.dob);
        email=(EditText)v.findViewById(R.id.edt_email);
        mobile=(EditText)v.findViewById(R.id.edt_number);
        about=(EditText)v.findViewById(R.id.about);
        dialog = new ProgressDialog(getActivity());
        update=v.findViewById(R.id.update);
        update.setVisibility(View.GONE);
        edit.setVisibility(View.VISIBLE);
        name.setText(Constatns.fname);
        last_name.setText(Constatns.lname);
        Dob.setText(Constatns.dob);
        email.setText(Constatns.email);
        mobile.setText(Constatns.mobile);
        about.setText(Constatns.about);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        final Calendar myCalendar = Calendar.getInstance();

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.setVisibility(View.INVISIBLE);
                update.setVisibility(View.VISIBLE);
                name.setFocusableInTouchMode(true);
                name.setFocusable(true);
                name.setSelection(name.getText().toString().length());

                last_name.setFocusableInTouchMode(true);
                last_name.setFocusable(true);

                email.setFocusableInTouchMode(false);
                email.setFocusable(false);

                mobile.setFocusableInTouchMode(false);
                mobile.setFocusable(false);

                about.setFocusableInTouchMode(true);
                about.setFocusable(true);
                about.setBackgroundResource(R.drawable.bg);

                mobile.setBackground(null);
                email.setBackground(null);
                        last_name.setBackgroundResource(R.drawable.bg);
                name.setBackgroundResource(R.drawable.bg);

            }
        });
        DOB = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub




                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //   String myFormat = "yyyy/mm/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                Dob.setText(sdf.format(myCalendar.getTime()));
            }

        };
        Dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), DOB, myCalendar


                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),

                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().length()>0)
                { firstname=name.getText().toString();}
                if(last_name.getText().toString().length()>0)
                { lastname=last_name.getText().toString();}
//                if(email.getText().toString().length()>0)
//                { Email=email.getText().toString();}

                if(about.getText().toString().length()>0)
                {about_=about.getText().toString();}
                else
                {
                    about_="";
                }
                if(Dob.getText().toString().length()>0)
                {dob=Dob.getText().toString();}


//                if(firstname.length()>0&&lastname.length()>0&&Email.length()>0&number.length()>10&&about.length()>0&&dob.length()>0)
//                {
                 //
                if(Internet.isInternetAvailable(getActivity())) {
                    new Update_Profile().execute();
                }
                else
                {
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }


//                Toast.makeText(getActivity(), "Somethis is wrong with Api", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        return v;
    }
    class Update_Profile extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.is_success=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(!Constatns.is_success)
            {




                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
            }else
            {
                update.setVisibility(View.GONE);
                edit.setVisibility(View.VISIBLE);
                name.setFocusableInTouchMode(false);
                name.setFocusable(false);


                last_name.setFocusableInTouchMode(false);
                last_name.setFocusable(false);

                email.setFocusableInTouchMode(false);
                email.setFocusable(false);

                mobile.setFocusableInTouchMode(false);
                mobile.setFocusable(false);

                about.setFocusableInTouchMode(false);
                about.setFocusable(false);

                about.setBackground(null);

                mobile.setBackground(null);
                email.setBackground(null);
                last_name.setBackground(null);
                name.setBackground(null);


//                Intent home=new Intent(Signup.this, Login_Activity.class);
//                startActivity(home);
//                finish();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();

            HashMapParams.put("user_id",Constatns.user_id);

            HashMapParams.put("fname",firstname);
            HashMapParams.put("lname",lastname);

            HashMapParams.put("about",about_);
            HashMapParams.put("dob",dob);
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_UPDATE_PROFILE, HashMapParams);

            Log.e("responsemehamdprofile",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.is_success=true;
              Constatns.fname=firstname;
             Constatns.lname=lastname;
                  Constatns.dob=dob;

//                  Constatns.mobile=number;
                  Constatns.about=about_;

                    char first = firstname.charAt(0);
                    Constatns.dp=first+".png".toLowerCase();
                    JSONObject obj = new JSONObject(FinalData);
//                    JSONArray categories = obj.getJSONArray("status");
//                    for (int i = 0; i < categories.length(); i++) {
//                        JSONObject c = categories.getJSONObject(i);
//
//                        Constatns.email= c.getString("email");
//                        Constatns.fname= c.getString("fname");
//                        Constatns.lname= c.getString("lname");
//                        Constatns.type= c.getString("type");
//                        Constatns.mobile= c.getString("mobile");
//                        Constatns.gender= c.getString("gender");
//
//
//
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.is_success=false;
            }

            return FinalData;
        }
    }

}