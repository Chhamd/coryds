package com.axcel.co.coryds.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Activity.Login_Activity;
import com.axcel.co.coryds.Activity.MainActivity_Driver;
import com.axcel.co.coryds.Adapters.single_chat_users_dataper;
import com.axcel.co.coryds.ChildFragments.Ride_requests;
import com.axcel.co.coryds.Config.BottomMenuHelper;
import com.axcel.co.coryds.Config.Chat_Model;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.MainFragments.MyRequest_Fragment_Driver;
import com.axcel.co.coryds.R;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.e("mynoti", "onMessageReceived: " + remoteMessage.getNotification().getTag());


        Log.d("msg", "onMessageReceived: " + remoteMessage.getNotification().getBody());
        Intent i = new Intent(this, MainActivity_Driver.class);
        if(remoteMessage.getNotification().getBody().contains("request"))
        {

             i=new Intent(this, MainActivity_Driver.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            Constatns.rides=Constatns.rides+1;
            i.putExtra("flag",1);
            startActivity(i);
        }     else if(remoteMessage.getNotification().getBody().contains("cancelled"))
        {

             i=new Intent(this, MainActivity_Driver.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            i.putExtra("flag",1);
            startActivity(i);
        }
        else if(remoteMessage.getNotification().getBody().contains("message"))
        {

             i=new Intent(this, MainActivity_Driver.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            i.putExtra("flag",2);
            startActivity(i);
        }

        else if(remoteMessage.getNotification().getBody().contains("enquiry"))
        {

             i=new Intent(this, MainActivity_Driver.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            i.putExtra("flag",1);
            startActivity(i);
        }
        else if(remoteMessage.getNotification().getBody().contains("payment"))
        {

             i=new Intent(this, MainActivity_Driver.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            i.putExtra("flag",4);
            startActivity(i);
        }

//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "1929262";
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody()).setAutoCancel(true).setContentIntent(pendingIntent);;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        manager.notify(0, builder.build());
//        Log.e("message",remoteMessage.getNotification().getBody().subSequence(remoteMessage.getNotification().getBody().indexOf("1")+1,remoteMessage.getNotification().getBody().lastIndexOf("1"))+"");

//
    if(remoteMessage.getNotification().getBody().contains("request"))
    {

        Intent p=new Intent(this, MainActivity_Driver.class);
        p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Constatns.rides=Constatns.rides+1;
        p.putExtra("flag",1);
        startActivity(p);
    }     else if(remoteMessage.getNotification().getBody().contains("cancelled"))
    {

        Intent p=new Intent(this, MainActivity_Driver.class);
        p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        p.putExtra("flag",1);
        startActivity(p);
    }
            else if(remoteMessage.getNotification().getBody().contains("message"))
        {

            Intent p=new Intent(this, MainActivity_Driver.class);
            p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            p.putExtra("flag",2);
            startActivity(p);
        }

    else if(remoteMessage.getNotification().getBody().contains("enquiry"))
    {

        Intent p=new Intent(this, MainActivity_Driver.class);
        p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        p.putExtra("flag",1);
        startActivity(p);
    }
    else if(remoteMessage.getNotification().getBody().contains("payment"))
    {

        Intent p=new Intent(this, MainActivity_Driver.class);
        p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        p.putExtra("flag",4);
        startActivity(p);
    }
    else if(remoteMessage.getNotification().getBody().contains("rated"))
    {

        Intent p=new Intent(this, MainActivity_Driver.class);
        p.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        p.putExtra("flag",4);
        startActivity(p);
    }
    }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d("firebase4", "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
        // [START dispatch_job]
//        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
//                .build();
//        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, Login_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "1929262";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.icon)
                        .setContentTitle("message")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}