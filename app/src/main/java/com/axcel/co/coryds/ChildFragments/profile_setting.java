package com.axcel.co.coryds.ChildFragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class profile_setting extends Fragment {
    Switch is_number,is_sms,is_chat,is_pets,is_music,is_smoking,is_food;
    private ProgressDialog dialog;
    Button save;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.profile_setting,container,false);
        is_number=(Switch)v.findViewById(R.id.isnumber);
        is_sms=(Switch)v.findViewById(R.id.issms);
        is_chat=(Switch)v.findViewById(R.id.iscaht);
        is_pets=(Switch)v.findViewById(R.id.ispets);
        is_music=(Switch)v.findViewById(R.id.ismusic);
        is_smoking=(Switch)v.findViewById(R.id.issmoking);
        is_food=(Switch)v.findViewById(R.id.food);
        dialog = new ProgressDialog(getActivity());
        check_switches();
        save=(Button)v.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new update_setting().execute();
            }
        });



        return v;
    }
    public void check_switches()
    {
        if( Constatns.is_number==1)
        {
            is_number.setChecked(true);
        }
        else {
            is_number.setChecked(false);
        }
        if( Constatns.is_chat==1)
        {
            is_chat.setChecked(true);
        }
        else {
            is_chat.setChecked(false);
        }
        if( Constatns.is_music==1)
        {
            is_music.setChecked(true);
        }
        else {
            is_music.setChecked(false);
        }
        if( Constatns.is_pets==1)
        {
            is_pets.setChecked(true);
        }
        else {
            is_pets.setChecked(false);
        }
        if( Constatns.is_smoking==1)
        {
            is_smoking.setChecked(true);
        }
        else {
            is_smoking.setChecked(false);
        }
        if( Constatns.is_sms==1)
        {
            is_sms.setChecked(true);
        }
        else {
            is_sms.setChecked(false);
        }

        if(Constatns.is_food==1)
        {
            is_food.setChecked(true);
        }
        else {
            is_food.setChecked(false);
        }
    }
    public String getisnumber()
    {
      if(is_number.isChecked())
      {
          Constatns.is_number=1;
          return "1";
      }
      else {
          Constatns.is_number=0;
          return "0";
      }


    }
    public String getsms()
    {
        if(is_sms.isChecked())
        {
            Constatns.is_sms=1;
            return "1";
        }
        else {
            Constatns.is_sms=0;
            return "0";
        }


    }
    public String getsmoke()
    {
        if(is_smoking.isChecked())
        {
            Constatns.is_smoking=1;
            return "1";
        }
        else {
            Constatns.is_smoking=0;
            return "0";
        }


    }
    public String getpet()
    {
        if(is_pets.isChecked())
        {

            Constatns.is_pets=1;
            return "1";
        }
        else {
            Constatns.is_pets=0;
            return "0";
        }


    }
    public String getchat()
    {
        if(is_chat.isChecked())
        {
            Constatns.is_chat=1;
            return "1";
        }
        else {
            Constatns.is_chat=0;
            return "0";
        }


    }
    public String getmusic()
    {
        if(is_music.isChecked())
        {
            Constatns.is_music=1;
            return "1";
        }
        else {
            Constatns.is_music=0;
            return "0";
        }


    }
    public String get_food()
    {
        if(is_food.isChecked())
        {
            Constatns.is_food=1;
            return "1";
        }
        else {
            Constatns.is_food=0;
            return "0";
        }


    }
    class update_setting extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);



        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("email",Constatns.email);
            HashMapParams.put("sms",getsms());
            HashMapParams.put("smoke", getsmoke());
            HashMapParams.put("pet",getpet());
            HashMapParams.put("chat", getchat());
            HashMapParams.put("mobile",getisnumber());
            HashMapParams.put("music", getmusic());
            HashMapParams.put("food", get_food());
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_SET_Settings, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);



                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }

}
