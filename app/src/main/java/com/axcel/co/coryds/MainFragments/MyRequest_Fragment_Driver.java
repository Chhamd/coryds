package com.axcel.co.coryds.MainFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.axcel.co.coryds.Adapters.MyRide_PagerAdapter;
import com.axcel.co.coryds.ChildFragments.Ride_enquiries;
import com.axcel.co.coryds.ChildFragments.Ride_requests;
import com.axcel.co.coryds.R;
import com.google.android.material.tabs.TabLayout;

public class MyRequest_Fragment_Driver extends Fragment {
    TabLayout tab_myride;
    ViewPager mviewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.myrequest_fragment_driver,container,false);
        tab_myride=v.findViewById(R.id.tab_myride);
        mviewPager=v.findViewById(R.id.mviewPager);
       MyRide_PagerAdapter adapter=new MyRide_PagerAdapter(getChildFragmentManager());
        adapter.addNewTab("Requests",new Ride_requests());
        adapter.addNewTab("Enquries",new Ride_enquiries());
         mviewPager.setAdapter(adapter);
        tab_myride.setupWithViewPager(mviewPager);


        return v;
    }
}
