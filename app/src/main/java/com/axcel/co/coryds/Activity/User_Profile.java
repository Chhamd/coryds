package com.axcel.co.coryds.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;


import com.axcel.co.coryds.Adapters.MyRide_PagerAdapter;
import com.axcel.co.coryds.ChildFragments.about;
import com.axcel.co.coryds.ChildFragments.profile_setting;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.R;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class User_Profile extends AppCompatActivity {
    TabLayout tab_myride;
    ViewPager mviewPager;
    TextView username;
    CircleImageView dp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__profile);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tab_myride=findViewById(R.id.tab_myride);
        mviewPager=findViewById(R.id.mviewPager);
        dp=findViewById(R.id.dp);
        username=(TextView)findViewById(R.id.username);
        username.setText(Constatns.fname+" "+Constatns.lname);
        TextView about=findViewById(R.id.about);
        TextView reviews=findViewById(R.id.reviews);
        RatingBar rating=findViewById(R.id.rating);
        rating.setRating(Float.parseFloat(Constatns.rating));
        reviews.setText("( "+Constatns.reviews+" Reviews)");
        about.setText(Constatns.about);
        Log.e("check",Constatns.fname);
        MyRide_PagerAdapter adapter=new MyRide_PagerAdapter(getSupportFragmentManager());
        adapter.addNewTab("ABOUT",new about());
        // adapter.addNewTab("Ratings",new user_rating());
        adapter.addNewTab("SETTINGS",new profile_setting());
        // adapter.addNewTab("My Cars",new Mycars());
        mviewPager.setAdapter(adapter);
        tab_myride.setupWithViewPager(mviewPager);



        Picasso.get()
                .load(AppConfig.BASE_URL+Constatns.dp)
                .into(dp);
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }

}
