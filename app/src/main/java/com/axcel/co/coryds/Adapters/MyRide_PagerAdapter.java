package com.axcel.co.coryds.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MyRide_PagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> farr=new ArrayList<Fragment>();
    ArrayList<String> sarr=new ArrayList<String>();
    public void addNewTab(String title, Fragment f){

        sarr.add(title);
        farr.add(f);

    }

    public MyRide_PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return farr.get(position);
    }

    @Override
    public int getCount() {
        return farr.size();
    }
      public String getPageTitle(int position)
    {
        return sarr.get(position);
    }
}
