package com.axcel.co.coryds.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;

import com.axcel.co.coryds.Utils.Internet;
import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class PhoneVerification extends AppCompatActivity {
    private String mVerificationId;

    //The edittext to input the code
    private EditText editTextCode;
    private ProgressDialog dialog;
    //firebase auth object
    private FirebaseAuth mAuth;
    TextView number;
    PinView pinView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification_demo);
        dialog = new ProgressDialog(PhoneVerification.this);
        editTextCode=findViewById(R.id.editTextCode);

        mAuth = FirebaseAuth.getInstance();
//        mAuth.signOut();
//        sendVerificationCode(Constatns.mobile);
        if(Internet.isInternetAvailable(PhoneVerification.this)) {
            Log.e("checkmecode","yes"+Constatns.mobile);
            sendVerificationCode(Constatns.mobile);
        }
        else
        {
            Toast.makeText(PhoneVerification.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
         pinView = findViewById(R.id.pinView);
//        number=findViewById(R.id.number);
//        number.setText("On "+Constatns.mobile);
        Button submit =findViewById(R.id.button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = pinView.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    pinView.setError("Enter valid code");
                    pinView.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });

    }

    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();
//            Toast.makeText(PhoneVerification.this, code+"", Toast.LENGTH_SHORT).show();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                pinView.setText(code);
                //verifying the code
                verifyVerificationCode(code);

            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(PhoneVerification.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            Log.e("check","sfdsfsdfdsf");
            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    @Override
    public void onBackPressed() {


    }
    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneVerification.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity
                        new update_verified().execute();

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Toast.makeText(PhoneVerification.this, message+"", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    class update_verified extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);

                    if(Constatns.is_number_verified.equals("0"))
                    {
                        Toast.makeText(PhoneVerification.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Intent home=new Intent(PhoneVerification.this, MainActivity_Driver.class);
                        home.putExtra("flag",0);
                        startActivity(home);
                        finish();
                    }






        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id",Constatns.user_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_verified, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                Constatns.is_number_verified="1";



            }
            else
            {
                Constatns.is_number_verified="0";
            }

            return FinalData;
        }
    }
}
