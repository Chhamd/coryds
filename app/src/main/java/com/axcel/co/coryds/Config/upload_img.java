package com.axcel.co.coryds.Config;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class upload_img {
    private static upload_img mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;

    private upload_img(Context context){
        mCtx=context;
        requestQueue=getRequestQueue();

    }

    private RequestQueue getRequestQueue(){

       if(requestQueue==null)
           requestQueue= Volley.newRequestQueue(mCtx.getApplicationContext());
        return requestQueue;
    }


    public static synchronized upload_img getInstance(Context context){
        if(mInstance==null){
            mInstance=new upload_img(context);
        }
        return mInstance;
    }
    public<T> void addToRequestQue(Request<T> request){
      getRequestQueue().add(request);
    }



}
