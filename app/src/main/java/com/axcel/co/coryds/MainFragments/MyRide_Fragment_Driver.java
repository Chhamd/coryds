package com.axcel.co.coryds.MainFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.axcel.co.coryds.Activity.Post_a_Ride_clone;
import com.axcel.co.coryds.Adapters.MyRide_PagerAdapter;
import com.axcel.co.coryds.ChildFragments.Ongoing_Rides;
import com.axcel.co.coryds.ChildFragments.Past_Rides;
import com.axcel.co.coryds.ChildFragments.Posted_Rides;
import com.axcel.co.coryds.ChildFragments.Today_Rides;
import com.axcel.co.coryds.ChildFragments.Upcoming_Rides;
import com.axcel.co.coryds.R;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

public class MyRide_Fragment_Driver extends Fragment {
    TabLayout tab_myride;
    ViewPager mviewPager;
    FloatingActionButton new_ride;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.myride_fragment_driver,container,false);
        tab_myride=v.findViewById(R.id.tab_myride);
        mviewPager=v.findViewById(R.id.mviewPager);
       MyRide_PagerAdapter adapter=new MyRide_PagerAdapter(getChildFragmentManager());
        adapter.addNewTab("User Rides Requests",new Posted_Rides());
//        adapter.addNewTab("UpComing",new Upcoming_Rides());

        adapter.addNewTab("OnGoing ",new Ongoing_Rides());
        adapter.addNewTab("UpComing ",new Today_Rides());
        adapter.addNewTab("Past ",new Past_Rides());

         mviewPager.setAdapter(adapter);
        tab_myride.setupWithViewPager(mviewPager);
        mviewPager.setCurrentItem(1);
        new_ride = (FloatingActionButton) v.findViewById(R.id.new_Ride);
        new_ride.setImageResource(R.drawable.addd);
        new_ride.setOnClickListener(new View.OnClickListener() {
        @Override
         public void onClick(View v)
          {

           Intent i=new Intent(getActivity(), Post_a_Ride_clone.class);
           startActivity(i);

          }
       });

        return v;
    }
}
