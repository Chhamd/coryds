package com.axcel.co.coryds.Activity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.car_category_Model;
import com.axcel.co.coryds.Config.car_type_Model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static androidx.core.content.FileProvider.getUriForFile;

public class EditCar extends AppCompatActivity {
    ProgressDialog dialog;
    ArrayList<String> meids;
    String CarType_id;
    ArrayList<car_category_Model> car_categories;
    ArrayList<car_type_Model> types;
    Spinner brand,type,comfort,color;
    ArrayAdapter<CharSequence> brandadapter;
    ArrayAdapter<CharSequence> typeadapter;
    ArrayAdapter<CharSequence> comfortadapter;
    ArrayAdapter<CharSequence> coloradapter;
    Button addcar;
    String vehicletype_id,vehicle_number,vehicle_comfort,vehicle_colour,cat;
    EditText number;
    String[] comforts=new String[5];
    String[] colours=new String[8];
    Button car_image;
    RelativeLayout car_image_lay;
    ImageView Car_Image;
    String filename;
    Uri mImageUri;
    String image_Data;
    private int GALLERY = 1, CAMERA = 2;
    String ID,NUMBER,NAME,BRAND,COLOR;
    int comf;
String Server_response;
String Old_Image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_car);
        dialog = new ProgressDialog(EditCar.this);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        toolbar.setTitle("EDIT CAR");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        ID=getIntent().getStringExtra("id");
        NUMBER=getIntent().getStringExtra("number");
        NAME=getIntent().getStringExtra("name");
        BRAND=getIntent().getStringExtra("brand");
        COLOR=getIntent().getStringExtra("color");
        comf=Integer.parseInt(getIntent().getStringExtra("comfort"));
        Old_Image=getIntent().getStringExtra("image");
        brand=findViewById(R.id.brandspin);
        type=findViewById(R.id.typespin);
        comfort=findViewById(R.id.comfortspin);
        color=findViewById(R.id.colorspin);
        addcar=findViewById(R.id.addcar);

        number=findViewById(R.id.vehicleNumber);
        number.setText(NUMBER);
        car_image_lay=findViewById(R.id.image_lay);
        Car_Image=findViewById(R.id.car_image);
        image_Data="";
        Picasso.get()
                .load(Old_Image).placeholder(R.drawable.default_car)
                .into(Car_Image);
        car_image=findViewById(R.id.image);
        car_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Internet.isInternetAvailable(EditCar.this)) {
                    showPictureDialog();
                }
                else
                {
                    Toast.makeText(EditCar.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        car_image_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                takePhotoFromCamera(1);
                if(Internet.isInternetAvailable(EditCar.this)) {
                    showPictureDialog();
                }
                else
                {
                    Toast.makeText(EditCar.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if(Internet.isInternetAvailable(EditCar.this)) {
            new AllCarsBrands().execute();
        }
        else
        {
            Toast.makeText(EditCar.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }


        brand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0)
                {


                    typeadapter = new ArrayAdapter<CharSequence>(EditCar.this, R.layout.spinner_text,getcartypes(car_categories,types,position));
                    typeadapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
                    type.setAdapter(typeadapter);
                    ;
                    type.setSelection(get_type_index(getcartypes(car_categories,types,position),NAME));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        addcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //vehicletype_id = types.get(type.getSelectedItemPosition() - 1).getV_category_id() + "";
//
                vehicletype_id=meids.get(type.getSelectedItemPosition() - 1);
                vehicle_number = number.getText().toString().toUpperCase();
                if (vehicle_number.length() < 4){
                    number.setError("Invalid");
                }
                vehicle_comfort=comfort.getSelectedItemPosition()+"";
                vehicle_colour=colours[color.getSelectedItemPosition()];
//
                if(type.getSelectedItemPosition()!=0 && vehicle_number.length()>=4&&comfort.getSelectedItemPosition()>0&&color.getSelectedItemPosition()>0)
                {

                    // Toast.makeText(AddNewCar.this, vehicletype_id+"", Toast.LENGTH_SHORT).show();
                    if(Internet.isInternetAvailable(EditCar.this)) {
                        new UPDATE_CAR().execute();
                    }
                    else
                    {
                        Toast.makeText(EditCar.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    Toast.makeText(EditCar.this, "Missing Parameters", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }
    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(EditCar.this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};


        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), GALLERY);
    }
    public void takePhotoFromCamera() {

        filename="";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        filename=System.currentTimeMillis() + ".jpg";
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(filename));
        mImageUri=getCacheImagePath(filename);

        startActivityForResult(takePictureIntent, CAMERA);

    }
    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(EditCar.this, getPackageName() + ".provider", image);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {

            if(data.getData()!=null){


//                try {
                Uri mImageUri=data.getData();
                Bitmap bitmap;
                try {
                    bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                    Car_Image.setImageBitmap(bitmap);
                    image_Data =getStringImage(bitmap);
                    Log.e("froncodecheck",image_Data);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }
        if (requestCode==CAMERA)
        {
            this.getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = this.getContentResolver();
            Bitmap bitmap;
            try
            {
                bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                image_Data =getStringImage(bitmap);
                Log.e("froncodecheck",image_Data);
                Car_Image.setImageBitmap(bitmap);
            }
            catch (Exception e)
            {

            }
        }



    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    class AllCarsBrands extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            car_categories=new ArrayList<>();
            types=new ArrayList<>();




        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            brandadapter = new ArrayAdapter<CharSequence>(EditCar.this, R.layout.spinner_text,getcarbrandsname(car_categories));
            brandadapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            brand.setAdapter(brandadapter);

            brand.setSelection(get_Cat_index(car_categories,BRAND)+1);

            comforts[0]="---Select---";
            comforts[1]="Normal";
            comforts[2]="Basic";
            comforts[3]="Comfortable";
            comforts[4]="LUXURY";

            comfortadapter = new ArrayAdapter<CharSequence>(EditCar.this, R.layout.spinner_text,comforts);
            comfortadapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            comfort.setAdapter(comfortadapter);
comfort.setSelection(comf);
            colours[0]="Select Colour";
            colours[1]="Black";
            colours[2]="Blue";
            colours[3]="Brown";
            colours[4]="Red";
            colours[5]="Silver";
            colours[6]="White";
            colours[7]="Yellow";
            coloradapter = new ArrayAdapter<CharSequence>(EditCar.this, R.layout.spinner_text,colours);
            coloradapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            color.setAdapter(coloradapter);

            color.setSelection(get_color_index(colours,COLOR));

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();






            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_ALL_CAR_BRANDS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("category");

                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        car_categories.add(new car_category_Model(c.getString("category_id"),c.getString("category_name")));

                    }

                    //types
                    JSONArray Types = obj.getJSONArray("types");

                    for (int i = 0; i < Types.length(); i++) {
                        JSONObject a = Types.getJSONObject(i);

                        types.add(new car_type_Model(a.getString("v_vehicle_type_id"),a.getString("v_vehicle_type_name"),a.getString("v_category_id")));

                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }
    public  String[]  getcarbrandsname(ArrayList<car_category_Model> car)
    {
        String[] temp=new String[car.size()+1];
        temp[0]="Select Brand";
        for (int i=0,j=1;i<car.size();i++,j++)
        {
            temp[j]=car.get(i).getCategory_name();

        }

        return  temp;

    }
    public  String[]  getcartypes(ArrayList<car_category_Model> car,ArrayList<car_type_Model> cartypes,int position)
    {
        meids=new ArrayList<>();
        String id=car.get(position-1).getCategory_id();

        ArrayList<String> finaltypes=new ArrayList();

        for(int i=0;i<cartypes.size();i++)
        {
            if(cartypes.get(i).getV_category_id().equals(id))
            {
                finaltypes.add(cartypes.get(i).getVehicle_type_name());
                meids.add(cartypes.get(i).getVehicle_type_id());
                //  CarType_id=cartypes.get(i).getVehicle_type_id();
            }


        }





        String[] temp=new String[finaltypes.size()+1];
        temp[0]="Select Type";
        for (int i=0,j=1;i<finaltypes.size();i++,j++)
        {
            temp[j]=finaltypes.get(i);

        }

        return  temp;

    }


    class UPDATE_CAR extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.is_success=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(!Constatns.is_success)
            {
                Toast.makeText(EditCar.this, Server_response, Toast.LENGTH_SHORT).show();
            }else
            {
                Toast.makeText(EditCar.this, Server_response, Toast.LENGTH_SHORT).show();
                onBackPressed();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();

            HashMapParams.put("user_id",Constatns.user_id);
            HashMapParams.put("vehicle_id", ID);
            HashMapParams.put("vehicletype_id", vehicletype_id);
            HashMapParams.put("vehicle_number",vehicle_number);
            HashMapParams.put("vehicle_comfort", vehicle_comfort);
            HashMapParams.put("vehicle_colour",vehicle_colour);
            if(image_Data.length()>0)
            {
                HashMapParams.put("car_image", image_Data);
                HashMapParams.put("car_name", Constatns.user_id+getdate()+"car.png".replace(" ",""));

            }

            Log.e("checkdetail",vehicletype_id+":"+vehicle_number+":"+vehicle_comfort+":"+vehicle_colour);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_UPDATE_CAR, HashMapParams);
            Server_response=FinalData;
            Log.e("responsemehamdcars",FinalData);
            if(FinalData.contains("Updated")) {
                try {
                    Constatns.is_success=true;
                    JSONObject obj = new JSONObject(FinalData);
//                    JSONArray categories = obj.getJSONArray("status");
//                    for (int i = 0; i < categories.length(); i++) {
//                        JSONObject c = categories.getJSONObject(i);
//
//                        Constatns.email= c.getString("email");
//                        Constatns.fname= c.getString("fname");
//                        Constatns.lname= c.getString("lname");
//                        Constatns.type= c.getString("type");
//                        Constatns.mobile= c.getString("mobile");
//                        Constatns.gender= c.getString("gender");
//
//
//
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.is_success=false;
            }

            return FinalData;
        }
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }
    public String getdate()
    {  String currentDate="";

        currentDate = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.getDefault()).format(new Date());


        return currentDate;
    }
    public int get_Cat_index(ArrayList<car_category_Model> car_categories,String brand)
    {

        int val= 0;
        for(int i=0;i<car_categories.size();i++)
        {
            if(car_categories.get(i).getCategory_name().equals(brand))
            {
                val=i;
            }
        }

        return  val;
    }
    public int get_type_index(String[] name,String brand)
    {

        int val= 0;
        for(int i=0;i<name.length;i++)
        {
            if(name[i].equals(brand))
            {
                val=i;
            }
        }

        return  val;
    }
    public int get_color_index(String[] name,String brand)
    {

        int val= 0;
        for(int i=0;i<name.length;i++)
        {
            if(name[i].equals(brand))
            {
                val=i;
            }
        }

        return  val;
    }
}
