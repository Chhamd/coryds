package com.axcel.co.coryds.Config;

public class car_type_Model {

String vehicle_type_id,vehicle_type_name,v_category_id;

    public car_type_Model(String vehicle_type_id, String vehicle_type_name, String v_category_id) {
        this.vehicle_type_id = vehicle_type_id;
        this.vehicle_type_name = vehicle_type_name;
        this.v_category_id = v_category_id;

    }

    public String getVehicle_type_id() {
        return    this.vehicle_type_id;
    }
    public String getVehicle_type_name() {
        return    this.vehicle_type_name;
    }


    public String getV_category_id() {
        return    this.v_category_id;
    }

}
