package com.axcel.co.coryds.ChildFragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Adapters.RecyclerTouchListener;
import com.axcel.co.coryds.Adapters.Ride_Enq_RecyclerAdapter;
import com.axcel.co.coryds.Adapters.Ride_Requests_RecyclerAdapter_Clone;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.Config.Model_enq;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Ride_enquiries extends Fragment {
    private ProgressDialog dialog;
    private List<Model_enq> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Ride_Enq_RecyclerAdapter mAdapter;
    TextView nodata;
    Switch past;
    boolean is_old=false;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.ride_enquiries_child,container,false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        nodata=v.findViewById(R.id.no_data);
        nodata.setVisibility(View.VISIBLE);
        dialog = new ProgressDialog(getActivity());
        past=v.findViewById(R.id.past);
        if(Internet.isInternetAvailable(getActivity())) {
            is_old=false;
            new RIDE_ENQ().execute();
        }
        else
        {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }


        past.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                is_old=b;
                if(Internet.isInternetAvailable(getActivity())) {
                    movieList = new ArrayList<>();
                    new RIDE_ENQ().execute();
                }
                else
                {
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Model movie = movieList.get(position);
//                Toast.makeText(getActivity(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return v;
    }

class RIDE_ENQ extends AsyncTask<Void, Void, String> {

    @Override
    protected void onPreExecute() {
        movieList = new ArrayList<>();

        dialog.setMessage("Processing . . .");
        dialog.show();
        super.onPreExecute();
        Log.e("called","yes00");

    }

    @Override
    protected void onPostExecute(String string1) {
        dialog.dismiss();
        super.onPostExecute(string1);
        if (movieList.size()>0) {
            nodata.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            mAdapter = new Ride_Enq_RecyclerAdapter(movieList, getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            nodata.setVisibility(View.GONE);
        }
        else
        {
            nodata.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    protected String doInBackground(Void... params) {

        ImageProcessClass imageProcessClass = new ImageProcessClass();

        HashMap<String, String> HashMapParams = new HashMap<String, String>();


        HashMapParams.put("type", "1");
        HashMapParams.put("user_id", Constatns.user_id);
        HashMapParams.put("date", getdate());
        HashMapParams.put("is_old", is_old+"");


        String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_ENQ, HashMapParams);

        Log.e("responsemehamdreq",FinalData);
        if(!FinalData.contains("error")) {
            try {

                JSONObject obj = new JSONObject(FinalData);
                JSONArray categories = obj.getJSONArray("status");
                for (int i = 0; i < categories.length(); i++) {
                    JSONObject c = categories.getJSONObject(i);

                    Model_enq movie = new Model_enq(c.getString("name"),c.getString("source"),c.getString("destination"),c.getString("date"),c.getString("time"),c.getString("user_mobile"),c.getString("dp"),c.getString("rating"),c.getString("reviews"),c.getString("passenger_id"));
                    movieList.add(movie);




                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        return FinalData;
    }
}
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
}
