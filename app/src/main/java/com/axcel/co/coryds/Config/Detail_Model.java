package com.axcel.co.coryds.Config;

public class Detail_Model {

    String rideid;
    String customerid;
    String driverid;
    String longitude;
    String latitude;
    String distance;
    String source;
    String destination;
    long time;

    public Detail_Model() {
    }

    public Detail_Model(String rideid, String customerid, String driverid, String longitude, String latitude, String distance, String source, String destination, long time) {
        this.rideid = rideid;
        this.customerid = customerid;
        this.driverid = driverid;
        this.longitude = longitude;
        this.latitude = latitude;
        this.distance = distance;
        this.source = source;
        this.destination = destination;
        this.time = time;
    }

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
