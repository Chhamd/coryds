package com.axcel.co.coryds.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Location.PlaceAutoSuggestAdapter;
import com.axcel.co.coryds.R;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Post_a_Ride extends AppCompatActivity {

    ImageButton plus,minus;
    Button btn_upinfo;
    private ProgressDialog dialog;
    EditText price;
    EditText pickup,seatsedt;
    Spinner spinner;
    LocationManager locationManager;
    TextView picktime,droptime,date;
    AutoCompleteTextView autoCompleteTextView,drop;
    String source,destination,finaltimepick,finaldoptime,seats,car,pricefinal;
    String pickupltlng,droplatlng,mydate;
    ArrayAdapter<CharSequence> langAdapter;
    RelativeLayout parent,datelayout;
    CheckBox onetime;
    DatePickerDialog.OnDateSetListener date1;
    String isontime;
    CheckBox is_round;
    RelativeLayout dep1;
    LinearLayout dep2;
    TextView return_date;
    TextView return_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_a__ride);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        price=(EditText)findViewById(R.id.textprice);
        date=(TextView)findViewById(R.id.date);

        btn_upinfo=(Button)findViewById(R.id.btn_upinfo);
        spinner = (Spinner)findViewById(R.id.spinner2);
        picktime=findViewById(R.id.picktime);
        is_round=findViewById(R.id.is_round);
        dep1=findViewById(R.id.departure_layout1);
        dep2=findViewById(R.id.departure_layout2);
        dep1.setVisibility(View.GONE);
        dep2.setVisibility(View.GONE);
        is_round.setVisibility(View.GONE);
        return_date=findViewById(R.id.departure_date);

        return_time=findViewById(R.id.departure_return_time);



        //

        is_round.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    dep1.setVisibility(View.VISIBLE);
                    dep2.setVisibility(View.VISIBLE);
                }
                else
                {
                    dep1.setVisibility(View.GONE);
                    dep2.setVisibility(View.GONE);
                }
            }
        });

//        droptime=findViewById(R.id.droptime);
        seatsedt=findViewById(R.id.seats);
        dialog = new ProgressDialog(Post_a_Ride.this);
        new MyCars().execute();
        parent=findViewById(R.id.alltimes);
        datelayout=findViewById(R.id.datelayout);
        onetime=findViewById(R.id.onetime);
        onetime.setChecked(false);
        datelayout.setVisibility(View.GONE);
        final Calendar myCalendar = Calendar.getInstance();
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //   String myFormat = "yyyy/mm/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());

                date.setText(sdf.format(myCalendar.getTime()));
            }

        };
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Post_a_Ride.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        onetime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    isontime="1";
                    parent.setVisibility(View.GONE);
                    datelayout.setVisibility(View.VISIBLE);
                    is_round.setVisibility(View.VISIBLE);
                }
                else
                {   dep1.setVisibility(View.GONE);
                    dep2.setVisibility(View.GONE);
                    is_round.setVisibility(View.GONE);
                    isontime="0";
                    datelayout.setVisibility(View.GONE);
                    parent.setVisibility(View.VISIBLE);
                }
            }
        });
        picktime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Post_a_Ride.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        picktime.setText( hour + ":" + selectedMinute+ " " + AM_PM );
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
     /*   droptime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Post_a_Ride.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        droptime.setText( hour + ":" + selectedMinute+ " " + AM_PM );


                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
*/

        btn_upinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                source=autoCompleteTextView.getText().toString();
                destination=drop.getText().toString();
                car= Constatns.mycarsid[spinner.getSelectedItemPosition()];
                finaltimepick=picktime.getText().toString();
//                finaldoptime=droptime.getText().toString();
                seats=seatsedt.getText().toString();
                pricefinal=price.getText().toString();
                mydate=date.getText().toString();
//                if(mydate.compareTo(getdate())>0 && checktimings(finaltimepick,finaldoptime) && source.length()>4 && destination.length()>4&& seats.length()==1)

 if(mydate.compareTo(getdate())>0  && source.length()>4 && destination.length()>4&& seats.length()==1)
{
//    Toast.makeText(Post_a_Ride.this,  "", Toast.LENGTH_SHORT).show();
    pricefinal=price.getText().toString();


    new POST_A_RIDE().execute();
}
else
{
    Toast.makeText(Post_a_Ride.this, "Please Select Valid Date", Toast.LENGTH_SHORT).show();
}

            }
        });


        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        autoCompleteTextView=findViewById(R.id.edt_strtloc);


        autoCompleteTextView.setAdapter(new PlaceAutoSuggestAdapter(Post_a_Ride.this,android.R.layout.simple_list_item_1));
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        drop=findViewById(R.id.edt_endtloc);


        drop.setAdapter(new PlaceAutoSuggestAdapter(Post_a_Ride.this,android.R.layout.simple_list_item_1));
        drop.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Post_a_Ride.this, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        return_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Post_a_Ride.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        return_time.setText( hour + ":" + selectedMinute+ " " + AM_PM );
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }

        });






    }
    class POST_A_RIDE extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.isposted=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(Constatns.isposted=true)
            {
                autoCompleteTextView.setText("");
                drop.setText("");

                picktime.setText("");
//                droptime.setText("");
                seatsedt.setText("");
                Toast.makeText(Post_a_Ride.this, "Rides Posted", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(Post_a_Ride.this, "Error In Posting", Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();
            String s_lat,s_long,d_lat,d_long;
            HashMap<String, String> HashMapParams = new HashMap<String, String>();
            LatLng latLng=getLatLngFromAddress(source);
            pickupltlng=latLng.latitude+","+latLng.longitude;
            s_lat=latLng.latitude+"";
            s_long=latLng.longitude+"";

            LatLng latLng2=getLatLngFromAddress(destination);
            droplatlng=latLng2.latitude+","+latLng2.longitude;
            d_lat=latLng2.latitude+"";
            d_long=latLng2.longitude+"";
            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("p_lat",pickupltlng);
            HashMapParams.put("d_lat", droplatlng);
            HashMapParams.put("source",source);
            HashMapParams.put("destination", destination);
            HashMapParams.put("pick_time",finaltimepick);
//            HashMapParams.put("drop_time", finaldoptime);
            HashMapParams.put("seats",seats);
            HashMapParams.put("date", mydate);
            HashMapParams.put("vehicle_id",car);
            HashMapParams.put("status", "1");
            HashMapParams.put("price", pricefinal);
            HashMapParams.put("is_onetime", isontime);
            HashMapParams.put("s_lat", s_lat);
            HashMapParams.put("s_long", s_long);
            HashMapParams.put("d_lat", d_lat);
            HashMapParams.put("d_long", d_long);
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_POST_RIDE, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.isposted=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);



                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }



    class MyCars extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();


        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            langAdapter = new ArrayAdapter<CharSequence>(Post_a_Ride.this, R.layout.spinner_text, Constatns.mycars );
            langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            spinner.setAdapter(langAdapter);



        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id",Constatns.user_id);


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_CARS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    Constatns.mycarsid=new String[categories.length()];
                    Constatns.mycars=new String[categories.length()];
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                        Constatns.mycars[i]=(c.getString("name")+"-"+c.getString("vehicle_number"));
                        Constatns.mycarsid[i]=(c.getString("vehicle_id"));







                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }
    private LatLng getLatLngFromAddress(String address){

        Geocoder geocoder=new Geocoder(Post_a_Ride.this);
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocationName(address, 1);
            if(addressList!=null){
                Address singleaddress=addressList.get(0);
                LatLng latLng=new LatLng(singleaddress.getLatitude(),singleaddress.getLongitude());
                return latLng;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private Address getAddressFromLatLng(LatLng latLng){
        Geocoder geocoder=new Geocoder(Post_a_Ride.this);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 5);
            if(addresses!=null){
                Address address=addresses.get(0);
                return address;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }
    private boolean checktimings(String time, String endtime) {

        String pattern = "hh:mm aa";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }
}
