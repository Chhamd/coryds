package com.axcel.co.coryds.ChildFragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Adapters.RecyclerTouchListener;
import com.axcel.co.coryds.Adapters.Today_Adapter;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.Config.Today_model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Today_Rides extends Fragment {
    private ProgressDialog dialog;
    private ArrayList<Today_model> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Today_Adapter mAdapter;
    TextView no_data;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.upcomingride_fragment,container,false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        no_data=v.findViewById(R.id.no_data);
        no_data.setVisibility(View.GONE);

        dialog = new ProgressDialog(getActivity());






        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Model movie = movieList.get(position);
//                Toast.makeText(getActivity(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return v;
    }
//    private void prepareMovieData() {
//        Model movie = new Model("Mad Max: Fury Road", "Action & Adventure", "$ 250",1);
//        movieList.add(movie);
//
//        movie = new Model("Inside Out", "Animation, Kids & Family", "2015",1);
//        movieList.add(movie);
//
//        movie = new Model("Star Wars: Episode VII - The Force Awakens", "Action", "2015",1);
//        movieList.add(movie);
//
//        movie = new Model("Shaun the Sheep", "Animation", "2015",1);
//        movieList.add(movie);
//
//        movie = new Model("The Martian", "Science Fiction & Fantasy", "2015",1);
//        movieList.add(movie);
//
//
//        mAdapter.notifyDataSetChanged();
//    }
    class Rides extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            movieList = new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(movieList.size()>0)
            {
                no_data.setVisibility(View.GONE);
                mAdapter = new Today_Adapter(movieList,getActivity());
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();

            }
            else
                {
                    no_data.setText("There Is No Ride ");
                    no_data.setVisibility(View.VISIBLE);
                }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("date", getdate());
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_TODAY_RIDES, HashMapParams);

            Log.e("TodayRides",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

//                        Today_model movie = new Today_model(c.getString("name"),c.getString("pick"),c.getString("drop"),c.getString("date"),c.getString("start_time"),c.getString("price"),c.getString("available_seats"),c.getString("vehicle_number"),c.getString("vehicle_name"),c.getString("trip_id"),c.getString("dp"),4);
                        Today_model movie = new Today_model(c.getString("name"),c.getString("pick"),c.getString("drop"),c.getString("date")+" At: "+c.getString("start_time"),c.getString("date"),c.getString("price"),c.getString("available_seats"),c.getString("vehicle_number"),c.getString("vehicle_name"),c.getString("trip_id"),c.getString("dp"),4);

                        movieList.add(movie);




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    class BOOKED_RIDES extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            movieList = new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            mAdapter = new Today_Adapter(movieList,getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id", Constatns.user_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_BOOKED_RIDE_DETAIL, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Today_model movie = new Today_model(c.getString("driver_name"),c.getString("source"),c.getString("destination"),c.getString("booking_date"),c.getString("booking_time"),c.getString("booking_price"),c.getString("booking_seats"),c.getString("is_active"),c.getString("payment_status"),c.getString("trip_id"),c.getString("pic"),0);
                        movieList.add(movie);




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");
        super.onResume();
        if(movieList.size()>0)
        {
            mAdapter = new Today_Adapter(movieList,getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
        else
        {
            if(Internet.isInternetAvailable(getActivity())) {
                new Rides().execute();;
            }
            else
            {
                Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }

    }
}
