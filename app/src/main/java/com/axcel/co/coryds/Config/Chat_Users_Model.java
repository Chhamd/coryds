package com.axcel.co.coryds.Config;

public class Chat_Users_Model {
    String thread_id,  subject,  from_,to_, user_name, dp,date;


    public Chat_Users_Model() {
    }

    public Chat_Users_Model(String thread_id, String from_, String to_,String subject, String user_name, String dp,String date) {
        this.thread_id = thread_id;
        this.from_ = from_;
        this.to_ = to_;
        this.subject = subject;
        this.user_name=user_name;
        this.dp = dp;
        this.date = date;



    }

    public String getThread_id() {
        return this.thread_id;
    }
    public String getFrom_() {
        return this.from_;
    }
    public String getSubject() {
        return this.subject;
    }
    public String getUser_name() {
        return this.user_name;
    }
    public String getDp() {
        return this.dp;
    }
    public String getDate() {
        return this.date;
    }
    public String getTo_() {
        return this.to_;
    }
}
