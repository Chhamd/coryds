package com.axcel.co.coryds.MainFragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.MainActivity_Driver;
import com.axcel.co.coryds.Adapters.Notification_dataper;
import com.axcel.co.coryds.Adapters.Upcoming_Adapter;
import com.axcel.co.coryds.Adapters.chat_users_dataper;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Chat_Users_Model;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Upcoming_model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Chat_Fragment extends Fragment {

    private ProgressDialog dialog;
    ArrayList<String> CHAT_IT;
    private ArrayList<Chat_Users_Model> chats;
    ArrayList<String> IsRead;
    ArrayList<String> date;
    private RecyclerView recyclerView;
    private chat_users_dataper mAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.chat_fragment,container,false);;
        dialog=new ProgressDialog(getActivity());




        recyclerView=v.findViewById(R.id.users);
        if(Internet.isInternetAvailable(getActivity())) {
            new GET_CHATS().execute();
        }
        else
        {
            Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        return v;
    }
    class GET_CHATS extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            chats=new ArrayList<>();
            dialog.setMessage("UPDATING DATA . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(chats.size()>0)
            {
                mAdapter = new chat_users_dataper(chats,getActivity());
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }





        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("type", "0");
            HashMapParams.put("user_id", Constatns.user_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_ALL_CHATS, HashMapParams);

            Log.e("ALL_CHATS",FinalData);
            if(!FinalData.contains("error")) {
                Log.e("ALL_CHATS","yes");
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                        Chat_Users_Model movie = new Chat_Users_Model(c.getString("thread_id"),c.getString("from_"),c.getString("to_"),c.getString("subject"),c.getString("user_name"),c.getString("dp"),c.getString("date"));
                        chats.add(movie);






                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
}
