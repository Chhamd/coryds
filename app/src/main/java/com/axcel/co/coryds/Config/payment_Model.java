package com.axcel.co.coryds.Config;

public class payment_Model {
    String name,ride,seats,price,date,ride_id,email,mobile,dp,Status;


    public payment_Model() {
    }

    public payment_Model(String name, String ride, String seats, String price, String date,String ride_id,String email,String mobile,String dp,String Status) {
        this.name = name;
        this.ride = ride;
        this.seats = seats;
        this.price = price;
        this.date=date;
        this.ride_id=ride_id;
        this.email=email;
        this.mobile=mobile;
        this.dp=dp;
        this.Status=Status;

    }
    public String getStatus() {
        return this.Status;
    }
    public String getDp() {
        return this.dp;
    }
    public String getEmail() {
        return this.email;
    }
    public String getMobile() {
        return this.mobile;
    }

    public String getRide_id() {
        return this.ride_id;
    }
    public String getName() {
        return this.name;
    }
    public String getRide() {
        return this.ride;
    }
    public String getSeats() {
        return this.seats;
    }
    public String getPrice() {
        return this.price;
    }

    public String getDate() {
        return this.date;
    }

}
