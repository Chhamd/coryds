package com.axcel.co.coryds.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.axcel.co.coryds.Adapters.MyRide_PagerAdapter;
import com.axcel.co.coryds.ChildFragments.about;
import com.axcel.co.coryds.ChildFragments.about_us;
import com.axcel.co.coryds.ChildFragments.faqs;
import com.axcel.co.coryds.ChildFragments.privacy_policy;
import com.axcel.co.coryds.ChildFragments.profile_setting;
import com.axcel.co.coryds.ChildFragments.terms_of_use;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.R;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class Faqs extends AppCompatActivity {
    TabLayout tab_myride;
    ViewPager mviewPager;
    TextView username;
    CircleImageView dp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tab_myride=findViewById(R.id.tab_myride);
        mviewPager=findViewById(R.id.mviewPager);

        MyRide_PagerAdapter adapter=new MyRide_PagerAdapter(getSupportFragmentManager());
        adapter.addNewTab("FAQ's",new faqs());
        // adapter.addNewTab("Ratings",new user_rating());

        adapter.addNewTab("Terms",new terms_of_use());
        adapter.addNewTab("Privacy",new privacy_policy());
        // adapter.addNewTab("My Cars",new Mycars());
        mviewPager.setAdapter(adapter);
        tab_myride.setupWithViewPager(mviewPager);




    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }

}
