package com.axcel.co.coryds.ChildFragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.Adapters.Ride_detail_adpt;
import com.axcel.co.coryds.R;

public class Customers_all extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.frag_customers,container,false);
         TextView nodata=v.findViewById(R.id.nodata);
          nodata.setVisibility(View.GONE);
//        RecyclerView recyclerView=v.findViewById(R.id.recycler_view);
//        Ride_detail_adpt mAdapter = new Ride_detail_adpt(RideDetail.CUSTOMER_ID,RideDetail.CUSTOMER_NAME,RideDetail.CUSTOMER_PIC,RideDetail.PRICE,RideDetail.FROM,RideDetail.TO,RideDetail.vehicle_name,RideDetail.Vehicle_number,RideDetail.Date_,RideDetail.trip_id,RideDetail.ride_id,RideDetail.Driver_id,RideDetail.CUSTOMER_MOBILE,RideDetail.flag,RideDetail.CUSTOMER_IS_PICKED,RideDetail.CUSTOMER_IS_ACTIVE,RideDetail.CUSTOMER_IS_DROPED,RideDetail.CUSTOMER_RATING,RideDetail.CUSTOMER_IS_RATED,RideDetail.CUSTOMER_PAYMENT_status, getActivity());
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mAdapter);
//        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
//        recyclerView.setAdapter(mAdapter);
//        mAdapter.notifyDataSetChanged();
        return v;
    }


}