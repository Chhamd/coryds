package com.axcel.co.coryds.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Ride_Requests_RecyclerAdapter extends RecyclerView.Adapter<Ride_Requests_RecyclerAdapter.MyViewHolder> {

    private List<Model> moviesList;
    Context ct;
    public ProgressDialog dialog;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView fname,pick,drop,date,time,price,seats,cartype;
        TextView accept,reject;
        ImageView status;
        RelativeLayout myrow;
        CircleImageView dp;


        public MyViewHolder(View view) {
            super(view);
            fname = (TextView) view.findViewById(R.id.fname);
            pick = (TextView) view.findViewById(R.id.pick);
            drop = (TextView) view.findViewById(R.id.dropl);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            seats = (TextView) view.findViewById(R.id.seats);
            accept=  (TextView) view.findViewById(R.id.accept);
            reject=  (TextView) view.findViewById(R.id.reject);
            status=  (ImageView) view.findViewById(R.id.status);
            cartype  =  (TextView) view.findViewById(R.id.cartype);
            price=   (TextView) view.findViewById(R.id.price);
            myrow=  view.findViewById(R.id.myrow);
            dp=view.findViewById(R.id.dp);

        }
    }


    public Ride_Requests_RecyclerAdapter(List<Model> moviesList, Context ct) {

        this.moviesList = moviesList;
        this.ct=ct;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_requests_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Model movie = moviesList.get(position);
        holder.fname.setText(movie.getFname());
        holder.pick.setText(movie.getPick());
        holder.drop.setText(movie.getDrop());
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());
        holder.seats.setText(movie.getSeats()+" seats booking");
        holder.cartype.setText(movie.getCarname()+" | "+movie.getCarnumber());
        holder.price.setText("PKR "+movie.getPrice());
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Internet.isInternetAvailable(ct)) {
                    Constatns.accept=true;
                    Constatns.trip_id=movie.getRideid();
                    ;
                    movie.setstatus(1);
                    dialog = new ProgressDialog(ct);
                    Log.e("ride_id",movie.getRideid()+"-----"+Constatns.accept);


                    new Accept_request().execute();
                }
                else
                {
                    Toast.makeText(ct, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Internet.isInternetAvailable(ct)) {
                    movie.setstatus(0);
                    Constatns.accept=false;
                    Constatns.trip_id=movie.getRideid();
                    new Accept_request().execute();
                }
                else
                {
                    Toast.makeText(ct, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    if(movie.getStatus()==0)
    {
        holder.status.setImageResource(R.drawable.inactive);
    }
    else
    {
        holder.status.setImageResource(R.drawable.pending);
    }
        Picasso.get()
                .load(AppConfig.BASE_URL+movie.getpic())
                .into(holder.dp);





        holder.myrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(ct, RideDetail.class);
//                i.putExtra("ride_id",movie.getRideid());
//                ct.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
    class Accept_request extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
//            movieList = new ArrayList<>();
//
            Constatns.is_accept=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            Constatns.accept=false;
            Constatns.trip_id="";
            if (Constatns.is_accept)
            {
                notifyDataSetChanged();
            }
            else
            {

            }

//            mAdapter = new New_Ride_RecyclerAdapter(movieList,getActivity());
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//            recyclerView.setLayoutManager(mLayoutManager);
//            recyclerView.setItemAnimator(new DefaultItemAnimator());
//            recyclerView.setAdapter(mAdapter);
//            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
//            recyclerView.setAdapter(mAdapter);
//            mAdapter.notifyDataSetChanged();


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("trip_id", Constatns.trip_id);
            if(Constatns.accept)
            {
                HashMapParams.put("accept","1");
            }
            else
            {
                HashMapParams.put("accept","0");
            }

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_ACCEPT_REQ, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.is_accept=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

//                        Model movie = new Model(c.getString("user_name"),c.getString("source"),c.getString("destination"),c.getString("booking_date"),c.getString("booking_time"),c.getString("booking_price"),c.getString("booking_seats"),"","",c.getString("trip_id"),Integer.parseInt(c.getString("is_active")));
//                        movieList.add(movie);




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
}