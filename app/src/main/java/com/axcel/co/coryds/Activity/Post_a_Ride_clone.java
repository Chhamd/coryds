package com.axcel.co.coryds.Activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.axcel.co.coryds.Adapters.MyRide_PagerAdapter;
import com.axcel.co.coryds.ChildFragments.Onway_Fragment;
import com.axcel.co.coryds.ChildFragments.Two_way_Fragment;
import com.axcel.co.coryds.ChildFragments.about;
import com.axcel.co.coryds.ChildFragments.profile_setting;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Location.PlaceAutoSuggestAdapter;
import com.axcel.co.coryds.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class Post_a_Ride_clone extends AppCompatActivity {
    TabLayout tab_myride;
    ViewPager mviewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_a__ride_clone);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
//        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tab_myride=findViewById(R.id.tab_myride);
        mviewPager=findViewById(R.id.mviewPager);
        MyRide_PagerAdapter adapter=new MyRide_PagerAdapter(getSupportFragmentManager());
        adapter.addNewTab("One Way",new Onway_Fragment());
        // adapter.addNewTab("Ratings",new user_rating());
        adapter.addNewTab("ROUND TRIP",new Two_way_Fragment());
        // adapter.addNewTab("My Cars",new Mycars());
        mviewPager.setAdapter(adapter);
        tab_myride.setupWithViewPager(mviewPager);

    }

    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }


}
