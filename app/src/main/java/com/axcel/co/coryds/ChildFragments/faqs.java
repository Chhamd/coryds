package com.axcel.co.coryds.ChildFragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Adapters.FAqs_Adpt;
import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class faqs extends Fragment {
    ProgressDialog dialog;
    RecyclerView recyclerView;
    FAqs_Adpt mAdapter;
    ArrayList<String> question,answers,media;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.faqs_sc,container,false);
        dialog = new ProgressDialog(getActivity());
        recyclerView = (RecyclerView)v.findViewById(R.id.list);
        new GET_FAQS().execute();
        return v;
    }

    class GET_FAQS extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            question=new ArrayList<>();
            answers=new ArrayList<>();
            media=new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);




            mAdapter = new FAqs_Adpt(question,answers,media);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);




        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();




            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_FAQS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");

                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        question.add(c.getString("question")) ;
                        answers.add(c.getString("answer")) ;
                        if(c.getString("media")==null)
                        {
                            media.add("0") ;
                        }
                        else
                        {
                            media.add(c.getString("media")) ;
                        }



                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }



            return FinalData;
        }
    }
}