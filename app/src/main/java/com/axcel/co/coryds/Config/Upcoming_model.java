package com.axcel.co.coryds.Config;

public class Upcoming_model {
    String fname,  pick,  drop, date, time, price ,seats,carnumber,carname,rideid,pic,bookig_Seats;

    int status,payment_status;
    public Upcoming_model() {
    }

    public Upcoming_model(String fname, String pick, String drop, String date, String time, String price , String seats, String carnumber, String carname, String rideid, String pic,String bookig_Seats, int status,int payment_status) {
        this.fname = fname;
        this.pick = pick;
        this.drop = drop;
        this.date=date;
        this.time = time;
        this.price=price;
        this.seats=seats;
        this.carnumber=carnumber;
        this.carname=carname;
        this.status=status;
        this.rideid=rideid;
        this.bookig_Seats=bookig_Seats;

        this.pic=pic;
        this.payment_status=payment_status;



    }

    public String getFname() {
        return this.fname;
    }
    public String getPick() {
        return this.pick;
    }
    public String getDrop() {
        return this.drop;
    }
    public String getDate() {
        return this.date;
    }
    public String getTime() {
        return this.time;
    }
    public String getPrice() {
        return this.price;
    }
    public String getSeats() {
        return this.seats;
    }
    public String getCarnumber() {
        return this.carnumber;
    }
    public String getCarname() {
        return this.carname;
    }
    public String getRideid() {
        return this.rideid;
    }
    public void setstatus(int status) {
        this.status=status;
    }
    public int getStatus() {
        return this.status;
    }
    public int getPayment_status() {
        return this.payment_status;
    }
    public String getpic() {
        return this.pic;
    }
    public String getBookig_Seats() {
        return this.bookig_Seats;
    }
}
