package com.axcel.co.coryds.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.axcel.co.coryds.ChildFragments.Onway_Fragment;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Location.PlaceAutoSuggestAdapter;
import com.axcel.co.coryds.Location.SingleShotLocationProvider;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Edit_a_Ride extends AppCompatActivity {
    CheckBox is_onetime,is_usual;
    EditText departure_date,dp_time;
    RelativeLayout days;
    boolean isonetime;
    TextView IS_One,IS_USUAL;



    ImageButton plus,minus;
    Button btn_upinfo;
    private ProgressDialog dialog;
    TextView price;
    EditText pickup;
    TextView seatsedt;
    Spinner spinner;
    LocationManager locationManager;
    TextView picktime,droptime;
    EditText date;
    AutoCompleteTextView autoCompleteTextView,drop;
    String source,destination,finaltimepick,finaldoptime,seats,car,pricefinal;
    String pickupltlng,droplatlng,mydate;
    ArrayAdapter<CharSequence> langAdapter;
    RelativeLayout parent,datelayout;
    CheckBox onetime;
    DatePickerDialog startDatePicker;
    String isontime;
    String trip_id;
    CheckBox is_round;
    RelativeLayout dep1;
    LinearLayout dep2;
    TextView return_date;
    TextView return_time;


    //
String Phone,Comments,Route,Route_latlong;
    int    year,month,day;
    String Server_response;
    Location gps_loc, network_loc, final_loc;
    ImageView curentloc;
    double latitude, longitude;
    String current_loc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_a__ride);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        is_onetime=findViewById(R.id.is_onetime);
        is_usual=findViewById(R.id.is_usual);
        IS_One=findViewById(R.id.t1);
        IS_USUAL=findViewById(R.id.t2);
        departure_date=findViewById(R.id.dpdate);
        days=findViewById(R.id.usual1);
        initiate();
        is_onetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {isonetime=false;
                    switchtousual();
                }else {
                    switchtoonetime();
                    isonetime=true;
                }
            }
        });
        is_usual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {
                    isonetime=false;
                    switchtousual();

                }else {

                    switchtoonetime();
                    isonetime=true;
                }
            }
        });
        IS_One.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {isonetime=false;
                    switchtousual();
                }else {
                    switchtoonetime();
                    isonetime=true;
                }
            }
        });
        IS_USUAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {
                    isonetime=false;
                    switchtousual();

                }else {

                    switchtoonetime();
                    isonetime=true;
                }
            }
        });



        price=(TextView)findViewById(R.id.price);
        date=findViewById(R.id.dpdate);

        btn_upinfo=(Button)findViewById(R.id.btn_upinfo);
        spinner = (Spinner)findViewById(R.id.spinner2);
        picktime=findViewById(R.id.dp_time);
      //  droptime=findViewById(R.id.droptime);
        seatsedt=findViewById(R.id.seats);
        dialog = new ProgressDialog(Edit_a_Ride.this);
        new MyCars().execute();
        parent=findViewById(R.id.alltimes);
        datelayout=findViewById(R.id.datelayout);
        onetime=findViewById(R.id.is_onetime);


        is_round=findViewById(R.id.is_round);

      //  return_date=findViewById(R.id.dpdate);

     //   return_time=findViewById(R.id.departure_return_time);



        final Calendar myCalendar = Calendar.getInstance();
        startDatePicker = new DatePickerDialog(Edit_a_Ride.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {



                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
                date.setText(sdf.format(myCalendar.getTime()));

            }
        }, year, month, day);



        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);


                String dateString = getIntent().getStringExtra("date");
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

                Date readDate = null;
                try {
                    readDate = df.parse(dateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(readDate.getTime());


                startDatePicker.getDatePicker().init(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH),null);
                startDatePicker.show();
            }
        });


        picktime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;

                mTimePicker = new TimePickerDialog(Edit_a_Ride.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        picktime.setText( hour + ":" + selectedMinute+ " " + AM_PM );
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                Date date = null;
                try {
                    date = sdf.parse(getIntent().getStringExtra("pick_time"));
                } catch (ParseException e) {
                }
                Calendar c = Calendar.getInstance();
                c.setTime(date);



                mTimePicker.updateTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE));
            }
        });
       /* droptime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Edit_a_Ride.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        droptime.setText( hour + ":" + selectedMinute+ " " + AM_PM );


                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });*/


        btn_upinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                source=autoCompleteTextView.getText().toString();
                destination=drop.getText().toString();
                car= Constatns.mycarsid[spinner.getSelectedItemPosition()];
                finaltimepick=picktime.getText().toString();
           //     finaldoptime=droptime.getText().toString();
                seats=seatsedt.getText().toString();
                pricefinal=price.getText().toString();
                mydate=date.getText().toString();
//if(mydate.compareTo(getdate())>0  )
//{
//    Toast.makeText(Post_a_Ride.this,  "", Toast.LENGTH_SHORT).show();

    if( source.length()>4 && destination.length()>4&& seats.length()==1)
    {
        pricefinal=price.getText().toString();
        if(Internet.isInternetAvailable(Edit_a_Ride.this)) {
            new POST_A_RIDE().execute();
        }
        else
        {
            Toast.makeText(Edit_a_Ride.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
//        MyDialogue();

    }
    else
    {
        Toast.makeText(Edit_a_Ride.this, "Please Enter Valid Input", Toast.LENGTH_SHORT).show();
    }


//}
//else
//{
//    Toast.makeText(Edit_a_Ride.this, "Please Enter Valid Date", Toast.LENGTH_SHORT).show();
//}

            }
        });
        SingleShotLocationProvider.requestSingleUpdate(Edit_a_Ride.this,
                new SingleShotLocationProvider.LocationCallback() {
                    @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        try {

                            Geocoder geocoder = new Geocoder(Edit_a_Ride.this, Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1);
                            if (addresses != null && addresses.size() > 0) {
                                current_loc=addresses.get(0).getAddressLine(0);
                                Log.e("checkmylocationfinal",addresses.get(0).getAddressLine(0));

                            } else {
                                current_loc=null;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("Location", "my location is " + location.toString());
                    }
                });
        curentloc = findViewById(R.id.location);
        autoCompleteTextView=findViewById(R.id.pick);
        curentloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_loc!=null)
                {

                    autoCompleteTextView.setText(current_loc);

                }
                else
                {
                    Toast.makeText(Edit_a_Ride.this, "Waiting For Your location", Toast.LENGTH_SHORT).show();
                }

            }
        });
        autoCompleteTextView.setAdapter(new PlaceAutoSuggestAdapter(Edit_a_Ride.this,android.R.layout.simple_list_item_1));


        drop=findViewById(R.id.drop);
        drop.setAdapter(new PlaceAutoSuggestAdapter(Edit_a_Ride.this,android.R.layout.simple_list_item_1));




        btn_upinfo=(Button)findViewById(R.id.btn_upinfo);
        spinner = (Spinner)findViewById(R.id.spinner2);
        picktime=findViewById(R.id.dp_time);
//        droptime=findViewById(R.id.droptime);
        seatsedt=findViewById(R.id.seats);

        autoCompleteTextView.setText(getIntent().getStringExtra("pick_add"));

        drop.setText(getIntent().getStringExtra("drop_add"));
        picktime.setText(getIntent().getStringExtra("pick_time").toUpperCase());

        date.setText(getIntent().getStringExtra("date"));

        if(getIntent().getBooleanExtra("is_onetime",true))
        {
            initiate();
        }
        else
        {
            switchtousual();
        }


        seatsedt.setText(getIntent().getStringExtra("seats"));
        price.setText(getIntent().getStringExtra("price"));
        trip_id=getIntent().getStringExtra("trip_id");

        ImageButton p_seats=findViewById(R.id.p_seats);
        ImageButton m_seats=findViewById(R.id.m_seats);
        p_seats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(seatsedt.getText().toString());

                val=val+1;
                seatsedt.setText(val+"");
            }
        });
        m_seats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(seatsedt.getText().toString());
                if(val>=0)
                {
                    val=val-1;
                    seatsedt.setText(val+"");
                }

            }
        });

        ImageButton p_price=findViewById(R.id.p_price);
        ImageButton m_price=findViewById(R.id.m_price);
        p_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(price.getText().toString());

                val=val+100;
                price.setText(val+"");
            }
        });
        m_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(price.getText().toString());
                if(val>=100)
                {
                    val=val-100;
                    price.setText(val+"");
                }

            }
        });



    }
    public void MyDialogue()
    {
        final View Text_b = View.inflate(Edit_a_Ride.this, R.layout.post_ride_comments, null);
        final EditText phone = Text_b.findViewById(R.id.phone);
        final EditText comments = Text_b.findViewById(R.id.cmnts);
        final AutoCompleteTextView route = Text_b.findViewById(R.id.route);
        route.setAdapter(new PlaceAutoSuggestAdapter(Edit_a_Ride.this, android.R.layout.simple_list_item_1));
        AlertDialog.Builder builder = new AlertDialog.Builder(Edit_a_Ride.this);

        builder
                .setView(Text_b)
                .setCancelable(false)
                .setPositiveButton("POST", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(phone.getText().toString().length()==11&&comments.getText().toString().length()>0&&route.getText().length()>0)
                        {
                            Phone=phone.getText().toString();
                            Comments=comments.getText().toString();
                            Route=route.getText().toString();
                            LatLng new1=getLatLngFromAddress(route.getText().toString());
                            Route_latlong=new1.latitude+","+new1.longitude;
                            Log.e("checkroute",Route_latlong);
//
                            if(Internet.isInternetAvailable(Edit_a_Ride.this)) {
                                new POST_A_RIDE().execute();
                            }
                            else
                            {
                                Toast.makeText(Edit_a_Ride.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            Toast.makeText(Edit_a_Ride.this, "All  Fields Are Mandatory", Toast.LENGTH_SHORT).show();

                        }


                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();



    }
    public void initiate()
    {
        isonetime=true;
        isontime="1";
        is_onetime.setChecked(true);

        is_usual.setChecked(false);
        departure_date.setVisibility(View.VISIBLE);
        days.setVisibility(View.GONE);

    }
    public void switchtousual()
    {
        isontime="0";
        is_onetime.setChecked(false);
        is_usual.setChecked(true);
        departure_date.setVisibility(View.INVISIBLE);
        days.setVisibility(View.VISIBLE);
    }
    public void switchtoonetime()
    {  isontime="1";
        is_onetime.setChecked(true);
        is_usual.setChecked(false);
        departure_date.setVisibility(View.VISIBLE);
        days.setVisibility(View.GONE);
    }

    class POST_A_RIDE extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.isposted=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(Constatns.isposted=true)
            {
                autoCompleteTextView.setText("");
                drop.setText("");

                picktime.setText("");
           //     droptime.setText("");
                seatsedt.setText("");

                Toast.makeText(Edit_a_Ride.this, Server_response, Toast.LENGTH_LONG).show();
                Constatns.is_refresh=true;
                onBackPressed();
            }
            else
            {
                Toast.makeText(Edit_a_Ride.this, Server_response, Toast.LENGTH_LONG).show();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();
            String s_lat,s_long,d_lat,d_long;
            HashMap<String, String> HashMapParams = new HashMap<String, String>();
            LatLng latLng=getLatLngFromAddress(source);
            pickupltlng=latLng.latitude+","+latLng.longitude;
            s_lat=latLng.latitude+"";
            s_long=latLng.longitude+"";

            LatLng latLng2=getLatLngFromAddress(destination);
            droplatlng=latLng2.latitude+","+latLng2.longitude;
            d_lat=latLng2.latitude+"";
            d_long=latLng2.longitude+"";
            HashMapParams.put("trip_id", trip_id);
          //  HashMapParams.put("user_id", Constatns.user_id);

            HashMapParams.put("p_lat","~"+pickupltlng+"~");
            HashMapParams.put("d_lat", "~"+droplatlng+"~");
            HashMapParams.put("source",source);
            HashMapParams.put("destination", destination);
            HashMapParams.put("pick_time",timeConversion(finaltimepick));
        //    HashMapParams.put("drop_time", finaldoptime);
            HashMapParams.put("seats",seats);
            HashMapParams.put("date", mydate);
            HashMapParams.put("vehicle_id",car);
            HashMapParams.put("status", "1");
            HashMapParams.put("price", pricefinal);
            HashMapParams.put("is_onetime", isontime);
            HashMapParams.put("s_lat_s", s_lat);
            HashMapParams.put("s_long_s", s_long);
            HashMapParams.put("d_lat_s", d_lat);
            HashMapParams.put("d_long_s", d_long);
            HashMapParams.put("current_time", getdate3());
            HashMapParams.put("trip_time", mydate+finaltimepick);
//            HashMapParams.put("phone", Phone);
//            HashMapParams.put("coments", Comments);
//            HashMapParams.put("route", Route);
//            HashMapParams.put("route_latlong", "~"+Route_latlong+"~");
//            HashMapParams.put("trip_routes",source +"~"+Route+"~"+destination);
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_EDIT_RIDE, HashMapParams);
            Server_response=FinalData;
            Log.e("responsemehamdeditride",FinalData);
            if(FinalData.contains("updated")) {
                try {
                    Constatns.isposted=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);



//                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
//                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
//                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
//                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
//                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
//                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }



    class MyCars extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();


        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            langAdapter = new ArrayAdapter<CharSequence>(Edit_a_Ride.this, R.layout.spinner_text, Constatns.mycars );
            langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
            spinner.setAdapter(langAdapter);



        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id",Constatns.user_id);


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_CARS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    Constatns.mycarsid=new String[categories.length()];
                    Constatns.mycars=new String[categories.length()];
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                        Constatns.mycars[i]=(c.getString("name"));
                        Constatns.mycarsid[i]=(c.getString("vehicle_id"));







                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }
    private LatLng getLatLngFromAddress(String address){

        Geocoder geocoder=new Geocoder(Edit_a_Ride.this);
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocationName(address, 1);
            if(addressList!=null){
                Address singleaddress=addressList.get(0);
                LatLng latLng=new LatLng(singleaddress.getLatitude(),singleaddress.getLongitude());
                return latLng;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private Address getAddressFromLatLng(LatLng latLng){
        Geocoder geocoder=new Geocoder(Edit_a_Ride.this);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 5);
            if(addresses!=null){
                Address address=addresses.get(0);
                return address;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }
    private boolean checktimings(String time, String endtime) {

        String pattern = "hh:mm aa";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }
    public static String timeConversion(String Time) {
        DateFormat f1 = new SimpleDateFormat("hh:mm a"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("HH:mm");
        String x = f2.format(d); // "23:00"

        return x;
    }
    public String getdate3()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd hh:mm aa", Locale.getDefault()).format(new Date());
        return currentDate;
    }
}
