package com.axcel.co.coryds.Adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;


import com.axcel.co.coryds.Activity.post_a_Ride_for_user;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Model_upRides;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class user_rides_adpt_ extends RecyclerView.Adapter<user_rides_adpt_.MyViewHolder> {

    private List<Model_upRides> moviesList;
    Context ct;
    private ProgressDialog dialog;
String TRIP_id,payment_method;
    //chat data
    String chat_user;



    String chat_username,thread_id,from_,to_,dp;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView fname,pick,drop,date,time,seats;
        Button post_ride;

        RelativeLayout myrow;
        CircleImageView dp;

        ImageButton send_message;

        public MyViewHolder(View view) {
            super(view);
            fname = (TextView) view.findViewById(R.id.fname);
            pick = (TextView) view.findViewById(R.id.txt_strtloc);
            drop = (TextView) view.findViewById(R.id.txt_endloc);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            seats = (TextView) view.findViewById(R.id.seats);
            post_ride=  (Button) view.findViewById(R.id.post_ride);



            myrow=  view.findViewById(R.id.myrow);
            dp=(CircleImageView)view.findViewById(R.id.dp);

            send_message=  (ImageButton) view.findViewById(R.id.send_message);

        }
    }


    public user_rides_adpt_(List<Model_upRides> moviesList, Context ct) {
        this.moviesList = moviesList;
        this.ct=ct;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_rides_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        dialog = new ProgressDialog(ct);
        final Model_upRides movie = moviesList.get(position);
        holder.fname.setText(movie.getFname());
        holder.pick.setText(movie.getPick());
        holder.drop.setText(movie.getDrop());
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime().toUpperCase());
        holder.seats.setText(movie.getSeats()+" seats");

        holder.post_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(ct, post_a_Ride_for_user.class);
                i.putExtra("customer_id",movie.getOwner_id());
                i.putExtra("seats",movie.getSeats());
                i.putExtra("source",movie.getPick());
                i.putExtra("dest",movie.getDrop());
                i.putExtra("date",movie.getDate());
                i.putExtra("time",movie.getTime());
                i.putExtra("post_id",movie.getPost_id());
                i.putExtra("source_latlong","");
                i.putExtra("dest_latlong","");
                ct.startActivity(i);
            }
        });



        Picasso.get()
                .load(AppConfig.BASE_URL+movie.getpic())
                .into(holder.dp);



//                holder.message.setText("PAY NOW");






    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}