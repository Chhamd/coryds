package com.axcel.co.coryds.Config;

public class Model_enq {
    String fname,  pick,  drop, date, time, price ,seats,carnumber,carname,rideid,pic;
String rating,reviews;
int status;
    String user_id;
    String mobile;
    public Model_enq() {
    }

    public Model_enq(String fname, String pick, String drop, String date, String time,String mobile, String pic, String rating, String reviews,String user_id) {
        this.fname = fname;
        this.pick = pick;
        this.drop = drop;
        this.date=date;
        this.time = time;

        this.pic=pic;
        this.rating=rating;
        this.reviews=reviews;
        this.user_id=user_id;
        this.mobile=mobile;
    }

    public String getFname() {
        return this.fname;
    }
    public String getPick() {
        return this.pick;
    }
    public String getDrop() {
        return this.drop;
    }
    public String getDate() {
        return this.date;
    }
    public String getTime() {
        return this.time;
    }
    public String getPrice() {
        return this.price;
    }
    public String getSeats() {
        return this.seats;
    }
    public String getCarnumber() {
        return this.carnumber;
    }
    public String getCarname() {
        return this.carname;
    }
    public String getRideid() {
        return this.rideid;
    }
    public void setstatus(int status) {
         this.status=status;
    }
    public int getStatus() {
        return this.status;
    }
    public String getpic() {
        return this.pic;
    }
    public String getRating() {
        return this.rating;
    }
    public String getReviews() {
        return this.reviews;
    }
    public String getUser_id() {
        return this.user_id;
    }
    public String getMobile() {
        return this.mobile;
    }
}
