package com.axcel.co.coryds.ChildFragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.axcel.co.coryds.Adapters.RecyclerTouchListener;
import com.axcel.co.coryds.Adapters.user_rides_adpt_;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model_upRides;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Posted_Rides extends Fragment {
    private ProgressDialog dialog;
    private List<Model_upRides> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private user_rides_adpt_ mAdapter;

    DatePickerDialog startDatePicker;
    int    year,month,day;

    TextView noride;

    FloatingActionButton new_ride;
    String source_name,des_name,source_latlong,dest_latlong,pick_Date,pick_time,seats;

    @Override
    public void onResume() {
        super.onResume();
        if(Internet.isInternetAvailable(getActivity())) {

            new Show_User_rides().execute();
        }
        else
        {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.posted_rides_frag,container,false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);



        dialog = new ProgressDialog(getActivity());







        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Model movie = movieList.get(position);
//                Toast.makeText(getActivity(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        return v;
    }


    class Show_User_rides extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            movieList = new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(movieList.size()==0)
            {
//                Toast.makeText(getActivity(), "if", Toast.LENGTH_SHORT).show();
            }
            else {
//                Toast.makeText(getActivity(), "else", Toast.LENGTH_SHORT).show();
                recyclerView.setVisibility(View.VISIBLE);
                mAdapter = new user_rides_adpt_(movieList,getActivity());
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }



        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("type","driver");

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.BaseURL+"user_rides.php", HashMapParams);

            Log.e("show_user_rides",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Model_upRides movie = new Model_upRides(c.getString("name"),c.getString("pick"),c.getString("drop"),c.getString("date"),c.getString("start_time"),"1",c.getString("seats"),c.getString("post_id"),"2",c.getString("user_ride_id"),c.getString("dp"),"0","0","0","0","0","0","0",c.getString("user_ride_id"),0);
                        movieList.add(movie);




                    }
                    Log.e("show_user_rides",movieList.size()+"");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }


    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    private double distance1(double lat_a, double lng_a, double lat_b, double lng_b) {
//        float pk = (float) (180.f/Math.PI);
//
//        double a1 = lat_a / pk;
//        double a2 = lng_a / pk;
//        double b1 = lat_b / pk;
//        double b2 = lng_b / pk;
//
//        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
//        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
//        double t3 = Math.sin(a1) * Math.sin(b1);
//        double tt = Math.acos(t1 + t2 + t3);
//
//        return 6366000 * tt;

        float[] distance = new float[2];

        Location.distanceBetween( lat_a, lng_a,
                lat_b, lng_b, distance);

        return distance[0];
    }
    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }
    private LatLng getLatLngFromAddress(String address){

        Geocoder geocoder=new Geocoder(getActivity());
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocationName(address, 1);
            if(addressList!=null){
                Address singleaddress=addressList.get(0);
                LatLng latLng=new LatLng(singleaddress.getLatitude(),singleaddress.getLongitude());
                return latLng;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
