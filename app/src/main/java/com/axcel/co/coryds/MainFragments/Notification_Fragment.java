package com.axcel.co.coryds.MainFragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Adapters.Notification_dataper;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Notification_Fragment extends Fragment {
    private ProgressDialog dialog;
   ArrayList<String> notification_id;
    ArrayList<String> notification_content;
    ArrayList<String> IsRead;
    ArrayList<String> date;
    private RecyclerView recyclerView;
    private Notification_dataper mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.notification,container,false);

        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);

        dialog = new ProgressDialog(getActivity());
        if(Internet.isInternetAvailable(getActivity())) {
            new Notifications().execute();
        }
        else
        {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }



        return v;
    }
    class Notifications extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

         notification_id=new ArrayList<>();;
           notification_content=new ArrayList<>();
            IsRead=new ArrayList<>();
            date=new ArrayList<>();
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            mAdapter = new Notification_dataper(notification_id,notification_content,date,IsRead,getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("date", getdate());
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_NOTIFICATIONS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                            notification_id.add(c.getString("id"));
                        notification_content.add(c.getString("content"));
                        date.add("About "+c.getString("date"));
                        IsRead.add(c.getString("is_read"));


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
}
