package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Config.Chat_Model;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.R;

import java.util.ArrayList;

public class single_chat_users_dataper extends RecyclerView.Adapter<single_chat_users_dataper.MyViewHolder> {

   ArrayList<Chat_Model> chat;

    Context ct;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView m_name,m_date,m_message;
        public TextView y_name,y_date,y_message;
        public LinearLayout you,my;


        public MyViewHolder(View view) {
            super(view);
            m_name = (TextView) view.findViewById(R.id.m_name);
            m_date = (TextView) view.findViewById(R.id.m_date);
            m_message = (TextView) view.findViewById(R.id.m_message);

            y_name = (TextView) view.findViewById(R.id.y_name);
            y_date = (TextView) view.findViewById(R.id.y_date);
            y_message = (TextView) view.findViewById(R.id.y_message);

            you = (LinearLayout) view.findViewById(R.id.you);
            my = (LinearLayout) view.findViewById(R.id.my);

        }
    }


    public single_chat_users_dataper(ArrayList<Chat_Model> chat, Context ct) {
        this.chat = chat;
        this.ct = ct;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_chat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {



        if(Constatns.user_id.equals(chat.get(position).getFrom_()))
            {
                holder.my.setVisibility(View.VISIBLE);
                holder.you.setVisibility(View.INVISIBLE);
//                holder.m_name.setText(name.get(position));
                holder.m_message.setText(chat.get(position).getMessage());
                holder.m_date.setText(chat.get(position).getDate()+"  "+chat.get(position).getTime());
            }
       else
        {
            holder.my.setVisibility(View.INVISIBLE);
            holder.you.setVisibility(View.VISIBLE);
//                holder.y_name.setText(name.get(position));
            holder.y_message.setText(chat.get(position).getMessage());
            holder.y_date.setText(chat.get(position).getDate()+"  "+chat.get(position).getTime());

        }


    }

    @Override
    public int getItemCount() {
        return chat.size();
    }
}