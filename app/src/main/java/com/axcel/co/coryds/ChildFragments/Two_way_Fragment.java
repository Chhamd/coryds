package com.axcel.co.coryds.ChildFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.axcel.co.coryds.Activity.AddNewCar;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Location.PlaceAutoSuggestAdapter;
import com.axcel.co.coryds.Location.SingleShotLocationProvider;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;


public class Two_way_Fragment extends Fragment {
    CheckBox is_onetime,is_usual;
    EditText departure_date,dp_time;
    RelativeLayout days;
    boolean isonetime;
    TextView IS_One,IS_USUAL;
    public      String[] mycars;
    public      String[] mycarsid;
    EditText price,seats;
    ArrayAdapter<CharSequence> langAdapter;
    Spinner spinner;
    LocationManager locationManager;
    AutoCompleteTextView Pick_edt,Drop_edt;
    DatePickerDialog.OnDateSetListener date1,date2;
    private ProgressDialog dialog;;
    Button btn_upinfo;
    String source,destination,car,finaltimepick,seats_,pricefinal,mydate_d;
    String pickupltlng,droplatlng;
    String isontime;

    EditText returndate,returntime;
    DatePickerDialog startDatePicker,startDatePicker2;
    int    year,month,day;
    String  Phone,Comments,Route,Route_latlong;
    String RETURN_DATE,RETURN_TIME;
    Location gps_loc, network_loc, final_loc;
String current_loc;
    double latitude, longitude;
    ImageView curentloc;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.twoway,container,false);
        dialog = new ProgressDialog(getActivity());
        is_onetime=v.findViewById(R.id.is_onetime);
        is_usual=v.findViewById(R.id.is_usual);
        IS_One=v.findViewById(R.id.t1);
        IS_USUAL=v.findViewById(R.id.t2);
        departure_date=v.findViewById(R.id.dpdate);
        days=v.findViewById(R.id.usual1);
        spinner = v.findViewById(R.id.spinner2);
        price=v.findViewById(R.id.price);
        seats=v.findViewById(R.id.seats);
        dp_time=v.findViewById(R.id.dp_time);
        btn_upinfo=v.findViewById(R.id.btn_upinfo);
        returndate=v.findViewById(R.id.ret_date);
        returntime=v.findViewById(R.id.departure_return_time);
        curentloc=v.findViewById(R.id.location);
        initiate();
        is_onetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {isonetime=false;
                    switchtousual();
                }else {
                    switchtoonetime();
                    isonetime=true;
                }
            }
        });
        is_usual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {
                    isonetime=false;
                    switchtousual();

                }else {

                    switchtoonetime();
                    isonetime=true;
                }
            }
        });
        IS_One.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {isonetime=false;
                    switchtousual();
                }else {
                    switchtoonetime();
                    isonetime=true;
                }
            }
        });
        IS_USUAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isonetime)
                {
                    isonetime=false;
                    switchtousual();

                }else {

                    switchtoonetime();
                    isonetime=true;
                }
            }
        });

        ImageButton p_seats=v.findViewById(R.id.p_seats);
        ImageButton m_seats=v.findViewById(R.id.m_seats);
        p_seats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(seats.getText().toString());

                val=val+1;
                seats.setText(val+"");
            }
        });
        m_seats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(seats.getText().toString());
                if(val>=0)
                {
                    val=val-1;
                    seats.setText(val+"");
                }

            }
        });

        ImageButton p_price=v.findViewById(R.id.p_price);
        ImageButton m_price=v.findViewById(R.id.m_price);
        p_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(price.getText().toString());

                val=val+100;
                price.setText(val+"");
            }
        });
        m_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val=  Integer.parseInt(price.getText().toString());
                if(val>=100)
                {
                    val=val-100;
                    price.setText(val+"");
                }

            }
        });
        SingleShotLocationProvider.requestSingleUpdate(getActivity(),
                new SingleShotLocationProvider.LocationCallback() {
                    @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                        try {

                            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1);
                            if (addresses != null && addresses.size() > 0) {
                                current_loc=addresses.get(0).getAddressLine(0);
                                Log.e("checkmylocationfinal",addresses.get(0).getAddressLine(0));

                            } else {
                                current_loc=null;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("Location", "my location is " + location.toString());
                    }
                });
        curentloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_loc!=null)
                {

                    Pick_edt.setText(current_loc);

                }
                else
                {
                    Toast.makeText(getActivity(), "Waiting For Your location", Toast.LENGTH_SHORT).show();
                }

            }
        });
        Pick_edt=v.findViewById(R.id.pick);


        Pick_edt.setAdapter(new PlaceAutoSuggestAdapter(getActivity(),android.R.layout.simple_list_item_1));

        Drop_edt=v.findViewById(R.id.drop);

        Drop_edt.setAdapter(new PlaceAutoSuggestAdapter(getActivity(),android.R.layout.simple_list_item_1));


        final Calendar myCalendar = Calendar.getInstance();
        date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //   String myFormat = "yyyy/mm/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());

                departure_date.setText(sdf.format(myCalendar.getTime()));
            }

        };
        date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //   String myFormat = "yyyy/mm/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());

                returndate.setText(sdf.format(myCalendar.getTime()));
            }

        };
        startDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
                departure_date.setText(sdf.format(myCalendar.getTime()));

            }
        }, year, month, day);
        startDatePicker2 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
                returndate.setText(sdf.format(myCalendar.getTime()));

            }
        }, year, month, day);
        departure_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new DatePickerDialog(getActivity(), date1, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                startDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                startDatePicker.show();
            }
        });
        dp_time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        dp_time.setText( hour + ":" + selectedMinute+ " " + AM_PM );
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        btn_upinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                source=Pick_edt.getText().toString();
                destination=Drop_edt.getText().toString();
                car= mycarsid[spinner.getSelectedItemPosition()];
                finaltimepick=dp_time.getText().toString();

                seats_=seats.getText().toString();
                pricefinal=price.getText().toString();
                mydate_d=departure_date.getText().toString();
                RETURN_DATE=returndate.getText().toString();
                RETURN_TIME=returntime.getText().toString();
//                if(mydate.compareTo(getdate())>0 && checktimings(finaltimepick,finaldoptime) && source.length()>4 && destination.length()>4&& seats.length()==1)


                 if(source.length()>4 && destination.length()>4 )
                {
                    Log.e("datecheck",returndate.getText().toString().compareTo(mydate_d)+"");
                    if(isonetime)
                    {
                        if(returndate.getText().toString().compareTo(mydate_d)>0)
                        {
                            MyDialogue();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Return Date Is not Valid", Toast.LENGTH_SHORT).show();

//                            new POST_A_RIDE().execute();
                        }
                    }
                    else
                    {
//                        Toast.makeText(getActivity(), "you can post ride 4", Toast.LENGTH_SHORT).show();
                        MyDialogue();

//                        new POST_A_RIDE().execute();
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "Please Select Valid Credentials", Toast.LENGTH_SHORT).show();
                }
            }
        });
        returndate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new DatePickerDialog(getActivity(), date2, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                startDatePicker2.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                startDatePicker2.show();
            }
        });
        returntime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        int hour=0;
                        if(selectedHour < 12) {
                            hour=selectedHour;
                            AM_PM = "AM";
                        } else {
                            hour=selectedHour-12;
                            AM_PM = "PM";
                        }
                        returntime.setText( hour + ":" + selectedMinute+ " " + AM_PM );
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });




        return v;
    }
    public void initiate()
    {
        isonetime=true;
        isontime="1";
        is_onetime.setChecked(true);
        returndate.setVisibility(View.VISIBLE);
        returntime.setVisibility(View.VISIBLE);
        is_usual.setChecked(false);
        departure_date.setVisibility(View.VISIBLE);
        days.setVisibility(View.GONE);

    }
    public void switchtousual()
    {
        isontime="0";
        is_onetime.setChecked(false);
        is_usual.setChecked(true);
        departure_date.setVisibility(View.GONE);
        days.setVisibility(View.VISIBLE);
        returndate.setVisibility(View.GONE);
        returntime.setVisibility(View.VISIBLE);
    }
    public void switchtoonetime()
    {  isontime="1";
        is_onetime.setChecked(true);
        is_usual.setChecked(false);
        departure_date.setVisibility(View.VISIBLE);
        days.setVisibility(View.GONE);
        returndate.setVisibility(View.VISIBLE);
        returntime.setVisibility(View.VISIBLE);
    }


    class MyCars extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();


        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);


            if(mycars.length==0)
            {
//                btn_upinfo.setVisibility(View.GONE);
//                Toast.makeText(getActivity(), "There is no car, please add car in Cars section", Toast.LENGTH_SHORT).show();
                // to prevent irritating accidental logouts
//                Intent i=new Intent(getActivity(), AddNewCar.class);
//                startActivity(i);

            }
            else
            {
                btn_upinfo.setVisibility(View.VISIBLE);
                langAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_text, mycars );
                langAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
                spinner.setAdapter(langAdapter);
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id",Constatns.user_id);


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_CARS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    mycarsid=new String[categories.length()+1];
                    mycars=new String[categories.length()+1];
                    mycars[0]=("Select a car");
                    mycarsid[0]=("99");
                    for (int i = 0,j=1; i < categories.length(); i++,j++) {
                        JSONObject c = categories.getJSONObject(i);
                        mycars[j]=(c.getString("name")+"-"+c.getString("vehicle_number"));
                        mycarsid[j]=(c.getString("vehicle_id"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                mycarsid=new String[0];
                mycars=new String[0];
            }

            return FinalData;
        }
    }
    private LatLng getLatLngFromAddress(String address){

        Geocoder geocoder=new Geocoder(getActivity());
        List<Address> addressList;

        try {
            addressList = geocoder.getFromLocationName(address, 1);
            if(addressList!=null){
                Address singleaddress=addressList.get(0);
                LatLng latLng=new LatLng(singleaddress.getLatitude(),singleaddress.getLongitude());
                return latLng;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private Address getAddressFromLatLng(LatLng latLng){
        Geocoder geocoder=new Geocoder(getActivity());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 5);
            if(addresses!=null){
                Address address=addresses.get(0);
                return address;
            }
            else{
                return null;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }

    private boolean checktimings(String time, String endtime) {

        String pattern = "hh:mm aa";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }
    class POST_A_RIDE extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.isposted=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(Constatns.isposted=true)
            {
                Pick_edt.setText("");
                Drop_edt.setText("");

                dp_time.setText("");
//                droptime.setText("");
                seats.setText("");
                Toast.makeText(getActivity(), "Rides Posted", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
                Constatns.is_refresh=true;
            }
            else
            {
                Toast.makeText(getActivity(), "Error in posting", Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();
            String s_lat,s_long,d_lat,d_long;
            HashMap<String, String> HashMapParams = new HashMap<String, String>();
            LatLng latLng=getLatLngFromAddress(source);
            pickupltlng=latLng.latitude+","+latLng.longitude;
            s_lat=latLng.latitude+"";
            s_long=latLng.longitude+"";

            LatLng latLng2=getLatLngFromAddress(destination);
            droplatlng=latLng2.latitude+","+latLng2.longitude;
            d_lat=latLng2.latitude+"";
            d_long=latLng2.longitude+"";
            HashMapParams.put("current_time", getdate3());
            HashMapParams.put("trip_time", mydate_d+finaltimepick);

            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("source_lt","~"+pickupltlng+"~");
            HashMapParams.put("destination_lt", "~"+droplatlng+"~");
            HashMapParams.put("source",source);
            HashMapParams.put("destination", destination);
            HashMapParams.put("pick_time1",timeConversion(finaltimepick));
            HashMapParams.put("s_lat1", s_lat);
            HashMapParams.put("s_long1", s_long);
            HashMapParams.put("d_lat1", d_lat);
            HashMapParams.put("d_long1", d_long);
            HashMapParams.put("date1", mydate_d);

            HashMapParams.put("sourceb",destination);
            HashMapParams.put("destinationb", source);
            HashMapParams.put("source_ltb","~"+droplatlng+"~");
            HashMapParams.put("destination_ltb", "~"+pickupltlng+"~");
            HashMapParams.put("pick_time2",timeConversion(RETURN_TIME));
            HashMapParams.put("date2", RETURN_DATE);
            HashMapParams.put("s_lat2", d_lat);
            HashMapParams.put("s_long2", d_long);
            HashMapParams.put("d_lat2", s_lat);
            HashMapParams.put("d_long2", s_lat);

            HashMapParams.put("seats",seats_);

            HashMapParams.put("vehicle_id",car);
            HashMapParams.put("status", "1");
            HashMapParams.put("price", pricefinal);
            HashMapParams.put("is_onetime", isontime);




            HashMapParams.put("phone", Phone);
            HashMapParams.put("coments", Comments);
            HashMapParams.put("route", Route);
            HashMapParams.put("route_latlong", "~"+Route_latlong+"~");
            HashMapParams.put("trip_routes",source +"~"+Route+"~"+destination);
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_POST_two_RIDE, HashMapParams);
Log.e("timecheckm",timeConversion(finaltimepick)+":"+timeConversion(RETURN_TIME));
            Log.e("responsemehamdtwoway",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.isposted=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);



                        Constatns.is_number= Integer.parseInt(c.getString("is_number"));
                        Constatns.is_sms=Integer.parseInt(c.getString("is_sms"));
                        Constatns.is_chat=Integer.parseInt(c.getString("is_chat"));
                        Constatns.is_pets= Integer.parseInt(c.getString("is_pet"));
                        Constatns.is_music= Integer.parseInt(c.getString("is_music"));
                        Constatns.is_smoking= Integer.parseInt(c.getString("is_smoking"));



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    public String getdate3()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd hh:mm aa", Locale.getDefault()).format(new Date());
        return currentDate;
    }

    public static String timeConversion(String Time) {
        DateFormat f1 = new SimpleDateFormat("hh:mm a"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("HH:mm");
        String x = f2.format(d); // "23:00"

        return x;
    }
    public void MyDialogue()
    {
        final View Text_b = View.inflate(getActivity(), R.layout.post_ride_comments, null);
        final EditText phone = Text_b.findViewById(R.id.phone);
        final EditText comments = Text_b.findViewById(R.id.cmnts);
        final AutoCompleteTextView route = Text_b.findViewById(R.id.route);
        route.setAdapter(new PlaceAutoSuggestAdapter(getActivity(), android.R.layout.simple_list_item_1));
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                .setView(Text_b)
                .setCancelable(false)
                .setPositiveButton("POST", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(phone.getText().toString().length()==11&&comments.getText().toString().length()>0&&route.getText().length()>0)
                        {
                            Phone=phone.getText().toString();
                            Comments=comments.getText().toString();
                            Route=route.getText().toString();
                            LatLng new1=getLatLngFromAddress(route.getText().toString());
                            Route_latlong=new1.latitude+","+new1.longitude;
                            Log.e("checkroute",Route_latlong);
//
                            if(Internet.isInternetAvailable(getActivity())) {
                                new POST_A_RIDE().execute();
                            }
                            else
                            {
                                Toast.makeText(getActivity(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "All  Fields are Mandatory", Toast.LENGTH_SHORT).show();

                        }


                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();



    }
    @Override
    public void onResume() {

        new MyCars().execute();

        super.onResume();
    }
}
