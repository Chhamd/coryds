package com.axcel.co.coryds.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Activity.Rate_now;
import com.axcel.co.coryds.Activity.RideDetail;
import com.axcel.co.coryds.ChildFragments.Onway_Fragment;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Chat_Users_Model;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Past_model;
import com.axcel.co.coryds.Location.PlaceAutoSuggestAdapter;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.axcel.co.coryds.service.TrackerService;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LOCATION_SERVICE;

public class Ride_detail_adpt extends RecyclerView.Adapter<Ride_detail_adpt.ViewHolder> {
 ArrayList<String> user_id,user_name,user_pic,price,from,to,mobile,payment_method,Ride_id;
 ArrayList<String> IS_PICKED,IS_ACTIVE,IS_RATED,IS_DROPED,customer_Rating;
 String date,vehicle_name,vehicle_number;
 String trip_id, ride_id,driver_id;
    Context ct;
    int flag;
    private ProgressDialog dialog;
    String MyUsser_id;
    boolean is_picked,is_Droped;
    ArrayList<String> payment_status;
    //
    //
    String chat_user;
    String FinalData;
            //chat data

    String chat_username,thread_id,from_,to_,dp;
    private static final int PERMISSIONS_REQUEST = 1;
    public Ride_detail_adpt(ArrayList<String> user_id,ArrayList<String> user_name,ArrayList<String> user_pic,ArrayList<String> price,ArrayList<String> from ,ArrayList<String> to,String vehicle_name,String vehicle_number,String date,String trip_id,String ride_id,String driver_id,ArrayList<String> mobile,int flag,ArrayList<String> IS_PICKED,ArrayList<String> is_active,ArrayList<String> IS_DROPED,ArrayList<String> customer_Rating,ArrayList<String> IS_RATED,ArrayList<String> payment_status,ArrayList<String> payment_method,ArrayList<String> Ride_id,Context ct)
    {
        this.ct=ct;
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_pic = user_pic;
        this.price = price;
        this.from = from;
        this.to = to;

        this.vehicle_name = vehicle_name;
        this.vehicle_number = vehicle_number;
        this.date = date;
        this.trip_id = trip_id;
        this.ride_id = ride_id;
        this.driver_id = driver_id;
        this.flag=flag;
        this.mobile=mobile;

        this.IS_PICKED=IS_PICKED;
        this.IS_ACTIVE=is_active;
        this.IS_DROPED=IS_DROPED;
        this.customer_Rating=customer_Rating;
        this.IS_RATED=IS_RATED;
        this.payment_status=payment_status;
        this.payment_method=payment_method;
        this.Ride_id=Ride_id;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView img_profile;
        ImageView status;
        TextView txt_pname,txt_vehicle,txt_fare,txt_seats,txt_date,stloc,endloc;
        Button rating;
        ImageButton call,msg;
        RatingBar rating_custom;


        public ViewHolder(View itemView)
        {
            super(itemView);
            this.img_profile=itemView.findViewById(R.id.v_image);
            this.txt_pname=itemView.findViewById(R.id.txt_pname);

            this.txt_fare =itemView.findViewById(R.id.txt_fare);
            this.txt_seats=itemView.findViewById(R.id.txt_seats);
            this.txt_date =itemView.findViewById(R.id.txt_date);
            this.stloc=itemView.findViewById(R.id.txt_strtloc);
            this.endloc=itemView.findViewById(R.id.txt_endloc);
            this.rating=itemView.findViewById(R.id.btn_rating);
            this.call=itemView.findViewById(R.id.btncall);
            this.msg=itemView.findViewById(R.id.btnMsg);
            this.status=itemView.findViewById(R.id.status);
            this.rating_custom=itemView.findViewById(R.id.rating);


        }
    }

    @Override
    public Ride_detail_adpt.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ride_detail_rating, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final Ride_detail_adpt.ViewHolder holder, final int position)
    {
        dialog = new ProgressDialog(ct);
        holder.txt_pname.setText(user_name.get(position));
        holder.txt_fare.setText("RS: "+price.get(position));
        holder.stloc.setText(from.get(position));
        holder.endloc.setText(to.get(position));
        holder.rating_custom.setRating(Float.parseFloat(customer_Rating.get(position)));
        Picasso.get()
                .load(AppConfig.BASE_URL+user_pic.get(position))
                .into(holder.img_profile);
        if(flag==0)
        {



            if(IS_ACTIVE.get(position).contains("1")&& IS_PICKED.get(position).contains("1") && IS_DROPED.get(position).contains("1"))
            {
                if(IS_RATED.get(position).contains("1"))
                {
                    holder.status.setImageResource(R.drawable.completed);
                    holder.rating.setText("RATED");
                }
                else
                {
                    holder.status.setImageResource(R.drawable.completed);
                    holder.rating.setText("RATE NOW");
                }

            }
            else if(IS_ACTIVE.get(position).contains("1")&& IS_PICKED.get(position).contains("1") && IS_DROPED.get(position).contains("0"))
            {
                holder.status.setImageResource(R.drawable.ongoing);
                holder.rating.setText("On Going");
            }
            else if(IS_ACTIVE.get(position).contains("1")&& IS_PICKED.get(position).contains("0") && IS_DROPED.get(position).contains("0"))
            {
                holder.status.setImageResource(R.drawable.notpicked);
                holder.rating.setText("Not Picked");
            }
            else if(IS_ACTIVE.get(position).contains("0")&& IS_PICKED.get(position).contains("0") && IS_DROPED.get(position).contains("0"))
            {
                holder.status.setImageResource(R.drawable.inactive);
                holder.rating.setText("Not Active");
            }

        }
        else if(flag==1)
        {
            holder.rating.setText("Call Now");
        }
        else if(flag==2)
        {

            if(IS_ACTIVE.get(position).contains("1")&&payment_status.get(position).contains("1"))
            {

                if(IS_PICKED.get(position).contains("0"))
                {
                    holder.rating.setText("Pick");
                }
                if(IS_PICKED.get(position).contains("1"))
                {
                    Constatns.t_ride_id=ride_id;
                    Constatns.t_driver_id=Constatns.user_id;
                    Constatns.t_customer_id=MyUsser_id;
                    Constatns.t_srouce="start";
                    Constatns.t_destination="end";
//                    if(!isMyServiceRunning(TrackerService.class)){
//                        ct.startService(new Intent(ct, TrackerService.class));
//                        //Toast.makeText(getApplicationContext(),"tracking",Toast.LENGTH_LONG).show();
//                    }
                    holder.rating.setText("DROP");
                }
                 if(IS_DROPED.get(position).contains("1") )
                {
                    holder.rating.setText("RATE NOW");
                }
                if(IS_RATED.get(position).contains("1"))
                {
                    holder.status.setImageResource(R.drawable.completed);
                    holder.rating.setText("RATED");
                }

            }else if(IS_ACTIVE.get(position).contains("1")&&payment_status.get(position).contains("0"))
            {
                holder.rating.setText("NOT PAYED");
            }
            else
            {
                holder.rating.setText("Not Active");
            }

        }
        else if(flag==3)
        {
            holder.rating.setText("Message");
        }
        else if(flag==4)
        {

            holder.rating.setVisibility(View.INVISIBLE);
            holder.call.setVisibility(View.VISIBLE);

            holder.msg.setVisibility(View.VISIBLE);

        }
        else if (flag==5)
        {
            holder.rating.setVisibility(View.INVISIBLE);
            holder.call.setVisibility(View.VISIBLE);

            holder.msg.setVisibility(View.VISIBLE);
        }

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile.get(position), null));
                ct.startActivity(intent);
            }
        });
        holder.msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dp=user_pic.get(position);
                chat_user=user_id.get(position);
                chat_username=user_name.get(position);
                if(Internet.isInternetAvailable(ct)) {
                    new StartChatOrFindPrev().execute();
                }
                else
                {
                    Toast.makeText(ct, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }




            }
        });
        holder.rating.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(flag==0)
                {
                    if(IS_ACTIVE.get(position).contains("1")&& IS_PICKED.get(position).contains("1") && IS_DROPED.get(position).contains("1")&& IS_RATED.get(position).contains("0"))
                    {
                        Intent i=new Intent(ct, Rate_now.class);
                        i.putExtra("date",date);
                        i.putExtra("price",price.get(position));
                        i.putExtra("name",user_name.get(position));
                        i.putExtra("vehicle",vehicle_name+" | "+vehicle_number);
                        i.putExtra("dp",AppConfig.BASE_URL+user_pic.get(position));
                        i.putExtra("trip_id",trip_id);
                        i.putExtra("ride_id",Ride_id.get(position));
                        i.putExtra("user_id",user_id.get(position));
                        i.putExtra("driver_id",driver_id);
                        i.putExtra("payment_method",payment_method.get(position));

                        ct.startActivity(i);
                        Log.e("payment",payment_method.get(position));
                    }
                    else {
                        Toast.makeText(ct, "You Can Not Rate ", Toast.LENGTH_SHORT).show();
                    }

                }
                else if(flag==1)
                {

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile.get(position), null));
                    ct.startActivity(intent);
                }
                else if(flag==2)
                {


                    if(IS_ACTIVE.get(position).contains("1") && payment_status.get(position).contains("1"))
                    {
                        if(IS_PICKED.get(position).contains("0") && IS_DROPED.get(position).contains("0"))
                        {
                            if(Internet.isInternetAvailable(ct)) {
                                holder.rating.setText("DROP");
                                MyUsser_id= user_id.get(position);
                                holder.status.setImageResource(R.drawable.ongoing);

                                new Pick_customer().execute();
                            }
                            else
                            {
                                Toast.makeText(ct, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }


                        }
                        else if(IS_PICKED.get(position).contains("1") && IS_DROPED.get(position).contains("0"))
                        {

                            if(Internet.isInternetAvailable(ct)) {
                                holder.status.setImageResource(R.drawable.completed);
                                holder.rating.setText("RATE NOW");
                                MyUsser_id= user_id.get(position);

                                new Drop().execute();
                            }
                            else
                            {
                                Toast.makeText(ct, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else if(IS_ACTIVE.get(position).contains("1")&& IS_PICKED.get(position).contains("1") && IS_DROPED.get(position).contains("1") && IS_RATED.get(position).contains("0"))
                        {
                            Intent i=new Intent(ct, Rate_now.class);
                            i.putExtra("date",date);
                            i.putExtra("price",price.get(position));
                            i.putExtra("name",user_name.get(position));
                            i.putExtra("vehicle",vehicle_name+" | "+vehicle_number);
                            i.putExtra("dp",AppConfig.BASE_URL+user_pic.get(position));
                            i.putExtra("trip_id",trip_id);
                            i.putExtra("ride_id",Ride_id.get(position));
                            i.putExtra("user_id",user_id.get(position));
                            i.putExtra("driver_id",driver_id);
                            i.putExtra("payment_method",payment_method.get(position));
                            ct.startActivity(i);
                        }
                    }
                    else if(IS_ACTIVE.get(position).contains("1") && payment_status.get(position).contains("0"))
                    {
                        holder.rating.setText("NOT PAYED");
                        Toast.makeText(ct, "Customer Not Payed Yet", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        holder.rating.setText("Not Active");
                    }




                }
                else if(flag==3)
                {
                    Toast.makeText(ct, "Comming Soon", Toast.LENGTH_SHORT).show();
                }




            }
        });





    }

    @Override
    public int getItemCount()
    {
        return user_id.size();
    }
    class Pick_customer extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            is_picked=false;


            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
if(is_picked)
{
    Constatns.t_ride_id=ride_id;
    Constatns.t_driver_id=Constatns.user_id;
    Constatns.t_customer_id=MyUsser_id;
    Constatns.t_srouce="start";
    Constatns.t_destination="end";
//    if(!isMyServiceRunning(TrackerService.class)){
//        ct.startService(new Intent(ct, TrackerService.class));
//
//    }
//    updaterowpick(MyUsser_id);
    is_picked=false;
    MyUsser_id="";


    Toast.makeText(ct, "Picked Successfully", Toast.LENGTH_SHORT).show();
}
else
{
    Toast.makeText(ct, "Server Error", Toast.LENGTH_SHORT).show();
}

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("trip_id", trip_id);
            HashMapParams.put("user_id", MyUsser_id);
            HashMapParams.put("ride_id", ride_id);
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_PICK_CUSTOMER, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {

                is_picked=true;
            }
            else
            {
                is_picked=false;
            }


            return FinalData;
        }
    }
    class Drop extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            is_Droped=false;


            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if(is_Droped)
            {
                updaterowdrop(MyUsser_id);
                is_Droped=false;
                MyUsser_id="";
//                ct.stopService(new Intent(ct, TrackerService.class));
//                MyDialogue();
                Toast.makeText(ct, "Droped Successfully", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(ct, "Server Error", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("trip_id", trip_id);
            HashMapParams.put("user_id", MyUsser_id);
            HashMapParams.put("ride_id", ride_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_DROP_CUSTOMER, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {

                is_Droped=true;
            }
            else
            {
                is_Droped=false;
            }


            return FinalData;
        }
    }




    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) ct.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void MyDialogue()
    {
        final View Text_b = View.inflate(ct, R.layout.cashpopup, null);
        final TextView Textamount = Text_b.findViewById(R.id.ammount);
        final EditText ammount = Text_b.findViewById(R.id.ammount_added);
        final Button cancel = Text_b.findViewById(R.id.cancel);
        final Button submit = Text_b.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ct, "Payment succeess", Toast.LENGTH_SHORT).show();
            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(ct);

        builder
                .setView(Text_b)
                .setCancelable(false)
                .show();


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) ct).onBackPressed();
            }
        });
    }

    public void updaterowpick(String userid)
    {
        int index=0;
        for(int i=0;i<user_id.size();i++)
        {
            if(user_id.get(i)==userid)
            {
                index=i;
            }
        }
        IS_PICKED.set(index,"1");
        notifyDataSetChanged();

    }
    public void updaterowdrop(String userid)
    {
        int index=0;
        for(int i=0;i<user_id.size();i++)
        {
            if(user_id.get(i)==userid)
            {
                index=i;
            }
        }
        IS_DROPED.set(index,"1");
        notifyDataSetChanged();

    }
    class StartChatOrFindPrev extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {




            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
if(!FinalData.contains("error"))
{
    Intent i=new Intent(ct, Chat_Screen.class);
    i.putExtra("username",chat_username);
    i.putExtra("thread_id",thread_id);
    i.putExtra("from_",to_);
    i.putExtra("to_",from_);
    i.putExtra("dp",dp);
    ct.startActivity(i);
}
else
{
    Toast.makeText(ct, FinalData.replace("error",""), Toast.LENGTH_SHORT).show();
}


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();




            HashMapParams.put("owner",chat_user);
            HashMapParams.put("user_id",Constatns.user_id);
            HashMapParams.put("type","1");
             FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_CHAT_IF_ELSE, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {

                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
//                        Chat_Users_Model movie = new Chat_Users_Model(c.getString("thread_id"),c.getString("from_"),c.getString("to_"),c.getString("subject"),c.getString("user_name"),c.getString("dp"),c.getString("date"));

                        thread_id=c.getString("thread_id");
                        from_=Constatns.user_id;
                        to_=chat_user;







                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            return FinalData;
        }
    }
}

