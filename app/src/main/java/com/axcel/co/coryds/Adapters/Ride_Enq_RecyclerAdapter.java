package com.axcel.co.coryds.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Activity.Verification;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.Config.Model_enq;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Ride_Enq_RecyclerAdapter extends RecyclerView.Adapter<Ride_Enq_RecyclerAdapter.MyViewHolder> {

    private List<Model_enq> moviesList;
    Context ct;
    public ProgressDialog dialog;
String user_id;
    String chat_user;
    String chat_username,thread_id,from_,to_,DP;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView fname,pick,drop,date,time,price,seats,cartype;
        TextView reject;
        Button accept;
        ImageView status;
        RelativeLayout myrow;
        CircleImageView dp;
        RatingBar rating;
        TextView reviews;
ImageButton message,call;

        public MyViewHolder(View view) {
            super(view);
            fname = (TextView) view.findViewById(R.id.name);
            pick = (TextView) view.findViewById(R.id.pick);
            drop = (TextView) view.findViewById(R.id.drop);
            date = (TextView) view.findViewById(R.id.datetime);
          //  time = (TextView) view.findViewById(R.id.time);
            seats = (TextView) view.findViewById(R.id.seats);
            message=  (ImageButton) view.findViewById(R.id.message);
            call=  (ImageButton) view.findViewById(R.id.call);
          //  status=  (ImageView) view.findViewById(R.id.status);
            cartype  =  (TextView) view.findViewById(R.id.vehicle);
          //  price=   (TextView) view.findViewById(R.id.price);
           // myrow=  view.findViewById(R.id.myrow);
            dp=view.findViewById(R.id.dp);
            rating=view.findViewById(R.id.rating);
            reviews=view.findViewById(R.id.txt_reviews);
            dialog = new ProgressDialog(ct);
        }
    }


    public Ride_Enq_RecyclerAdapter(List<Model_enq> moviesList, Context ct) {

        this.moviesList = moviesList;
        this.ct=ct;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enq_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Model_enq movie = moviesList.get(position);
        holder.fname.setText(movie.getFname());
        holder.pick.setText(movie.getPick());
        holder.drop.setText(movie.getDrop());
        holder.date.setText(movie.getDate()+" "+movie.getTime().toUpperCase());


        holder.rating.setRating(Float.parseFloat(movie.getRating()));
        holder.reviews.setText("( "+movie.getReviews()+" Reviews)");
        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_user=movie.getUser_id();
                chat_username=movie.getFname();
                new StartChatOrFindPrev().execute();
            }
        });
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", movie.getMobile(), null));
                ct.startActivity(intent);
            }
        });
//        holder.accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//            }
//        });
//        holder.reject.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });

//    if(movie.getStatus()==0)
//    {
//        holder.status.setImageResource(R.drawable.inactive);
//    }
//    else
//    {
//        holder.status.setImageResource(R.drawable.pending);
//    }
        Picasso.get()
                .load(AppConfig.BASE_URL+movie.getpic())
                .into(holder.dp);





//        holder.myrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent(ct, RideDetail.class);
//                i.putExtra("ride_id",movie.getRideid());
//                ct.startActivity(i);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public void removeItem(int position) {
        moviesList.remove(position);
        notifyItemRemoved(position);
    }
    class StartChatOrFindPrev extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {




            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);

            Intent i=new Intent(ct, Chat_Screen.class);
            i.putExtra("username",chat_username);
            i.putExtra("thread_id",thread_id);
            i.putExtra("from_",to_);
            i.putExtra("to_",from_);
            i.putExtra("dp",DP);
            ct.startActivity(i);

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("owner",chat_user );
            HashMapParams.put("user_id",Constatns.user_id);
            HashMapParams.put("type","1");


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_CHAT_IF_ELSE, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {

                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
//                        Chat_Users_Model movie = new Chat_Users_Model(c.getString("thread_id"),c.getString("from_"),c.getString("to_"),c.getString("subject"),c.getString("user_name"),c.getString("dp"),c.getString("date"));
//                        chat_username=c.getString("user_name");
                        thread_id=c.getString("thread_id");
                        from_=Constatns.user_id;
                        to_=chat_user;







                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            return FinalData;
        }
    }
}