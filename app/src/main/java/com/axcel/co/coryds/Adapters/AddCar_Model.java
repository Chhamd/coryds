package com.axcel.co.coryds.Adapters;

public class AddCar_Model {
    String vimage;
String vmodel,vname,vno,id,color,comfort;

    public AddCar_Model(String vimage, String vmodel, String vname, String vno, String id,String color,String comfort) {
        this.vimage = vimage;
        this.vmodel = vmodel;
        this.vname = vname;
        this.vno = vno;
        this.id = id;
        this.color = color;
        this.comfort=comfort;
    }

    public String getVimage() {
        return vimage;
    }
    public String getColor() {
        return color;
    }
    public void setVimage(String vimage) {
        this.vimage = vimage;
    }

    public String getVmodel() {
        return vmodel;
    }

    public void setVmodel(String vmodel) {
        this.vmodel = vmodel;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getVno() {
        return vno;
    }

    public void setVno(String vno) {
        this.vno = vno;
    }

    public String getId() {
        return id;
    }

    public void setSeats(String seats) {
        this.id = id;
    }
    public String getComfort() {
        return comfort;
    }
}
