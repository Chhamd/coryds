package com.axcel.co.coryds.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Adapters.AddCar_Model;
import com.axcel.co.coryds.Adapters.CarAdd_Adapter;
import com.axcel.co.coryds.Adapters.Upcoming_Adapter;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class My_Cars extends AppCompatActivity {
    RecyclerView rcy_addcar;
    ArrayList<AddCar_Model> arrayList=new ArrayList<>();
    AddCar_Model model;
    private ProgressDialog dialog;
    Button addcar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my__cars);

        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dialog = new ProgressDialog(My_Cars.this);
        rcy_addcar=findViewById(R.id.rcy_addcar);
        addcar=findViewById(R.id.addcar);

        addcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(My_Cars.this,AddNewCar.class);

                startActivity(i);
            }
        });
//        if(Internet.isInternetAvailable(My_Cars.this)) {
//            new MyCars().execute();
//        }
//        else
//        {
//            Toast.makeText(My_Cars.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
//        }

    }
    class MyCars extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            arrayList=new ArrayList<>();

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);

if(arrayList.size()==0)
{
    Toast.makeText(My_Cars.this, "There Is No Car", Toast.LENGTH_SHORT).show();
}
else
{
    rcy_addcar.setLayoutManager(new LinearLayoutManager(My_Cars.this));
    CarAdd_Adapter adapter=new CarAdd_Adapter(arrayList,My_Cars.this);
    rcy_addcar.setAdapter(adapter);

}


        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id", Constatns.user_id);


            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_CARS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {
                    Constatns.islogedin=true;
                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    Constatns.mycarsid=new String[categories.length()];
                    Constatns.mycars=new String[categories.length()];
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);
                        Constatns.mycars[i]=(c.getString("name")+"-"+c.getString("vehicle_number"));
                        Constatns.mycarsid[i]=(c.getString("vehicle_id"));
                        model=new AddCar_Model("https://coryds.com/uploads/vehicle/thumbnails/"+c.getString("logo"),c.getString("model"),c.getString("name"),c.getString("vehicle_number"),c.getString("vehicle_id"),c.getString("color"),c.getString("seats"));
                        arrayList.add(model);






                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                Constatns.islogedin=false;
            }

            return FinalData;
        }
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }

    @Override
    protected void onResume() {
        if(Internet.isInternetAvailable(My_Cars.this)) {
            new MyCars().execute();
        }
        else
        {
            Toast.makeText(My_Cars.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
        super.onResume();
    }
}
