package com.axcel.co.coryds.Activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.AppInterruptHandler;
import com.axcel.co.coryds.Config.BottomMenuHelper;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.MainFragments.Chat_Fragment;
import com.axcel.co.coryds.MainFragments.MyRequest_Fragment_Driver;
import com.axcel.co.coryds.MainFragments.MyRide_Fragment_Driver;
import com.axcel.co.coryds.MainFragments.Notification_Fragment;
import com.axcel.co.coryds.MainFragments.Wallet_Fragment;
import com.axcel.co.coryds.R;

import com.axcel.co.coryds.Utils.Internet;
import com.axcel.co.coryds.service.TrackerService;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.core.content.FileProvider.getUriForFile;

public class MainActivity_Driver extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener {
   //Views Declaration-----------------------------------------------------
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    public static BottomNavigationView btm_nav;
    FrameLayout mframeLayout;
    CircleImageView dp;
    private int GALLERY = 1, CAMERA = 2;
    Uri mImageUri;
    ProgressDialog dialog;
    Bitmap bitmap;
    public static MainActivity_Driver mainact;

    private static final int PERMISSIONS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_driver);

        overridePendingTransition(R.anim.slideinright, R.anim.slideoutright);
        //Views Initialization-----------------------------------------------
        toolbar = findViewById(R.id.mtoolbar);
        btm_nav=findViewById(R.id.btm_nav);
        mframeLayout=findViewById(R.id.mframelayout);
        mainact=new MainActivity_Driver();
        dialog=new ProgressDialog(MainActivity_Driver.this);

        //set custom toolbar and drawer work-------------------------------------
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.mdrawer);
        drawerLayout.addDrawerListener(toggle);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open,
                R.string.close);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        View header= navigationView.getHeaderView(0);
        dp=header.findViewById(R.id.dp);
        TextView username=header.findViewById(R.id.username);
        RatingBar rating=header.findViewById(R.id.rating);
        Button cam=header.findViewById(R.id.cam);
        Log.e("dppp",AppConfig.BASE_URL+Constatns.dp);
        Picasso.get()
                .load(AppConfig.BASE_URL+Constatns.dp).placeholder(R.drawable.default_pic)
                .into(dp);


        username.setText(Constatns.fname+" "+Constatns.lname);

        rating.setRating(Float.parseFloat(Constatns.rating));
        cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Internet.isInternetAvailable(MainActivity_Driver.this)) {
                    showPictureDialog();
                }
                else
                {
                    Toast.makeText(MainActivity_Driver.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        if(Constatns.notifications>0) {
            BottomMenuHelper.showBadge(this, btm_nav, R.id.menu_noti, Constatns.notifications + "");
        }
        if(Constatns.rides>0)
        {
            BottomMenuHelper.showBadge(this, btm_nav, R.id.menu_req, Constatns.rides+"");
        }

        //set default fragment----------------------------------------------

       if(savedInstanceState==null){
           Fragment f=null;
           FragmentManager fm=getSupportFragmentManager();
           FragmentTransaction ft=fm.beginTransaction();

           if(getIntent().getIntExtra("flag",9)==0)
           { toolbar.setTitle("RIDES");
               f=new MyRide_Fragment_Driver();
               btm_nav.setSelectedItemId(R.id.menu_myride);
           }
           else if(getIntent().getIntExtra("flag",9)== 1)
           { toolbar.setTitle("REQUESTS");
               f=new MyRequest_Fragment_Driver();
               btm_nav.setSelectedItemId(R.id.menu_req);
           }
           else if(getIntent().getIntExtra("flag",9)== 2)
           {
               toolbar.setTitle("CHAT");
               f=new Chat_Fragment();
               btm_nav.setSelectedItemId(R.id.menu_chats);
           }
           else if(getIntent().getIntExtra("flag",9)== 4)
           {
               toolbar.setTitle("WALLET");
               f=new Wallet_Fragment();
               btm_nav.setSelectedItemId(R.id.menu_wallet);
           }
           else if(getIntent().getIntExtra("flag",9)== 8)
           {
               Intent i =new Intent(MainActivity_Driver.this, Ratings.class);
               startActivity(i);
           }
           if(f!=null)
           {
               ft.replace(R.id.mframelayout,f);
               ft.commit();
           }



       }

        //bottom navigation work------------------------------------------------
        btm_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(MainActivity_Driver.this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};


        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), GALLERY);
    }
    public void takePhotoFromCamera() {

       String filename="";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        filename=System.currentTimeMillis() + ".jpg";
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(filename));
        mImageUri=getCacheImagePath(filename);
//        DATA.uri.add(mImageUri);

        startActivityForResult(takePictureIntent, CAMERA);

    }
    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(MainActivity_Driver.this, getPackageName() + ".provider", image);
    }
    // on options menu------------------------------------------------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // navigation drawer listeners-------------------------------------------------
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        //  Fragment fragment=null;
        if (id == R.id.verification) {
//if(Constatns.is_verified.equals("1"))
//{


            if(Constatns.is_form_submit.equals("0")&&Constatns.is_admin_approved.equals("0"))
            {
                Intent i =new Intent(MainActivity_Driver.this, Verification.class);
                startActivity(i);
            }
            else if(Constatns.is_form_submit.equals("1")&&Constatns.is_admin_approved.equals("0"))
            {
                Toast.makeText(this, "Your Verification Is In Progress", Toast.LENGTH_SHORT).show();
            }
            else if(Constatns.is_form_submit.equals("1")&&Constatns.is_admin_approved.equals("1"))
            {
                Toast.makeText(this, "Your Are Already Verified", Toast.LENGTH_SHORT).show();

            }
//}
//else
//{
//    Toast.makeText(this, "Verification Not Required", Toast.LENGTH_SHORT).show();
//}
        }
        if (id == R.id.profile) {
            Intent i =new Intent(MainActivity_Driver.this, User_Profile.class);
            startActivity(i);
        }

        if (id == R.id.menu_mycars) {
            Intent i =new Intent(MainActivity_Driver.this, My_Cars.class);
            startActivity(i);
        }
        if (id == R.id.menu_ratings) {
            Intent i =new Intent(MainActivity_Driver.this, Ratings.class);
            startActivity(i);
        }



        else if (id == R.id.logout) {
         new Logout_server().execute();
        } else if (id == R.id.help) {
            Intent i =new Intent(MainActivity_Driver.this, Faqs.class);
            startActivity(i);
        }
        else if (id == R.id.contact) {
//            String url = "https://coryds.com/index.php/contact/";
//            Intent i = new Intent(Intent.ACTION_VIEW);
//            i.setData(Uri.parse(url));
//            startActivity(i);

            Intent i =new Intent(MainActivity_Driver.this, Contat_us.class);
            startActivity(i);
        }
        else if (id == R.id.rateus) {
            String url = "https://play.google.com/store/apps/details?id=com.axcel.co.coryds";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        else if (id == R.id.Share) {
            String url = "Download CoRyds from  https://play.google.com/store/apps/details?id=com.axcel.co.coryds";
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("text/plain");

            intent.putExtra(android.content.Intent.EXTRA_TEXT, url);
            /*Fire!*/
            startActivity(Intent.createChooser(intent, "Share via"));
        }

        // FragmentManager fm = getSupportFragmentManager();
        //FragmentTransaction ft = fm.beginTransaction();
        //ft.replace(R.id.frame_lay2, fragment);
        //ft.commit();
        navigationView.setCheckedItem(id);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;


    }


    //bottom navigation listener--------------------------------------------------
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment f=null;
            FragmentManager fm=getSupportFragmentManager();
            FragmentTransaction ft=fm.beginTransaction();
            switch (item.getItemId()) {
                case R.id.menu_myride:
                    toolbar.setTitle("RIDES");
                  f=new MyRide_Fragment_Driver();
                    ft.replace(R.id.mframelayout,f);
                    ft.commit();
                    return true;
                case R.id.menu_req:
                    toolbar.setTitle("REQUESTS");
                    f=new MyRequest_Fragment_Driver();
                    ft.replace(R.id.mframelayout,f);
                    ft.commit();
                    return true;
                case R.id.menu_noti:
                    BottomMenuHelper.removeBadge( btm_nav, R.id.menu_noti);
                    Constatns.notifications=Constatns.notifications-Constatns.notifications;
                    toolbar.setTitle("NOTIFICATIONS");///New RIde
                    f=new Notification_Fragment();
                    ft.replace(R.id.mframelayout,f);
                    ft.commit();
                    return true;
                case R.id.menu_chats:
                    toolbar.setTitle("CHATS");
                    f=new Chat_Fragment();
                    ft.replace(R.id.mframelayout,f);
                    ft.commit();
                    return true;

                case R.id.menu_wallet:
                    toolbar.setTitle("WALLET");
                    f=new Wallet_Fragment();
                    ft.replace(R.id.mframelayout,f);
                    ft.commit();
                    return true;

            }

            return false;
        }
    };


    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

          // bye
    }
     @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // bottom.setVisibility(View.GONE);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {

            if(data.getData()!=null){


//                try {
                    Uri mImageUri=data.getData();
                    Picasso.get()
                            .load(mImageUri)
                            .into(dp);
                try {
                    bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                    new update_profile_img().execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//
//                    img.setImageBitmap();

//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }
        } else if (requestCode == CAMERA) {


            Picasso.get()
                    .load(mImageUri)
                    .into(dp);

            try {
                bitmap=MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageUri);
                new update_profile_img().execute();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        // bottom.setVisibility(View.GONE);



    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }



    class Logout_server extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {


            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {

            dialog.dismiss();
            super.onPostExecute(string1);

            SharedPreferences.Editor editor = getSharedPreferences("LoginValues", MODE_PRIVATE).edit();
            editor.putString("username", " ");
            editor.putString("phone", " ");
            editor.putString("password", " ");
            editor.putString("token", " ");

            editor.apply();
            Intent i =new Intent(MainActivity_Driver.this, Login_Activity.class);
            startActivity(i);

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("phone",Constatns.mobile);


//            HashMapParams.put("image_name", Constatns.user_id+getdate()+"profile_user.png".replace(" ",""));

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_LOGOUT, HashMapParams);

            Log.e("responsemehamd",FinalData);
//            if(!FinalData.contains("error"))
//            {
//                Constatns.dp= FinalData;
//            }



            return FinalData;
        }
    }
    class update_profile_img extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {


            dialog.setMessage("Uploading . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {

            dialog.dismiss();
            super.onPostExecute(string1);

            Toast.makeText(MainActivity_Driver.this, "Your Profile Picture Is Updated.", Toast.LENGTH_SHORT).show();

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id",Constatns.user_id);

            HashMapParams.put("image", getStringImage(bitmap));
            HashMapParams.put("image_name", Constatns.user_id+getdate()+"profile_user.png".replace(" ",""));

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.UPDATE_PROFILE_IMG, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error"))
            {
                Constatns.dp= FinalData;
            }



            return FinalData;
        }
    }
    public String getdate()
    {  String currentDate="";

        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        return currentDate;
    }
    public static void shownewreq()
    {
        if(Constatns.rides>0)
        {
            BottomMenuHelper.showBadge(mainact, btm_nav, R.id.menu_req, Constatns.rides+"");
        }

    }}



