package com.axcel.co.coryds.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.axcel.co.coryds.ChildFragments.Two_way_Fragment;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class Rate_now extends AppCompatActivity {
Button submit;
    TextView txt_vehicle,txt_pname,Date,ammount_title;
CircleImageView img;
    private ProgressDialog dialog;
String User_id,Driver_id,Rating,Feedback,Trip_id,Ride_id;

         RatingBar rating;
    Boolean is_success;
    EditText price_;
    int old_price,Actual_price;
    String paid;
    String payment_method;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_now);
        dialog = new ProgressDialog(Rate_now.this);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rating=findViewById(R.id.rating);
        is_success=false;
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Rating=rating+"";
            }
        });
        submit=findViewById(R.id.btn_submit);
        final EditText feedback=findViewById(R.id.feedback);

        price_=findViewById(R.id.price_);
        txt_vehicle=findViewById(R.id.txt_vehicle);
        txt_pname=findViewById(R.id.txt_pname);
        Date=findViewById(R.id.date);
        ammount_title=findViewById(R.id.ammount_title);
old_price=Integer.parseInt(getIntent().getStringExtra("price"));
        Actual_price=old_price;

        txt_vehicle.setText(getIntent().getStringExtra("vehicle"));
        txt_pname.setText(getIntent().getStringExtra("name"));
//        price_.setText("PKR " +getIntent().getStringExtra("price"));

        Date.setText(getIntent().getStringExtra("date"));
        img=findViewById(R.id.v_image);

        Picasso.get()
                .load(getIntent().getStringExtra("dp"))
                .into(img);

         User_id=getIntent().getStringExtra("user_id");
        Driver_id=getIntent().getStringExtra("driver_id");
        Trip_id=getIntent().getStringExtra("trip_id");
        Ride_id=getIntent().getStringExtra("ride_id");
        payment_method=getIntent().getStringExtra("payment_method");

        if(payment_method.equals("wallet"))
        {
            price_.setVisibility(View.VISIBLE);
            ammount_title.setText("Plesase Collect "+"PKR 0");
            old_price=0;

        }
        else
        {
            ammount_title.setText("Plesase Collect "+"PKR " +getIntent().getStringExtra("price"));
            price_.setVisibility(View.VISIBLE);
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(feedback.getText().toString().length()<1)
                {
                    feedback.setText("Null");
                }
                Feedback=feedback.getText().toString();
                if(Integer.parseInt(price_.getText().toString())>=old_price)
                {
                    paid=price_.getText().toString();
                    if(Internet.isInternetAvailable(Rate_now.this)) {
                        new Rate_a_customer().execute();
                    }
                    else
                    {
                        Toast.makeText(Rate_now.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Rate_now.this, "Ammount Not Acceptable", Toast.LENGTH_SHORT).show();
                }



            }
        });




    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

        super.onBackPressed();       // bye
    }
    class Rate_a_customer extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            Constatns.is_success=false;
            dialog.setMessage("Processing . . .");
            dialog.show();
            Log.e("called","yes00");
            is_success=false;
            Log.e("details",User_id+":"+Driver_id+":"+Rating+":"+Feedback+":"+Trip_id+":"+Ride_id);
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);

if(is_success)
{
    Toast.makeText(Rate_now.this, "Feedback Submitted", Toast.LENGTH_LONG).show();
    onBackPressed();
}

else
{
    Toast.makeText(Rate_now.this, "Server Error Pleas Try Again Later", Toast.LENGTH_LONG).show();
}

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();


            HashMapParams.put("user_id",User_id);
            HashMapParams.put("driver_id", Driver_id);
            HashMapParams.put("rating",Rating);
            HashMapParams.put("feedback", Feedback);
            HashMapParams.put("trip_id",Trip_id);
            HashMapParams.put("ride_id", Ride_id);
            HashMapParams.put("owner", "driver");
            HashMapParams.put("price", old_price+"");
            HashMapParams.put("actual", Actual_price+"");
            HashMapParams.put("payment_method", payment_method);
            HashMapParams.put("paid", paid);


            Log.e("Ratenow_user_id",User_id);

            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.RATE_CUSTOMER, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                is_success=true;
//                try {
//                    is_success=true;
//                    JSONObject obj = new JSONObject(FinalData);
//                    JSONArray categories = obj.getJSONArray("status");
//                    for (int i = 0; i < categories.length(); i++) {
//                        JSONObject c = categories.getJSONObject(i);
//
//                        Constatns.email= c.getString("email");
//                        Constatns.fname= c.getString("fname");
//                        Constatns.lname= c.getString("lname");
//                        Constatns.type= c.getString("type");
//                        Constatns.mobile= c.getString("mobile");
//                        Constatns.gender= c.getString("gender");
//
//
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

            }
            else
            {
                is_success=false;
            }

            return FinalData;
        }
    }
}
