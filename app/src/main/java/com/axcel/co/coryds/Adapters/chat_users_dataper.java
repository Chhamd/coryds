package com.axcel.co.coryds.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.Chat_Screen;
import com.axcel.co.coryds.Activity.MainActivity_Driver;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Chat_Users_Model;
import com.axcel.co.coryds.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class chat_users_dataper extends RecyclerView.Adapter<chat_users_dataper.MyViewHolder> {

    ArrayList<Chat_Users_Model> users;

    Context ct;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,date_time,subject;
        public CircleImageView dp;
        public RelativeLayout Row;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            Row = (RelativeLayout) view.findViewById(R.id.row);
            dp = (CircleImageView) view.findViewById(R.id.dp);
            date_time = (TextView) view.findViewById(R.id.date);
            subject = (TextView) view.findViewById(R.id.subject);
        }
    }


    public chat_users_dataper(ArrayList<Chat_Users_Model> users, Context ct) {
        this.users = users;
        this.ct=ct;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_chat_user, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.name.setText(users.get(position).getUser_name());
        Picasso.get()
                .load(AppConfig.BASE_URL+users.get(position).getDp())
                .into(holder.dp);
        holder.Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(ct, Chat_Screen.class);
                i.putExtra("username",users.get(position).getUser_name());
                i.putExtra("thread_id",users.get(position).getThread_id());
                i.putExtra("from_",users.get(position).getFrom_());
                i.putExtra("to_",users.get(position).getTo_());
                i.putExtra("dp",users.get(position).getDp());
                ct.startActivity(i);
            }
        });

        holder.date_time.setText(users.get(position).getDate());
        holder.subject.setText(users.get(position).getSubject());
//
//        if(Integer.parseInt(IsRead.get(position))==1)
//        {
//            holder.Row.setBackgroundResource(R.drawable.edt_grad2);
//        }
//        else





    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}