package com.axcel.co.coryds.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.axcel.co.coryds.Adapters.Rating_RecyclerAdapter;
import com.axcel.co.coryds.Adapters.RecyclerTouchListener;
import com.axcel.co.coryds.Adapters.Ride_detail_adpt;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Ratings extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Rating_RecyclerAdapter mAdapter;
    private ProgressDialog dialog;
    ArrayList<String> USERNAME,Date,LOC,Rate,Feedback,Img,Publish_Date;

TextView total_reviews,rate_avg,five,four,three,two,one;
    String avg,Total_Reviews,Five,Four,Three,Two,One;
    TextView nodata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);
        Toolbar toolbar = findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        total_reviews=findViewById(R.id.total_reviews);

        rate_avg=findViewById(R.id.rate);
        five=findViewById(R.id.five);
        four=findViewById(R.id.four);
        three=findViewById(R.id.three);
        two=findViewById(R.id.two);
        one=findViewById(R.id.one);
        nodata=findViewById(R.id.no_data);
        nodata.setVisibility(View.GONE);



        dialog = new ProgressDialog(Ratings.this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        if(Internet.isInternetAvailable(Ratings.this)) {
            new Get_Ratings().execute();
        }
        else
        {
            Toast.makeText(Ratings.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(Ratings.this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               // Model movie = movieList.get(position);
//                Toast.makeText(Ratings.this, movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        
    }
    private void prepareMovieData() {
//        Model movie = new Model("dummy", "dummy","dummy","dummy","Dummy","dumy","","", "$ 250","dfd","dsfdsf",0);
//        movieList.add(movie);
//        movieList.add(movie);
//        movieList.add(movie);
//        movieList.add(movie);
//        movieList.add(movie);
//        movieList.add(movie);
//        movieList.add(movie);
//        movieList.add(movie);




        mAdapter.notifyDataSetChanged();
    }
    @Override
    public void onBackPressed() {        // to prevent irritating accidental logouts

//        super.onBackPressed();
        Intent i=new Intent(Ratings.this, MainActivity_Driver.class);
        i.putExtra("flag",0);
        startActivity(i);
        // bye
    }
    class Get_Ratings extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            USERNAME=new ArrayList<>();
            Date=new ArrayList<>();
            LOC=new ArrayList<>();
            Rate=new ArrayList<>();
            Feedback=new ArrayList<>();
            Img=new ArrayList<>();
            Publish_Date=new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);


                rate_avg.setText(avg+" / 5");


            five.setText(Five);
            four.setText(Four);
            three.setText(Three);
            two.setText(Two);
            one.setText(One);

                total_reviews.setText("Based on "+Total_Reviews+" Reviews");




            mAdapter = new Rating_RecyclerAdapter(USERNAME,Date,LOC,Rate,Feedback,Img,Publish_Date);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Ratings.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(Ratings.this, LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(mAdapter);
            prepareMovieData();
            if(USERNAME.size()==0)
            {
                nodata.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            else
            {
                nodata.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }



        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("user_id", Constatns.user_id);
            HashMapParams.put("type", "customer");
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_GET_RATINGS, HashMapParams);

            Log.e("responsemehamd",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");

                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        avg= c.getString("avg");
                        Total_Reviews=c.getString("totalcount");
                        Five= c.getString("five");
                        Four= c.getString("four");
                        Three= c.getString("three");
                        Two=c.getString("two");
                        One=c.getString("one");

                        USERNAME.add(c.getString("name"));
                        Date.add(c.getString("date"));
                        LOC.add(c.getString("from")+" > "+c.getString("to"));
                        Rate.add(c.getString("rating"));
                        Feedback.add(c.getString("feedback"));
                        Img.add(c.getString("user_dp"));
                        Publish_Date.add(c.getString("publish_date"));

                    }
//                    JSONArray users = obj.getJSONArray("new_users");
//                    Log.e("hamdullah","yrsess"+users.toString());
//                    JSONArray users_sub=users.getJSONArray(0);
//                    for (int i = 0; i < users_sub.length(); i++) {
//                        JSONObject c = users_sub.getJSONObject(i);
//                        CUSTOMER_ID.add(c.getString("user_id"));
//                        CUSTOMER_NAME.add(c.getString("user_name"));
//                        CUSTOMER_PIC.add(c.getString("user_dp"));
//                        CUSTOMER_MOBILE.add(c.getString("customer_phone"));
//                        PRICE.add(c.getString("price"));
//                        FROM.add(c.getString("from"));
//                        TO.add(c.getString("to"));
//                        ride_id=c.getString("ride_id");
//                        CUSTOMER_IS_PICKED.add(c.getString("is_pick"));
//                        CUSTOMER_IS_DROPED.add(c.getString("is_drop"));
//                        CUSTOMER_IS_ACTIVE.add(c.getString("is_active"));
//                        CUSTOMER_PAYMENT_METHOD.add(c.getString("payment_method"));
//
//                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else
            {
                avg= "0.0";
                Total_Reviews="0";
            }


            return FinalData;
        }
    }
}
