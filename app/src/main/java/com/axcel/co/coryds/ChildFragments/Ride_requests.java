package com.axcel.co.coryds.ChildFragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.axcel.co.coryds.Activity.MainActivity_Driver;
import com.axcel.co.coryds.Adapters.RecyclerAdapter;
import com.axcel.co.coryds.Adapters.RecyclerTouchListener;
import com.axcel.co.coryds.Adapters.Ride_Requests_RecyclerAdapter;
import com.axcel.co.coryds.Adapters.Ride_Requests_RecyclerAdapter_Clone;
import com.axcel.co.coryds.Config.AppConfig;
import com.axcel.co.coryds.Config.AppInterruptHandler;
import com.axcel.co.coryds.Config.Constatns;
import com.axcel.co.coryds.Config.ImageProcessClass;
import com.axcel.co.coryds.Config.Model;
import com.axcel.co.coryds.MainFragments.Notification_Fragment;
import com.axcel.co.coryds.R;
import com.axcel.co.coryds.Utils.Internet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Ride_requests extends Fragment{
    private ProgressDialog dialog;
    private List<Model> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Ride_Requests_RecyclerAdapter_Clone mAdapter;
    Switch past;
boolean is_old=false;

    TextView nodata;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.ride_requests_child,container,false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        nodata=v.findViewById(R.id.no_data);
        nodata.setVisibility(View.GONE);
        dialog = new ProgressDialog(getActivity());
        past=v.findViewById(R.id.past);
        if(Internet.isInternetAvailable(getActivity())) {
            is_old=false;
            new RIDE_REQUESTS().execute();
        }
        else
        {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }


        past.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                is_old=b;
                if(Internet.isInternetAvailable(getActivity())) {

                    new RIDE_REQUESTS().execute();
                }
                else
                {
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Model movie = movieList.get(position);
//                Toast.makeText(getActivity(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return v;
    }



    class RIDE_REQUESTS extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            movieList = new ArrayList<>();

            dialog.setMessage("Processing . . .");
            dialog.show();
            super.onPreExecute();
            Log.e("called","yes00");

        }

        @Override
        protected void onPostExecute(String string1) {
            dialog.dismiss();
            super.onPostExecute(string1);
            if (movieList.size()>0) {
                nodata.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                mAdapter = new Ride_Requests_RecyclerAdapter_Clone(movieList, getActivity());
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
            else
            {
                nodata.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            ImageProcessClass imageProcessClass = new ImageProcessClass();

            HashMap<String, String> HashMapParams = new HashMap<String, String>();



            HashMapParams.put("driver_id", Constatns.user_id);
            HashMapParams.put("date", getdate());
            HashMapParams.put("is_old", is_old+"");
            String FinalData = imageProcessClass.ImageHttpRequest(AppConfig.URL_RIDE_REQUESRS, HashMapParams);

            Log.e("responsemehamdreq",FinalData);
            if(!FinalData.contains("error")) {
                try {

                    JSONObject obj = new JSONObject(FinalData);
                    JSONArray categories = obj.getJSONArray("status");
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject c = categories.getJSONObject(i);

                        Model movie = new Model(c.getString("user_name"),c.getString("source"),c.getString("destination"),c.getString("booking_date"),c.getString("booking_time"),c.getString("booking_price"),c.getString("booking_seats"),c.getString("user_mobile"),"Contact ",c.getString("trip_id"),c.getString("dp"),c.getString("rating"),c.getString("reviews"),c.getString("user_id"),Integer.parseInt(c.getString("is_active")));
                        movieList.add(movie);




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return FinalData;
        }
    }
    public String getdate()
    {
        String currentDate = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public  void OnNewRequest()
    {


            new RIDE_REQUESTS().execute();

    }
}
