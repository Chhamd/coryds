package com.axcel.co.coryds.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axcel.co.coryds.R;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LinearLayout parentLinearLayout=findViewById(R.id.parentLinearLayout);

        ImageView appNameTextView=findViewById(R.id.bg_img);
        startAnimationForAppLogo(parentLinearLayout, appNameTextView);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i=new Intent(Splash.this, Login_Activity.class);
                startActivity(i);
            }
        }, 4000);
    }
    public static void startAnimationForAppLogo( final LinearLayout parentLinearLayout,final ImageView textViewForInvisi){
        final AnimatorSet animSet = new AnimatorSet();
        // ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(parentLinearLayout, "alpha", 0f, 1f);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator transAnim = ObjectAnimator.ofFloat(parentLinearLayout, "translationY",
                        0f, -160f);
                animSet.playTogether(transAnim);
                animSet.setDuration(3000);
                animSet.start();

            }
        },1000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                textViewForInvisi.setVisibility(View.GONE);

            }
        }, 2500);

    }
}
